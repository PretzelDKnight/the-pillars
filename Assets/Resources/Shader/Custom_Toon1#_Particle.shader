﻿ Shader "Custom/Custom_Toon_Particle#" {
        Properties {
            _Color ("Color", color) = (1,1,1,1)
        }
        SubShader {
        Tags { "RenderType" = "Opaque" }
        CGPROGRAM
          #pragma surface surf SimpleLambert
  
        struct Input {
            float2 uv;
            float4 color: Color;
        };

        fixed4 _Color;
        uniform half4 TOON_SHADOW_COLOR;
        uniform half3 TOON_SKY_COLOR;
        uniform half3 TOON_GROUND_COLOR;

          half4 LightingSimpleLambert (SurfaceOutput s, half3 lightDir, half atten) {
              half NdotL = dot (s.Normal, lightDir);
              fixed skyDir = dot (s.Normal, fixed3(0,1,0));
              half4 c;
              c.rgb = s.Albedo * _LightColor0.rgb * 
              lerp(TOON_SHADOW_COLOR.rgb, 1, atten) * 
              lerp(TOON_GROUND_COLOR, TOON_SKY_COLOR, skyDir) * 
              max(1 - step(NdotL, 0.1), 0.2);
              c.a = s.Alpha;
              return c;
          }
  
      
        void surf (Input IN, inout SurfaceOutput o) {
            o.Albedo = IN.color.rgb;
            o.Emission = IN.color.rgb * IN.color.a;
        }
        ENDCG
        }
        Fallback "Diffuse"
    }