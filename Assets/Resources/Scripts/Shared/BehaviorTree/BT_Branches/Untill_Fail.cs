﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Untill_Fail : BTBaseNode
{
    public override RESULTS UpdateBehavior(Enemy enemy)
    {
        for (int i = 0; i < childNodes.Count; i++)
        {
            childNodes[i].UpdateBehavior(enemy);
            if (childNodes[i].current == RESULTS.FAILED)
            {
                current = childNodes[i].current;
                return current;
            }
        }
        current = RESULTS.RUNNING;
        return current;
        Debug.Log("Selector Node State : " + current);
    }
}
