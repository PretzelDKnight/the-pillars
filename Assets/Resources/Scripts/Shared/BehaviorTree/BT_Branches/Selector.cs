﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : BTBaseNode
{
    public override RESULTS UpdateBehavior(Enemy enemy)
    {
        for (int i = 0; i < childNodes.Count; i++)
        {
            childNodes[i].UpdateBehavior(enemy);
            if (childNodes[i].current == RESULTS.SUCCEED || childNodes[i].current == RESULTS.RUNNING || childNodes[i].current == RESULTS.READY)
            {
                current = childNodes[i].current;
                return current;
            }
        }
        current = RESULTS.FAILED;
        return current;
        Debug.Log("Selector Node State : " + current);
    }
}
