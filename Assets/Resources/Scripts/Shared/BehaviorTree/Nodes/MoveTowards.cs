﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : BTBaseNode
{
    private float rangeBtPlayer;

    public override RESULTS UpdateBehavior(Enemy enemy)
    {
        float distanceToObject = Vector3.Distance(enemy.transform.position, enemy.target.transform.position);

        if (distanceToObject < rangeBtPlayer)
        {
            return RESULTS.SUCCEED;
        }
        else
        {
            enemy.Move();
            return RESULTS.RUNNING;
        }
    }
}
