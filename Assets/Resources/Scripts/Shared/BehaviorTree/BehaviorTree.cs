﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorTree
{
    public Enemy enemy;
    public BTBaseNode root;

    public BehaviorTree(BTBaseNode node, Enemy _enemy)
    {
        enemy = _enemy;
        root = node;
    }

    public void Executed()
    {
        root.UpdateBehavior(enemy);
    }
}
