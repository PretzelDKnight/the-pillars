﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class BTBaseNode
{
    public enum RESULTS
    {
        READY,
        SUCCEED,
        FAILED,
        RUNNING,
    }

    public RESULTS current;

    public List<BTBaseNode> childNodes = new List<BTBaseNode>();

    public abstract RESULTS UpdateBehavior(Enemy enemy);
}