﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Areas
{
    public Vector3 center;
    public Vector3 size;
}

public class SpawnInRandomArea : MonoBehaviour
{
    [SerializeField] List<Areas> areaList;

    public GameObject enemyPrefab1;
    public GameObject enemyPrefab2;

    public int persentileChanceOfSpawningMelee;
    public float maxEnemyCount;

    float time;
    float countdown = 1f;

    bool spawn = true;

    List<GameObject> enemies = new List<GameObject>();

    private void Start()
    {
        spawn = false;
    }

    void Update()
    {
        if (spawn)
        {
            time -= Time.deltaTime;

            if(enemies.Count < maxEnemyCount)
            {
                if (time < 0)
                {
                    SpawnEnemy();
                    time = countdown;
                }
            }

            CullNull();
        }
    }

    public void SpawnEnemy()
    {
        if (areaList.Count > 0)
        {
            int i = Random.Range(0, areaList.Count);

            Vector3 pos = areaList[i].center + new Vector3(Random.Range(-areaList[i].size.x / 2, areaList[i].size.x / 2), //Calculate position where you want to spawn the object
            Random.Range(-areaList[i].size.y / 2, areaList[i].size.y / 2), Random.Range(-areaList[i].size.z / 2, areaList[i].size.z / 2)); //Reason you divide it by 2 is because it will offset off of the

            GameObject selectedEnemy = Random.Range(0, 100) < persentileChanceOfSpawningMelee ? enemyPrefab1 : enemyPrefab2;
            //center and will go outside the wanted area

            GameObject enemy = ObjectPooling.PoolInstantiate(selectedEnemy, pos, Quaternion.identity);
            enemies.Add(enemy);
        }
        else
            Debug.Log("Arealist in Spawner is empty!");
    }

    public void SpawnItems(GameObject obj, float times)
    {
        if (areaList.Count > 0)
        {
            for (int i = 0; i < times; i++)
            {
                int j = Random.Range(0, areaList.Count);

                Vector3 pos = areaList[j].center +
                    new Vector3(Random.Range(-areaList[j].size.x / 2, areaList[j].size.x / 2),
                    Random.Range(-areaList[j].size.y / 2, areaList[j].size.y / 2),
                    Random.Range(-areaList[j].size.z / 2, areaList[j].size.z / 2));

                ObjectPooling.PoolInstantiate(obj, pos, Quaternion.identity);
            }
        }
        else
            Debug.Log("Arealist in Spawner is empty!");
    }

    private void OnDrawGizmosSelected()
    {
        foreach (var area in areaList)
        {
            Gizmos.color = new Color(1, 0, 0, 0.5f);
            Gizmos.DrawCube(area.center, area.size);
        }
    }

    public bool SetSpawn
    {
        get { return spawn; }
        set { spawn = value; }
    }

    public int SetRatio
    {
        get { return persentileChanceOfSpawningMelee; }
        set { persentileChanceOfSpawningMelee = value; }
    }

    public void PurgeEnemies(Vector3 pos, float timeTaken)
    {
        StartCoroutine(RadialDelete(pos, timeTaken));
    }

    public float SetSpawnTime
    {
        get { return countdown; }
        set { countdown = value; }
    }

    IEnumerator RadialDelete(Vector3 pos, float timeTaken)
    {
        float time = 0;
        float distance = 0;

        while (time <= 1.1f)
        {
            time += Time.deltaTime / timeTaken;

            distance = Mathf.Lerp(0, 100, time);

            for (int i = enemies.Count - 1; i >= 0; i--)
            {
                if (enemies[i] == null)
                {
                    enemies.RemoveAt(i);
                    i--;
                    continue;
                }

                if (Vector3.Distance(enemies[i].transform.position, pos) <= distance)
                {
                    ObjectPooling.PoolDestroy(enemies[i]);
                    enemies.RemoveAt(i);
                    i--;
                }
            }

            yield return null;
        }

        if (enemies.Count > 0)
        {
            for (int i = enemies.Count - 1; i >= 0; i--)
            {
                ObjectPooling.PoolDestroy(enemies[i]);
                enemies.RemoveAt(i);
                i--;
            }
        }

        enemies.Clear();
    }

    void CullNull()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] == null)
            {
                enemies.RemoveAt(i);
                i -= 1;
            }
            else if (enemies[i].gameObject.activeSelf)
            {
                enemies.RemoveAt(i);
                i -= 1;
            }
        }
    }
}
