﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMeleeAttack : BTBaseNode
{
    private float rangeBtPlayer;
    Fist primaryWeap;

    public override RESULTS UpdateBehavior(Enemy enemy)
    {
        float distanceToObject = Vector3.Distance(enemy.transform.position, enemy.target.transform.position);

        if (distanceToObject < rangeBtPlayer)
        {
            enemy.UsePrimary();
            return RESULTS.SUCCEED;
        }
        else
        {
            return RESULTS.FAILED;
        }
    }
}
