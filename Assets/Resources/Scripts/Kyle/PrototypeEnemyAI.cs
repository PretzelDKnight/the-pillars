﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeEnemyAI : MonoBehaviour
{
    [SerializeField]
    Transform playerChar;
    [SerializeField] int currentHealth;
    public Player player;
    [SerializeField] int health;
    public ProgressBar progressBar;
    public Vector3 target;
    public float speed = 1.0f;
    public float damageDistance;

    public int bloodGained;

    private void Start()
    {
        currentHealth = health;
    }
    private void Awake()
    {
        //player = Player.FindObjectOfType<Player>();
        //playerChar = GameObject.FindGameObjectWithTag("Player").transform;
        //progressBar = FindObjectOfType<ProgressBar>();
        //Formation.instance.AddMember(this);
    }
    void Update()
    {
        float properSpeed = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, properSpeed);
        EnemyDie();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            player.RecieveDamage(transform.position);
        }
    }

    public void TakeDamageEnemy()
    {
        currentHealth -= 1;
    }

    void EnemyDie()
    {
        if (currentHealth <= 0)
        {
            //progressBar.currentProg += bloodGained;
            ObjectPooling.PoolDestroy(gameObject);
            currentHealth = health;
        }
    }

    public void SetTarget(Vector3 pos)
    {
        target = pos;
    }
}
