﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Flocker : MonoBehaviour
{
    public static Flocker instance;
    public float cohesionDist = 6f;
    public float seperationDist = 4f;
    public float alignmentDist = 6f;

    public float cohesionWeight = 0.2f;
    public float seperationWeight = 0.4f;
    public float alignmentWeight = 0.2f;


    public float collissionRadi = 0.2f;
    [Header("Stand Physics")]
    public float standingRadi = 0.2f;
    public float minStandRange = 0.2f;
    public float maxStandRange = 2f;
    [Header("Climb Physics")]
    public float climbingDistance = 0.2f;
    public float minClimbRange = 0.2f;
    public float maxClimbRange = 2f;

    List<AdvEnemyAIBase> neighbours = new List<AdvEnemyAIBase>();

    Vector3 cohessionVec, seperationVec, alignmentVec, tempVec;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Vector3 FlockCalculation(AdvEnemyAIBase boid)
    {
        return OptSeperation(boid);
    }

    public void AddNeighbour(AdvEnemyAIBase neighbour)
    {
        neighbours.Add(neighbour);
    }

    public void RemoveNeighbour(AdvEnemyAIBase neighbour)
    {
        neighbours.Remove(neighbour);
    }

    void FlockingFactor(AdvEnemyAIBase thisEnemy)
    {
        // cohessin
        cohessionVec = Vector3.zero;
        int cohessionCount = 0;
        // sepration
        Vector3 distance = Vector3.zero;

        // alignment
        alignmentVec = Vector3.zero;
        int alignCount = 0;

        for (int i = 0; i < neighbours.Count; i++)
        {
            
            if (thisEnemy != neighbours[i])
            {
                Vector3 dir = neighbours[i].transform.position - thisEnemy.transform.position;
                float dist = dir.sqrMagnitude;

                // cohession
                if (dist < cohesionDist * cohesionDist)
                {
                    cohessionCount++;
                    cohessionVec += neighbours[i].transform.position;
                }

                // Seperation
                if (dist < seperationDist * seperationDist) 
                {
                    distance += dir * (1 - (dist / Mathf.Pow(seperationDist,2) ));
                }

                // alignment
                if (dist < alignmentDist * alignmentDist)
                {
                    alignCount++;
                    alignmentVec += neighbours[i].velocity.normalized;
                }

            }
        }

        cohessionVec /= cohessionCount;
        cohessionVec = cohessionVec - thisEnemy.transform.position;
        cohessionVec = cohessionVec.normalized * cohesionWeight;

        seperationVec = distance * -seperationWeight;

        alignmentVec /= alignCount;

        alignmentVec = alignmentVec.normalized * alignmentWeight;

    }

    Vector3 Cohesion(Enemy thisEnemy)
    {
        Vector3 centre = Vector3.zero;
        int count = 0;
        for (int i = 0; i < neighbours.Count; i++)
        {
            if(thisEnemy != neighbours[i])
            {
                if (Vector3.Distance(thisEnemy.transform.position, neighbours[i].transform.position) < cohesionDist)
                {
                    count++;
                    centre += neighbours[i].transform.position;
                }
            }
        }

        centre /= count;
        centre = centre - thisEnemy.transform.position;
        return centre.normalized * cohesionWeight;
    }


    Vector3 OptSeperation(AdvEnemyAIBase thisEnemy)
    {
        Vector3 distance = Vector3.zero;
        int[] nineBlocks = null;
        nineBlocks = EnemyPartetioner.instance.GetNineBlockContent(thisEnemy.transform.position);
     //   Debug.Log("Active Block Count : " + nineBlocks.Length);
        AdvEnemyAIBase enemyAIBase;
        if(nineBlocks == null)return Vector3.zero;
            //  state checks
            bool onTop = false;
            bool isClimbing = false;
            for (int n = 0; n < nineBlocks.Length; n++)
            {
                for(int i = 0; i < EnemyPartetioner.instance.enemyListCollection[nineBlocks[n]].enemyList.Count; i++)
                {
                    
                    enemyAIBase = EnemyPartetioner.instance.enemyListCollection[nineBlocks[n]].enemyList[i];
                    if (thisEnemy != enemyAIBase)
                    {
                        float multiPlier =  enemyAIBase.isFlockable ? 1 : 0.06f;
                        Vector3 dir = enemyAIBase.transform.position - thisEnemy.transform.position;
                        float dist = dir.sqrMagnitude;
                        float squaredUalue = Mathf.Pow(seperationDist,2);
                        if (dist < Mathf.Pow(seperationDist, 2))
                        {
                            if(enemyAIBase.isFlockable)distance += -AlMathf.GetMagnitudePlanar(dir) * seperationDist * (1 - (dist /  squaredUalue)) * multiPlier;

                            // check on top of another enemy
                            tempVec = dir;
                            tempVec.y = 0;
                            if(tempVec.sqrMagnitude < Mathf.Pow(standingRadi,2) 
                            && enemyAIBase.transform.position.y + minStandRange < thisEnemy.transform.position.y
                            && enemyAIBase.transform.position.y + maxStandRange > thisEnemy.transform.position.y){
                                onTop = true;
                            }
                            
                            if(thisEnemy.enemyType == AdvEnemyAIBase.EnemyTypes.melee 
                            && tempVec.sqrMagnitude < Mathf.Pow(climbingDistance,2) 
                            && Vector3.Dot(tempVec, thisEnemy.transform.forward) > 0
                            && enemyAIBase.transform.position.y > thisEnemy.transform.position.y - minClimbRange
                            && enemyAIBase.transform.position.y < thisEnemy.transform.position.y + maxStandRange
                            && enemyAIBase.selectedAction == AdvEnemyAIBase.AIActionState.damagePillar){
                                isClimbing = true;
                            }

                        }
                        // physics section
                     //   ColissionManageMent(thisEnemy, enemyAIBase, dir, dist);
                    }
                }
            }

        // stand param notification
        if(onTop){
            thisEnemy.inTopOfEnemy = true;
            thisEnemy.isFlockable = false;
        }
        else{
            thisEnemy.inTopOfEnemy = false;
            thisEnemy.isFlockable = true;
        }
        // climb param notification
        if(isClimbing){
            thisEnemy.climbing = true;
            thisEnemy.isFlockable = false;
        }
        else {
             thisEnemy.climbing = false;
            thisEnemy.isFlockable = true;
        }

        return distance * seperationWeight;
    }

    void ColissionManageMent(AdvEnemyAIBase objA, AdvEnemyAIBase objB, Vector3 dir, float squareMag){
        if(squareMag < Mathf.Pow(collissionRadi * 2,2)){
            float mag = dir.magnitude;
            float difMag = ((collissionRadi * 2) - mag) *  0.5f;
            Vector3 dirNormalized = dir.normalized;
            objA.colissionShift += dirNormalized * difMag;
       //     objB.position += dirNormalized * difMag * 0.7f;
        }
    }

    Vector3 Seperation(AdvEnemyAIBase thisEnemy)
    {
        Vector3 distance = Vector3.zero;
        for (int i = 0; i < neighbours.Count; i++)
        {
            if (thisEnemy != neighbours[i])
            {
                Vector3 dir = neighbours[i].transform.position - thisEnemy.transform.position;
                float dist = dir.sqrMagnitude;
                float squaredUalue = Mathf.Pow(seperationDist,2);
                if (dist < Mathf.Pow(seperationDist, 2))
                {
                    distance += -dir.normalized * seperationDist * (1 - (dist /  squaredUalue));
                }
            }
        }

        return distance * seperationWeight;
    }

    Vector3 Alignment(Enemy thisEnemy)
    {
        Vector3 velocity = Vector3.zero;
        int count = 0;
        for (int i = 0; i < neighbours.Count; i++)
        {
            if (thisEnemy != neighbours[i])
            {
                if (Vector3.Distance(thisEnemy.transform.position, neighbours[i].transform.position) < alignmentDist)
                {
                    count++;
                    velocity += neighbours[i].velocity.normalized;
                }
            }
        }

        velocity /= count;

        return velocity.normalized * alignmentWeight;
    }
}