﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Formation : MonoBehaviour
{
    public static Formation instance = null;
    List<Enemy> members;
    public Transform centreOfForm;
    public float radiusFactor = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        members = new List<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddMember(Enemy enemy)
    {
        members.Add(enemy);
        if(members.Count - 1 > 0)
        {
            CalculatePosition();
        }
    }

    void CalculatePosition()
    {
        Vector3 centre = centreOfForm.position;
        float radius = members.Count * radiusFactor;
        float unit = 360 / members.Count;
        for (int i = 0; i < members.Count; i++)
        {
            Vector3 position = Vector3.zero;
            position.x = radius * Mathf.Cos(unit * i) + centre.x;
            position.z = radius * Mathf.Sin(unit * i) + centre.z;
            members[i].AssignPosition(position);
        }
    }
}
