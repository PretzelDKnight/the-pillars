﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{
    public static ObjectPooling instance;

    List<GameObject> pooledEnemies;

    static Dictionary<string, List<PoolObject>> objectPoolLibrary;
    static Dictionary<string, int> objectPoolLimit;

    private void Start()
    {
        if(instance != null) // Safety Check
            Destroy(this);
        else
            instance = this; // Singleton

        objectPoolLibrary = new Dictionary<string, List<PoolObject>>(); // Initialize Dictionary
        objectPoolLimit = new Dictionary<string, int>(); 
    }

    static public GameObject PoolInstantiate(GameObject obj, Vector3 pos, Quaternion rot)
    {
        PoolObject val;
        if(objectPoolLibrary.ContainsKey(obj.tag)) // Look for the "Tag" in the Dictionary
        {
            List<PoolObject> tempList = objectPoolLibrary[obj.tag]; // retrieving temporary list for the func

            for (int i = 0; i < tempList.Count; i++) // Go through the temp list
            {
                if (!tempList[i].gameObject.activeInHierarchy)
                {
                    //Debug.Log("Re-using ded body");
                    val = tempList[i]; // setting GameObject we're looking at into tempval
                    val.transform.position = pos;
                    val.transform.rotation = rot;
                    val.gameObject.SetActive(true);
                    return val.gameObject; // returns the inactive gameobject value 
                }
            }

            val = Instantiate(obj, pos, rot).GetComponent<PoolObject>();
            //Debug.Log("Created new body");

            if (tempList.Count <= objectPoolLimit[obj.tag]) // When the count is less than the Maximum Pool add it into the temp list
            {
                //Debug.Log("Reserved body space");
                tempList.Add(val);
            }

            return val.gameObject; // returns the newly create gameobject 
        }

        val = Instantiate(obj, pos, rot).GetComponent<PoolObject>(); // Spawn that non-dictionary GameObject
        objectPoolLibrary.Add(obj.tag, new List<PoolObject>() { val }); // Add the new object into the dictionary
        objectPoolLimit.Add(obj.tag, val.poolAmount); // Add the new objects pool limit
        //Debug.Log("New Body found and registered");

        return val.gameObject;
    }

    static public void PoolDestroy(GameObject obj)
    {
        if(objectPoolLibrary.ContainsKey(obj.tag))
        {
            List<PoolObject> tempList = objectPoolLibrary[obj.tag];
            if(tempList.Contains(obj.GetComponent<PoolObject>()))
            {
                //Debug.Log("Storing body in morgue");
                obj.SetActive(false);
            }
            else
            {
                //Debug.Log("Incinerating Dead body");
                Destroy(obj.gameObject);
            }
        }
    }
}
