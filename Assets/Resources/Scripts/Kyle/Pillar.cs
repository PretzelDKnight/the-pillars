﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pillar : MonoBehaviour
{
    public List<PillarSegment> segments = new List<PillarSegment>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Add(PillarSegment segment)
    {
        segments.Add(segment);
    }
}
