﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fist : Weapon
{
    [SerializeField] GameObject attackCollider;
    bool setFalse;


    public void FistAttack()
    {
        if (attackCollider.activeSelf == false)
            attackCollider.SetActive(true);
        else
            attackCollider.SetActive(false);
    }
}
