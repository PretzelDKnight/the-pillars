﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFadeLerp : MonoBehaviour
{
    Light objectLight;
    float lightIntensity;
    [SerializeField]float minIntensity;
    [SerializeField]float maxIntensity;
    [SerializeField]float lerpTime;
    [SerializeField] float pingPongLength = 1f;
    [SerializeField] float pingPongSpeed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        objectLight = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        LightLerp();
    }

    void LightLerp()
    {
        lerpTime = Mathf.PingPong(Time.time * pingPongSpeed, pingPongLength);
        lightIntensity = Mathf.Lerp(minIntensity, maxIntensity, lerpTime);

        objectLight.intensity = lightIntensity;
    }
}
