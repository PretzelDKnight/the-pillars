﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SegmentState
{
    Creator,
    Normal
}

public class PillarSegment : MonoBehaviour
{
    public float hitPoints;
    public PillarSegment child;
    [SerializeField] LayerMask segmentLayer;
    float currentHitPoints;
    SegmentState state;
    Material pSegmentMaterial;
    Pillar pillar;

    void Start()
    {
        Prep();
    }
     
    void Update()
    {
        if (state == SegmentState.Creator)
        {
            GeneratePillar();
        }
    }

    void CheckBelowSegment()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, -Vector3.up, out hit, segmentLayer))
        {
            Debug.DrawRay(transform.position, -Vector3.up , Color.green);
            if(hit.collider.tag == "Segment")
            {
                hit.collider.GetComponent<PillarSegment>().AssignChild(this);
            }
            else if (hit.collider.tag == "Ground")
            {
                state = SegmentState.Creator;
            }
        }
        Debug.DrawRay(transform.position, -Vector3.up, Color.green);
    }

    public void AssignChild(PillarSegment segment)
    {
        child = segment;
    }

    void GeneratePillar()
    {
        if (child != null)
        {
            pillar = new Pillar();
            pillar = gameObject.AddComponent<Pillar>();
            AddToPillar(pillar);
            state = SegmentState.Normal;
        }
    }

    PillarSegment AddToPillar(Pillar pillar)
    {
        pillar.Add(this);

        if (child != null)
        {
            return child.AddToPillar(pillar);
        }
        else
            return null;
    }

    void DestroySegment()
    {

    }

    void ReceiveDamage()
    {

    }

    void Prep()
    {
        currentHitPoints = hitPoints;
        state = SegmentState.Normal;
        CheckBelowSegment();
    }
}
