﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEnterExitNotifier : MonoBehaviour
{
    public int srcID;
    public ColliderData OnEnter;
    public ColliderData OnStay;
    public ColliderData OnExit;

    [System.Serializable]
    public class ColliderData : UnityEvent<Collider, int>{} 

    private void OnTriggerEnter(Collider other)
    {
        OnEnter.Invoke(other, srcID);
    }

    private void OnTriggerStay(Collider other)
    {
        OnStay.Invoke(other, srcID);
    }

    private void OnTriggerExit(Collider other)
    {
        OnExit.Invoke(other, srcID);
    }
}
