﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Rigidbody rigidbody;
    public TrailRenderer trail;
    public float flyTrailTime = 5;
    float onHitTrailTime = 0.5f;
    public bool alive;
    public GameObject bulletBubbles;
    [ShowOnly]public int lenght;
    ParticleSystem hitSplash;

    private void Update() {

        lenght = trail.positionCount;

        if(!alive && lenght < 1 && !hitSplash.isPlaying){
            OnKill();
            hitSplash.gameObject.SetActive(false);
        }

    }

    private void OnEnable() {
        trail.Clear();
        alive = true;
        rigidbody.isKinematic = false;
        trail.time = flyTrailTime;
    }

    private void OnTriggerEnter(Collider other) {
        if(alive){
            switch (other.gameObject.tag)
            {
                case "Player":
                    Player player = other.gameObject.GetComponent<Player>();
                    player.GetComponent<Player>().RecieveDamage(transform.position);
                    break;

                case "Enemy":
                    return;
  
                default:
                    break;
            }
            OnHit();
        }
    }

    void OnHit(){
        GameObject obj = ObjectPooling.PoolInstantiate(bulletBubbles, transform.position, Quaternion.LookRotation(-rigidbody.velocity, Vector3.forward));
        hitSplash = obj.GetComponent<ParticleSystem>();
        hitSplash.Play();
        rigidbody.isKinematic = true;
        trail.time = onHitTrailTime;
        alive = false;
    }

    void OnKill(){
        ObjectPooling.PoolDestroy(gameObject);
    }



}
