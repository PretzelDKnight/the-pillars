﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SegmentedPillar : MonoBehaviour
{

    // universal static
    public static SegmentedPillar playerOnPillar;


    public GameObject mainParent;
    public SphereCollider detectionCollider;
    public bool exist = true;
    public bool usedByPlayer = false;
    public Rigidbody rb;
    public PillarBlock[] pillarSegments;
    public int pillarSegmentCount;
    PillarBlock currentBlock;
    
    int playerNotifyCount = 0;


    [Header("Effects")]
    public ParticleSystem OnHitEffect;
    public ParticleSystem OnDestroyEffect;

    [Header("Events")]
    public UnityEvent OnSegmentBreak;
    public UnityEvent OnPillarDestroyed;


    private void Start()
    {
        pillarSegmentCount = pillarSegments.Length;
        currentBlock = pillarSegments[pillarSegmentCount - 1];
        currentBlock.Init();
        AssignController();
    }

    public void AssignController()
    {
        foreach (PillarBlock item in pillarSegments)
        {
            item.SetContrtolObject(this);
        }
    }


    public void RegisterSegmentLost() {
        rb.AddForce(Vector3.up, ForceMode.Impulse);
        pillarSegmentCount--;
        if (pillarSegmentCount > 0){
            currentBlock = pillarSegments[pillarSegmentCount - 1];
            currentBlock.Init();
            StartCoroutine(MoveDown());
        }
        else PillarDestroyed();
        OnSegmentBreak.Invoke();
    }

    IEnumerator MoveDown(){
        RaycastHit hit;
        Physics.Raycast(currentBlock.transform.position, -Vector3.up, out hit);
        float fallValue = 0;
        Vector3 disp = (hit.point - currentBlock.transform.position) + transform.position;
        while((transform.position - disp).sqrMagnitude > 0.01f){
            transform.position += Vector3.down * fallValue * Time.deltaTime;
            fallValue += 9.8f * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    public void PillarDestroyed() {
        exist = false;
        playerNotifyCount--;
        playerOnPillar = null;
        OnPillarDestroyed.Invoke();
    }

    IEnumerator DestroyPillar(){

        yield return new WaitWhile(() => OnDestroyEffect.isPlaying);
        Destroy(mainParent);
    }

    void ScatterEnemies(float upForce, float sideForce){
    }

    public void HitEffectAt(Vector3 pos) {
        OnHitEffect.transform.position = pos;
        OnHitEffect.Play();
    }

    public void DestroyEffectAt(Vector3 pos)
    {
        OnDestroyEffect.transform.position = pos;
        OnDestroyEffect.Play();
    }

    public void Damage(int damage) => currentBlock.Damage(damage);
    
    public void OnObjectEnter(Collider collider, int srcID)
    {
        if(collider.CompareTag("Player")){
            playerNotifyCount++;
            playerOnPillar = this;
            usedByPlayer = true;
        }
    }

    public void OnObjectExit(Collider collider, int srcID)
    {
        if(collider.CompareTag("Player")){
            playerNotifyCount--;
            playerOnPillar = null;
            usedByPlayer = false;
        }
    }

}
