﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableIndicatorRing : MonoBehaviour
{

    public float pointSpeed;
    public Transform indicatorObject;

    float nextUpdateTime;
    const float UPDATE_RATE = 1/23;
    Vector3 recDirection;
    void Update()
    {
        if(nextUpdateTime < Time.time){

            if(CollectableKeeper.activeCollectables.Count > 0){
                if(!indicatorObject.gameObject.activeSelf)indicatorObject.gameObject.SetActive(true);
                Vector3 dir;
                float dist = Mathf.Infinity;
                foreach (CollectableKeeper item in CollectableKeeper.activeCollectables)
                {
                    dir =  item.transform.position - indicatorObject.position;
                    if(dist > dir.sqrMagnitude){
                        dist = dir.sqrMagnitude;
                        recDirection = dir;
                    }
                    
                }

                recDirection.y = 0;
                recDirection.Normalize();

                indicatorObject.forward = Vector3.Lerp(indicatorObject.forward, recDirection, Time.deltaTime * pointSpeed);
            }
            else{
                if(indicatorObject.gameObject.activeSelf)indicatorObject.gameObject.SetActive(false);
            }



            nextUpdateTime = Time.time + UPDATE_RATE;
        }
    }
}
