﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ColliderRings : MonoBehaviour
{


    [HideInInspector]public List<Vector4> colissionRings = new List<Vector4>();
    [HideInInspector]public Vector3 mainRing;
    [HideInInspector]public float mainRingRadius;
    public bool initializeOnStart;
    public Transform rootOwner;
    Vector3 tempVec3 = Vector3.zero;

    private void Start() {
        if(initializeOnStart){
            Init();
            rootOwner = transform;
        }
    }

    public void Init()
    {
        ColiderRingKeeper.instance.AddSystem(this);
    }

    public void Terminate(){
        ColiderRingKeeper.instance.RemoveSystem(this);
    }

    public void SetOwner(Transform owner)
    {
        rootOwner = owner;
    }

    public void AddCircle(){
        Vector4 newRing = Vector4.zero;
        newRing.w = 1;
        colissionRings.Add(newRing);
    }

    public void ClearCircle(){

        colissionRings.RemoveAt(colissionRings.Count - 1);
    }

    public bool CheckInside(Vector3 position){
        return (position - transform.position + mainRing).sqrMagnitude < Mathf.Pow(mainRingRadius, 2) ? true : false;
    }

    public bool CheckTouching(Vector3 position){
        foreach (Vector4 ring in colissionRings) {
            tempVec3 = ring;
            if((position - (transform.position + tempVec3)).sqrMagnitude < Mathf.Pow(ring.w, 2)){
                return true;
            }
        }
        return false;
    }

}
