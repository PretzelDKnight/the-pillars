﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableKeeper : MonoBehaviour
{
    static public List<CollectableKeeper> activeCollectables = new List<CollectableKeeper>();

    private void OnEnable() {
        activeCollectables.Add(this);
    }

    private void OnDisable() {
        activeCollectables.Remove(this);
    }

    private void OnDestroy() {
        if(activeCollectables.Contains(this))activeCollectables.Remove(this);
    }
}
