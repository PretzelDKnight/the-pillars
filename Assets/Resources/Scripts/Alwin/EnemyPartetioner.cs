﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPartetioner : MonoBehaviour
{

    static public EnemyPartetioner instance;

    public Vector2 areaSize;
    public Vector2Int divissionCount;
    public EnemyBook[] enemyListCollection;

    Vector2Int tempVec = Vector2Int.one;
    int arraySize;

    public struct EnemyBook
    {
        public bool open;
        public List<AdvEnemyAIBase> enemyList;

        public EnemyBook(List<AdvEnemyAIBase> newList){
            enemyList = newList;
            open = true;
        }

    }

    public int GetIndex(Vector2Int indexPos){
        return (indexPos.y * divissionCount.x) + indexPos.x;
    }

    public int GetIndex(Vector3 pos){
        Vector2Int value = GetIndexVec(pos);
        return (value.y * divissionCount.x) + value.x;
    }

    public Vector2Int GetIndexVec(Vector3 pos){
        
        float xSegmentValue = areaSize.x/divissionCount.x;
        float zSegmentValue = areaSize.y/divissionCount.y;

        int xCount = (int)((pos.x - transform.position.x)/ xSegmentValue);
        int zCount = (int)((pos.z - transform.position.z)/ zSegmentValue);

        if(xCount < 0 || xCount > divissionCount.x - 1 || zCount < 0 || zCount > divissionCount.y - 1){
            return Vector2Int.one * -1;
        }

        return new Vector2Int(xCount, zCount);
    }


    private void Awake() {
        instance = this;
    }

    private void Start() {
        arraySize = divissionCount.x * divissionCount.y;
        enemyListCollection = new EnemyBook[arraySize];
    }

    public void AddToBlock(int index, AdvEnemyAIBase enemy){
        if(!enemyListCollection[index].open){
                List<AdvEnemyAIBase> newList = new List<AdvEnemyAIBase>(3);
                enemyListCollection[index] = new EnemyBook(newList);
            }
        if(!enemyListCollection[index].enemyList.Contains(enemy))enemyListCollection[index].enemyList.Add(enemy);
    }

    public void RemoveFromBlock(int index, AdvEnemyAIBase enemy){
         if(!enemyListCollection[index].open){
                enemyListCollection[index] = new EnemyBook(new List<AdvEnemyAIBase>(3));
                return;
            }
        if(enemyListCollection[index].enemyList.Contains(enemy))enemyListCollection[index].enemyList.Remove(enemy);
    }

    public int[] GetNineBlockContent(Vector3 pos){
        List<int> enemySendBooksIndex = new List<int>();
        Vector2Int coordinateIndex = GetIndexVec(pos);
        int aiIndex = GetIndex(coordinateIndex);
        if(aiIndex < 0){
            Debug.Log("Out of bound..");   
            return null;
        }
        for (int i = 0; i < 3; i++)
        {
            for (int n = 0; n < 3; n++)
            {
                tempVec.x = coordinateIndex.x + (-1 + n);
                tempVec.y = coordinateIndex.y + (-1 + i);
                int newCellIndex = GetIndex(tempVec);

                try
                {
                    if(newCellIndex >= 0 && newCellIndex < divissionCount.x * divissionCount.y){
                        bool test = enemyListCollection[newCellIndex].open;
                    }
                }
                catch (System.Exception)
                {
                    Debug.LogError("Cell index error : " + newCellIndex + " Vec Cord : " + coordinateIndex + " Shitf Vec : " + tempVec);
                    Debug.Break();
                    throw;
                }

                if(newCellIndex > -1 && newCellIndex < arraySize && enemyListCollection[newCellIndex].open)
                {
                    EnemyBook blockBook = enemyListCollection[newCellIndex];
                    if(blockBook.enemyList.Count > 0)enemySendBooksIndex.Add(newCellIndex);
                }
            }
        }
        return enemySendBooksIndex.ToArray();
    }


     void OnDrawGizmosSelected() {
        Gizmos.color = Color.white;

        // linet to x bound
        Gizmos.DrawLine(transform.position, transform.position+ transform.right * areaSize.x);
        // line y bound
        Gizmos.DrawLine(transform.position, transform.position+ transform.forward * areaSize.y);
        // linet to x bound
        Gizmos.DrawLine(transform.position + transform.forward * areaSize.y, transform.position + (transform.forward * areaSize.y) + (transform.right * areaSize.x));
        // line y bound
        Gizmos.DrawLine(transform.position + transform.right * areaSize.x, transform.position + (transform.right * areaSize.x) + (transform.forward * areaSize.y));
    
        float xDivWidth = areaSize.x/(float)divissionCount.x;
        float yDivWidth = areaSize.y/(float)divissionCount.y;

        for (int i = 0; i < divissionCount.x; i++)
        {
            Gizmos.DrawLine(transform.position + Vector3.right * xDivWidth * i, transform.position + (Vector3.right * xDivWidth * i) + (transform.forward * areaSize.y));
        }

        for (int i = 0; i < divissionCount.y; i++)
        {
             Gizmos.DrawLine(transform.position + Vector3.forward * yDivWidth * i, transform.position + (Vector3.forward * yDivWidth * i) + (transform.right * areaSize.x));
        }
    }

}
