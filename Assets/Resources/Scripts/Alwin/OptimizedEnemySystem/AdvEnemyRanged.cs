﻿using UnityEngine;

public class AdvEnemyRanged : AdvEnemyAIBase
{

    public Cannon weapon;
    Animator anim;

    public void Awake()
    {
        anim = GetComponentInChildren<Animator>();
    }

    protected override void Init(){
        base.Init();
        enemyType = EnemyTypes.ranged;
    }

    public override bool RunBrain(){

        if(base.RunBrain()) return true;

        SwitchState(AIActionState.move);
        selectedAction = AIActionState.move;
        return false;

    }
    
        public void SwitchState(AIActionState newMindState){

        if(selectedAction != newMindState){
            selectedAction = newMindState;

            switch (selectedAction)
            {

                case AIActionState.idle:
                    anim.SetBool("isAttacking", false);
                break;

                case AIActionState.attack:
                    anim.SetBool("isAttacking", true);
                break;

                case AIActionState.damagePillar:
                    anim.SetBool("isAttacking", true);
                break;

                case AIActionState.move:
                    anim.SetBool("isAttacking", false);
                break;

                default:
                    
                break;
            }
        
        }
    }

    protected override void Attack(){
        SwitchState(AIActionState.attack);
        weapon.Fire(playerObject.transform);
        anim.SetTrigger("Fire");
    }


}
