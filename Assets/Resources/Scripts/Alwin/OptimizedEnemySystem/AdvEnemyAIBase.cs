﻿using UnityEngine;

public class AdvEnemyAIBase : MonoBehaviour
{

    [Header("Ctrl")]
    public bool canChase;
    public bool canClimbe;

    [ShowOnly] public bool isAlive;
    [HideInInspector] public Rigidbody rigidbody;
    public int health = 100;
    public float chaceFactor;
    public float moveSpeed;
    public float moveFactor;
    public float maxVelocity;
    public float climbeSpeed = 1;
    public float rotationSpeed;
    public float flockFactor;
    public float knockBackTime;
    [Header("Attack")]
    public float attackRange;
    public float attackDelay;
    
    [Header("Particle System")]
    [SerializeField] ParticleSystem frozenEffect;
    [SerializeField] ParticleSystem frozenBurning;

    // custom physics
    [HideInInspector]public Vector3 velocity;
    [HideInInspector]public Vector3 colissionShift;
    [HideInInspector]public bool isFlockable;
    [HideInInspector]public bool inTopOfEnemy;
    public bool climbing;


    protected Transform playerObject;
    protected Vector3 tempVec;
    Vector3 colissionAvoidanceVector;
    protected float fallingSpeed;
    protected float recKnockBack;
    protected float nextAttackTime;
    protected float lastUpdate = 0;
    protected float lastFixedUpdate = 0;
    protected float freezeTime;
    protected float updateDeltaTime;
    protected float fixedUdateDeltaTime;
    protected float generalDelay;
    public ColliderRings recentColiderSection;
    float nextCellUpdateTime;
    float cellUpdateRate = 8;
    int recHealth;
    bool delay;
    

    [ShowOnly]public EnemyTypes enemyType;
    [ShowOnly]public AIActionState selectedAction;
    [ShowOnly]public int lastIndex = -1;
    [ShowOnly]public string runningStateInfo;
    


    public enum EnemyTypes
    {
        melee,
        ranged
    }

    public enum AIActionState
    {
        frozen,
        idle,
        move,
        attack,
        damagePillar,
        climb
    }
    #region Inheritable_Functions
    public virtual void Start()
    {
        Init();
    }

    public virtual void OnEnable(){
        Init();
    }

    public virtual void AnimListener(int sourceID)
    {

    }


    protected virtual void Init(){
        recHealth = health;
        playerObject = GameObject.FindGameObjectWithTag("Player").transform;
        rigidbody = GetComponent<Rigidbody>();
        AdvEnemyManager.instance.AddEnemy(this);
        rigidbody.velocity = Vector3.zero;
        velocity = Vector3.zero;
        lastIndex = -1;
        CheckInxed();
        fallingSpeed = 0;
        freezeTime = -1;
        recKnockBack = 0;
        isAlive = true;
        generalDelay = -1f;
        recentColiderSection = null;
        
        // shutting particle systems
        frozenEffect.Stop();
    }

    protected virtual void Attack()
    {

    }



    protected void CheckInxed(){
        int blockIndex = EnemyPartetioner.instance.GetIndex(transform.position);
        if(blockIndex < 0)return;
        if(blockIndex != lastIndex){
            if(lastIndex != -1){
                EnemyPartetioner.instance.RemoveFromBlock(lastIndex, this);
            }
            EnemyPartetioner.instance.AddToBlock(blockIndex, this);
            lastIndex = blockIndex;
        }
        nextCellUpdateTime = Time.time + 1/ cellUpdateRate;
    }

    protected void DeathCheck()
    {
        if (recHealth <= 0)
        {
            if (isAlive)
                KillEnemy();
        }
    }

    protected virtual void KillEnemy()
    {
        AdvEnemyManager.instance.RemoveEnemy(this);
        WaveManager.instance.KillUp();
        ObjectPooling.PoolDestroy(gameObject);
        EnemyPartetioner.instance.RemoveFromBlock(lastIndex, this);
        isAlive = false;
    }


    protected virtual void OnDisable() {
        KillEnemy();
    }

    #endregion

    #region Externally_Accesible

    public virtual void Freeze(float freezeTime)
    {
        this.freezeTime = freezeTime;
        frozenEffect.Play();
    }

    public virtual void TakeDamage(int damage, Vector3 knockBack)
    {
        recHealth -= damage;
        recKnockBack = knockBackTime;
        rigidbody.AddForce(knockBack, ForceMode.Impulse);
        DeathCheck();
    }

    public virtual void CheckColission(){
        for (int i = 0; i < ColiderRingKeeper.instance.colliderSystems.Count; i++)
        {
            if(ColiderRingKeeper.instance.colliderSystems[i].CheckInside(transform.position)){
                recentColiderSection = ColiderRingKeeper.instance.colliderSystems[i];
                return;
            }
        }
        recentColiderSection = null;
    }

    public virtual bool RunBrain(){
        // time delta calculation
        updateDeltaTime = Time.time - lastUpdate;
        lastUpdate = Time.time;

        // for any action which is to be continued for a time
        if(generalDelay > -1){
            generalDelay -= updateDeltaTime;
            delay = true;
            return true;
        }else delay = false;
       
        // check neibour presence
        CheckColission();

        // make decesion
        //selectedAction = AIActionState.move;
       

        // partetioning check
        if(Time.time > nextCellUpdateTime)CheckInxed();

        // revert non- flocking paramater
        if(selectedAction != AIActionState.damagePillar && !isFlockable){
            isFlockable = true;
        }

        // climb
        if(climbing) {
            selectedAction = AIActionState.climb;
            return true;
        }
 
        // frozen
        if(freezeTime > 0){
            selectedAction = AIActionState.frozen;
            freezeTime -= updateDeltaTime;
            if(freezeTime < 0)KillEnemy();
            return true;
        }

        // attacking section
        else if((playerObject.transform.position - transform.position).sqrMagnitude < (attackRange * attackRange)){
            if(Time.time > nextAttackTime){
                Attack();
                selectedAction = AIActionState.attack;
                nextAttackTime = Time.time + attackDelay;
            }
            return true;
        }


        return false;
    }

    public virtual void ExecutePhysics(){
        runningStateInfo = "";
        /*
        if(delay){
            StopMove();
            return;
        }
        */
        
        fixedUdateDeltaTime = Time.time - lastFixedUpdate;
        lastFixedUpdate = Time.time;
        if(recKnockBack > 0){
            recKnockBack -= Time.fixedDeltaTime;
            return;
        }
        // calculates flocking and physics
        Vector3 flockVel = Flocker.instance.FlockCalculation(this);

        // do action
        switch (selectedAction)
        {
            // direct move
            case AIActionState.move :
                // avoidance velocity
                if(recentColiderSection &&
                    !(SegmentedPillar.playerOnPillar ? SegmentedPillar.playerOnPillar.transform.Equals(recentColiderSection.rootOwner) : false)){
                    
                    colissionAvoidanceVector += AlMathf.GetMagnitudePlanar(transform.position + (velocity) - (recentColiderSection.transform.position + recentColiderSection.mainRing)) * 2;
                }
                else{
                    colissionAvoidanceVector *= 0.6f;
                }
                
                if(canChase)velocity += colissionAvoidanceVector + ((inTopOfEnemy ? 0 : 1) * flockVel * fixedUdateDeltaTime * flockFactor) + AlMathf.Clamp(playerObject.position - transform.position - velocity, moveSpeed) * fixedUdateDeltaTime * moveFactor;
                else velocity = Vector3.zero;

                velocity = AlMathf.Clamp(velocity, maxVelocity);
                runningStateInfo = "[Physics]: Moving..." + velocity.magnitude;
                velocity.y = 0;
                if(canChase)rigidbody.rotation = Quaternion.LookRotation(velocity, Vector3.up); 
                if(canClimbe){
                    // dont fall if on top of enemy
                    if(!inTopOfEnemy)velocity.y = rigidbody.velocity.y;
                    else velocity = Vector3.up * climbeSpeed;
                }
                if(velocity.sqrMagnitude < 0.001f)return;
                rigidbody.velocity = velocity;
                rigidbody.position -= colissionShift;
                colissionShift = Vector3.zero;
                
            break;

            // attack
            case AIActionState.attack :
                StopMove();
                tempVec = playerObject.position - transform.position;
                tempVec.y = 0;
                rigidbody.rotation = Quaternion.LookRotation(tempVec, Vector3.up); 

            break;

            // frozen
            case AIActionState.frozen :
                StopMove();
            break;

            // stay idle
            case AIActionState.idle :
                StopMove();
            break;

            // damage pillar
            case AIActionState.damagePillar :
                StopMove();
            break;

            // clinb over
            case AIActionState.climb :
                rigidbody.velocity = Vector3.up * 3f;
            break;

            default:
            break;
        }
    }

    public virtual void MaintainPhysics(){
        if(recKnockBack > 0){
            recKnockBack -= Time.fixedDeltaTime;
            return;
        }
        if(velocity.sqrMagnitude < 0.001f)return;
        velocity.y = rigidbody.velocity.y;
        rigidbody.velocity = velocity;
    }

     public virtual void StopMove(){
        velocity = Vector3.zero;
        velocity.y = rigidbody.velocity.y;
        rigidbody.velocity = velocity;
    }

    #endregion


}
