﻿using System.Collections.Generic;
using UnityEngine;

public class AdvEnemyManager : MonoBehaviour
{
    static public AdvEnemyManager instance;
    List<AdvEnemyAIBase> aiCollection = new List<AdvEnemyAIBase>();

    #region Public_Parameters
    public int aiUpdationWidth = 20;
    public float updateRate;
    #endregion

    #region Private_Parameters
    float nextUpdateTime;
    int aiUpdationIndex;
    int aiUpdationBatch;
    int stepStart;
    int stepSize = 4;
    int nextExecuteIndex;
    #endregion

    public void AddEnemy(AdvEnemyAIBase enemy)
    {
        if(!aiCollection.Contains(enemy))aiCollection.Add(enemy);
    }

    public void RemoveEnemy(AdvEnemyAIBase enemy)
    {
        if(aiCollection.Contains(enemy))aiCollection.Remove(enemy);
    }

    private void Awake() {
       instance = this;
    }

    void Update()
    {
        // decession making
        aiUpdationBatch++;
        while (aiUpdationIndex < aiCollection.Count && aiUpdationIndex < (aiUpdationWidth * aiUpdationBatch))
        {
            if(aiCollection[aiUpdationIndex].isAlive){
                aiCollection[aiUpdationIndex].RunBrain();
            }
            aiUpdationIndex++;
        }
        if(aiUpdationIndex >= aiCollection.Count)
        {
            aiUpdationBatch = 0;
            aiUpdationIndex = 0;
        }
    }

    void FixedUpdate()
    {
        nextExecuteIndex = stepStart;
        for (int i = stepStart; i < aiCollection.Count; i++)
        {
            if(i < nextExecuteIndex)aiCollection[i].MaintainPhysics();
            else {
                aiCollection[i].ExecutePhysics();
                nextExecuteIndex += stepSize;
            }
        }
        
        if( stepStart < stepSize - 1)stepStart++;
        else stepStart = 0;
    }
}
