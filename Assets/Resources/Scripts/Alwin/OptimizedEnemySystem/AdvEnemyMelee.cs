﻿using UnityEngine;

public class AdvEnemyMelee : AdvEnemyAIBase
{
    [Header("Pillar Damage")]
    [SerializeField] float pillarDamageDelay;
    [SerializeField] int pillarDamageAmount;
    float nextPillarDamageTime;

    Animator meleeAnim;

    public void Awake()
    {
        meleeAnim = GetComponentInChildren<Animator>();
    }

    public override bool RunBrain()
    {

        if (base.RunBrain()) return true;

        // damage the pillar
        else if (recentColiderSection &&
        SegmentedPillar.playerOnPillar &&
        SegmentedPillar.playerOnPillar.transform.Equals(recentColiderSection.rootOwner) &&
        recentColiderSection.CheckTouching(transform.position))
        {
            isFlockable = false;
            if (Time.time > nextPillarDamageTime)
            {
                SwitchState(AIActionState.damagePillar);
                nextPillarDamageTime = Time.time + pillarDamageDelay;
            }
            return true;
        }

        SwitchState(AIActionState.move);
        meleeAnim.SetBool("isAttacking", false);
        return false;

    }

    protected override void Attack()
    {
        SwitchState(AIActionState.attack);
        //generalDelay = 1;
    }

    public void SwitchState(AIActionState newMindState){

        if(selectedAction != newMindState){
            selectedAction = newMindState;

            switch (selectedAction)
            {

                case AIActionState.idle:
                    meleeAnim.SetBool("isAttacking", false);
                break;

                case AIActionState.attack:
                    meleeAnim.SetBool("isAttacking", true);
                break;

                case AIActionState.damagePillar:
                    meleeAnim.SetBool("isAttacking", true);
                break;

                case AIActionState.move:
                    meleeAnim.SetBool("isAttacking", false);
                break;

                default:
                    
                break;
            }
        
        }
    }

    public override void AnimListener(int sourceID)
    {
        switch (selectedAction)
        {
            // damage pillar
            case AIActionState.damagePillar:
                if (sourceID == 0 && SegmentedPillar.playerOnPillar){
                    SegmentedPillar.playerOnPillar.Damage(pillarDamageAmount);
                }
                else if (sourceID == 1){
                    SwitchState(AIActionState.idle);
                }
            break;
            // attack player
            case AIActionState.attack:
                if (sourceID == 0){
                    playerObject.GetComponent<Player>().RecieveDamage(transform.position);
                }
                else if (sourceID == 1){
                    SwitchState(AIActionState.idle);
                }
            break;

        }
    }
}
