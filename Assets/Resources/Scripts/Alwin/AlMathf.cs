﻿using UnityEngine;

static public class AlMathf
{
    static public Vector3 GetMagnitudePlanar(Vector3 dir){
        float xVal = dir.x > 0 ? dir.x : -dir.x;
        float zVal = dir.z > 0 ? dir.z : -dir.z;
        float ratio = 1/ (xVal > zVal ? xVal : zVal);
        ratio = ratio * (1.29289f - (xVal + zVal) * ratio * 0.29289f);
        return new Vector3(dir.x * ratio, 0, dir.z * ratio);
    }

    static public Vector3 GetMagnitude(Vector3 dir){
        float xVal = dir.x > 0 ? dir.x : -dir.x;
        float yVal = dir.y > 0 ? dir.y : -dir.y;
        float zVal = dir.z > 0 ? dir.z : -dir.z;
        float ratio = 1/ (xVal > zVal ? (xVal > yVal ? xVal : yVal) : (zVal > yVal ? zVal : yVal));
        ratio = ratio * (1.73205080757f - (xVal + zVal + yVal) * ratio * 0.42264973081f);
        return new Vector3(dir.x * ratio, dir.y * ratio, dir.z * ratio);
    }

    static public Vector3 Clamp(Vector3 dir, float clamp){
        if(dir.sqrMagnitude > clamp * clamp){
            return GetMagnitudePlanar(dir) * clamp;
        }
        return dir;
    }

}
