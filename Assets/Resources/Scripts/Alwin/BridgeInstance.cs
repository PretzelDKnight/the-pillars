﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeInstance : MonoBehaviour
{
    public float curveScanStep = 0.05f;
    public float minPlankSpacing = 0.3f;
    public Transform start;
    public Transform weightPoint;
    public Transform end;
    public Transform plankInstanceContainer;


    [Header("Runtime")]
    public float updateShiftThrushold;
    public float updateRate;

    bool isActive = true;

    float curveLength;
    float directLength;
    float nextUpdate;

    [HideInInspector]public float startToWeightLength;
    [HideInInspector]public float endToWeightLength;

    // mobility record
    Vector3 startPoint_Rec;
    Vector3 endPoint_Rec;
    Vector3 weightPoint_Rec;

    // wobble rec
    Vector3 targetWeightPosition;
    Vector3 weightVelocity;
    

    GameObject plank;

    List<BridgePlank> plankCollection = new List<BridgePlank>();

[System.Serializable]
    public struct BridgePlank
    {
        public Rigidbody rb;
        public GameObject plankObject;
        public float t;

        public BridgePlank(GameObject plankObject, float t){
            this.t = t;
            this.plankObject = plankObject;
            this.rb = plankObject.GetComponent<Rigidbody>();
        }

    }

    private void OnEnable() {
        plankInstanceContainer = transform.GetChild(transform.childCount - 1);
    }

    private void Start() {
        Generate();
    }

    private void Update() {

        // move point to woble
        if(isActive){
            weightVelocity += (targetWeightPosition - weightPoint.position) * Time.deltaTime;
            weightVelocity += -weightVelocity * 0.99f * Time.deltaTime;
            weightPoint.Translate(weightVelocity * Time.deltaTime, Space.World);

            bool positionShifted = false;
            bool checkMid = false;

            if((start.position - startPoint_Rec).sqrMagnitude > Mathf.Pow(updateShiftThrushold, 2)){
                startPoint_Rec = start.position;
                positionShifted = true;
                checkMid = true;
            }

            if((end.position - endPoint_Rec).sqrMagnitude > Mathf.Pow(updateShiftThrushold, 2)){
                endPoint_Rec = end.position;
                positionShifted = true;
            }

            if((weightPoint.position - weightPoint_Rec).sqrMagnitude > Mathf.Pow(updateShiftThrushold, 2)){
                weightPoint_Rec = weightPoint.position;
                positionShifted = true;
                checkMid = true;
            }

            if(Time.time > nextUpdate && positionShifted){
                CheckBridgeCondition();

                for (int i = 0; i < plankCollection.Count; i++)
                {
                    plankCollection[i].rb.transform.position = PointOnCurve(plankCollection[i].t);
                }

                nextUpdate = Time.time + 1/updateRate;
                if(checkMid)SetWeighttarget();
            }
            
            
        }
    }

    void CheckBridgeCondition(){
        if((start.position - end.position).sqrMagnitude > curveLength){
            isActive = false;
            BreakBridge();
        }
    }

    void BreakBridge(){
        for (int i = 0; i < plankCollection.Count; i++)
        {
            plankCollection[i].rb.isKinematic = false;
            plankCollection[i].rb.useGravity = true;
            plankCollection[i].rb.AddForce(Random.insideUnitSphere, ForceMode.Impulse);
        }
    }

    void SetWeighttarget(){
        Vector3 directLine = start.position - end.position;
        targetWeightPosition = end.position + (directLine * 0.5f) + Vector3.down;
        float error = Mathf.Infinity;

        int countLock = 180;
        while (error > 0.0001f && countLock > 0)
        {
            countLock--;
            Vector3 dirWStart = targetWeightPosition - start.position;
            targetWeightPosition = start.position + dirWStart.normalized * startToWeightLength;

            Vector3 dirWEnd = targetWeightPosition - end.position;
            targetWeightPosition = end.position + dirWEnd.normalized * endToWeightLength;

            error = Mathf.Abs((endToWeightLength + startToWeightLength) - (endToWeightLength + (targetWeightPosition - start.position).magnitude));
        }
    }

    public void Generate()
    {
        if(!start || !end || !weightPoint)return;
        curveLength = Mathf.Pow(FindCurveLength(),2);

        startPoint_Rec = start.position;
        endPoint_Rec = end.position;
        weightPoint_Rec = weightPoint.position;

        startToWeightLength = Vector3.Distance(start.position, weightPoint.position);
        endToWeightLength = Vector3.Distance(end.position, weightPoint.position);

        // set the weight point there
        SetWeighttarget();
        
        weightPoint.position = targetWeightPosition;
        weightVelocity = Vector3.up;

        plank = Resources.Load("Prefabs/Prototype/Plank") as GameObject;

        Clear();

        plankCollection.Clear();
        Vector3 lastPoint = start.position;
        float t = 0;
       CreatePlankAt(t, lastPoint);

        while (t < 1){
            Vector3 newPoint = PointOnCurve(t);
            // if the placing is right
            // place the plank
            if((lastPoint - newPoint).sqrMagnitude >= (minPlankSpacing * minPlankSpacing)){
                CreatePlankAt(t, newPoint);
                lastPoint = newPoint;
            }
            t += curveScanStep;
        }


    }

    float FindCurveLength(){
        Vector3 lastPoint = start.position;
        float t = 0;
        float curveStepIntegral = 0;
        while (t < 1){
            t += curveScanStep;
            Vector3 newPoint = PointOnCurve(t);
            // if the placing is right
            // place the plank
            Vector3 dir = lastPoint - newPoint;
            curveStepIntegral += dir.magnitude;
            lastPoint = newPoint;
            
        }
        return curveStepIntegral;
    }

    void CreatePlankAt(float t, Vector3 pos){
        Vector3 dir = PointOnCurve(t - minPlankSpacing) - PointOnCurve(t + minPlankSpacing);
        GameObject plankInstance = Instantiate(plank, pos, Quaternion.LookRotation(dir.normalized, Vector3.up), plankInstanceContainer);
        BridgePlank newPlank = new BridgePlank(plankInstance, t);
        plankCollection.Add(newPlank);
    }

    public void Clear(){
        // clear old planks if exists
        BoxCollider[] childrenObjects = plankInstanceContainer.GetComponentsInChildren<BoxCollider>();
        for (int i = childrenObjects.Length -1; i >= 0; i--)
        {
           DestroyImmediate(childrenObjects[i].gameObject);
        }
        plankCollection.Clear();
    }

    public Vector3 PointOnCurve(float t){
        Vector3 p1p2 = start.position + (weightPoint.position - start.position)* t;
        Vector3 p2p3 = weightPoint.position + (end.position - weightPoint.position)* t;
        return p1p2 + (p2p3 - p1p2) * t;
    }

    public void AddWobble(Vector3 force){
        weightVelocity += force;
    }

}
