﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderBoundDisplay : MonoBehaviour
{
    public Color color;

    public SphereCollider sphereCollider;

    private void OnDrawGizmos() {
        Gizmos.color = color;
        if(sphereCollider)Gizmos.DrawSphere(transform.position, sphereCollider.radius);
    }
}
