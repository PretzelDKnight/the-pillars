﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColiderRingKeeper : MonoBehaviour
{
    public List<ColliderRings> colliderSystems;

    public static ColiderRingKeeper instance;

    private void Awake() {
        instance = this;
    }

    public void AddSystem(ColliderRings colSystem){
        colliderSystems.Add(colSystem);
    }

    public void RemoveSystem(ColliderRings colSystem){
        colliderSystems.Remove(colSystem);
    }

    
}
