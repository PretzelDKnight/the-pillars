﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
 
      #if UNITY_EDITOR
 [CustomPropertyDrawer(typeof(ShowOnlyAttribute))]
 public class ShowOnlyDrawer : PropertyDrawer
 {


     public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
     {
         string valueStr;
 
         switch (prop.propertyType)
         {
             case SerializedPropertyType.Integer:
                 valueStr = prop.intValue.ToString();
                 break;
             case SerializedPropertyType.Boolean:
                 valueStr = prop.boolValue.ToString();
                 break;
             case SerializedPropertyType.Float:
                 valueStr = prop.floatValue.ToString("0.00000");
                 break;
             case SerializedPropertyType.String:
                 valueStr = prop.stringValue;
                 break;
             case SerializedPropertyType.Enum:
                    valueStr = prop.enumDisplayNames[prop.intValue];
                 break;
            case SerializedPropertyType.Vector2:
                    valueStr = "[" + prop.vector2Value.x.ToString() + "," + prop.vector2Value.y.ToString() + "]";
                 break;
            
             default:
                 valueStr = "(not supported)";
                 break;
         }
 
         EditorGUI.LabelField(position,label.text, valueStr);
     }
}
      #endif