﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;



[ExecuteInEditMode]
public class VisualControl : MonoBehaviour
{
    public Color SkyColor;
    public Color EarthColor;
    public Color ShadowColor;


    public void Start(){
        LoadColors();
    }

    private void OnEnable() {
        UpdateGlobalShaderVariables();
    }

    public void SaveColors(){
        Scene activeScene =SceneManager.GetActiveScene();
        SaveColors(SkyColor, EarthColor, ShadowColor, activeScene.name);
        Debug.Log("Data Saved..");
    }

    public void LoadColors(){
        Scene activeScene =SceneManager.GetActiveScene();
        SaveShaderGlobalData colorData = new SaveShaderGlobalData();
        Load(activeScene.name, ref colorData);
        SkyColor = colorData.skyColor.ToColor();
        Shader.SetGlobalColor("TOON_SKY_COLOR", colorData.skyColor.ToColor());

        EarthColor = colorData.earthColor.ToColor();
        Shader.SetGlobalColor("TOON_GROUND_COLOR", colorData.earthColor.ToColor());

        ShadowColor = colorData.shadowColor.ToColor();
        Shader.SetGlobalColor("TOON_SHADOW_COLOR", colorData.shadowColor.ToColor());
    }

    void UpdateGlobalShaderVariables(){
        Shader.SetGlobalColor("TOON_SKY_COLOR", SkyColor);
        Shader.SetGlobalColor("TOON_GROUND_COLOR", EarthColor);
        Shader.SetGlobalColor("TOON_SHADOW_COLOR", ShadowColor);
    }


     public static void SaveColors(Color skyColor, Color earthColor, Color shadowColor, string sceneName){
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/"+  sceneName.Replace(" ", "") + "_ShaderGlobalColors.dat", FileMode.OpenOrCreate);
        SaveShaderGlobalData saveData = new SaveShaderGlobalData(skyColor, earthColor, shadowColor);
        bf.Serialize(file, saveData);
        file.Close();
    }

    
    public static void Load(string sceneName, ref SaveShaderGlobalData colorData){
        if (File.Exists (Application.persistentDataPath + "/"+  sceneName.Replace(" ", "") + "_ShaderGlobalColors.dat")) {
            BinaryFormatter bf = new BinaryFormatter ();
            FileStream file = File.Open(Application.persistentDataPath + "/"+  sceneName.Replace(" ", "") + "_ShaderGlobalColors.dat", FileMode.Open);
            colorData = (SaveShaderGlobalData)bf.Deserialize(file);
            file.Close();
        } else {
           Debug.Log("Data Not Found");
        }
    }
    
[System.Serializable]
    public class SaveShaderGlobalData{
        public DataColor skyColor;
        public DataColor earthColor;
        public DataColor shadowColor;

        public SaveShaderGlobalData(){

        }

        public SaveShaderGlobalData(Color skyColor, Color earthColor, Color shadowColor){
            this.skyColor = new DataColor(skyColor);
            this.earthColor = new DataColor(earthColor);
            this.shadowColor = new DataColor(shadowColor);
        }

    }
[System.Serializable]
    public class DataColor{
        float r;
        float g;
        float b;
        
        public DataColor(Color color){
            r = color.r;
            g = color.g;
            b = color.b;
        }

        public Color ToColor(){
            return new Color(r,g,b,1);
        }
    }

}
