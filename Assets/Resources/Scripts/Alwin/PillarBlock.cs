﻿using UnityEngine;

public class PillarBlock : MonoBehaviour
{
    SegmentedPillar pillarParent;
    [SerializeField] float maxHealth = 2000;
    [SerializeField] float maxMaterialDamage = 0.5f;
    public Collider mainCollider;
    public Material material;
    public ColliderRings colliderRings;

    float healthRec;

    public void Init(){
        colliderRings = this.GetComponent<ColliderRings>();
        if(colliderRings){
            colliderRings.SetOwner(GetComponentInParent<SegmentedPillar>().transform);
            colliderRings.Init();
        }
    }

    private void Start() {
        mainCollider = GetComponent<Collider>();
        material = GetComponent<MeshRenderer>().material;
        healthRec = maxHealth;
    }

    public void Damage(int damageValue) {
        healthRec -= damageValue;
        pillarParent.HitEffectAt(transform.position);
        float effect = 1 - (healthRec/maxHealth);
        material.SetFloat("_Damage", maxMaterialDamage * effect);
        if (healthRec <= 0){
           CLoseObject();
        }
    }

    void CLoseObject(){
        pillarParent.RegisterSegmentLost();
        pillarParent.DestroyEffectAt(transform.position);
        colliderRings.Terminate();
        gameObject.SetActive(false);
    }

    public void SetContrtolObject(SegmentedPillar pillarCtrl) {
        pillarParent = pillarCtrl;
    }

}
