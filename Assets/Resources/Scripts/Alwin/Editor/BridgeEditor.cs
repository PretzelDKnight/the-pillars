﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

[CustomEditor(typeof(BridgeInstance))]
public class BridgeEditor : Editor
{
    BridgeInstance bridge;

    private void OnSceneGUI() {

        if(!bridge.start || !bridge.end || !bridge.weightPoint)return;

     //   EditorGUIUtility.ObjectContent(Object obj, System.Type objType)


        Event guiEvent = Event.current;

        Draw();

        if(guiEvent.type == EventType.Layout){
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        }

        
    }

    void Draw(){
            
        Handles.color = Color.white;
        
        bridge.start.position = Handles.FreeMoveHandle(bridge.start.position, Quaternion.identity, 0.2f, Vector3.zero, Handles.SphereHandleCap);
        bridge.end.position = Handles.FreeMoveHandle(bridge.end.position, Quaternion.identity, 0.2f, Vector3.zero, Handles.SphereHandleCap);
        Handles.color = Color.green;
        bridge.weightPoint.position = Handles.FreeMoveHandle(bridge.weightPoint.position, Quaternion.identity, 0.2f, Vector3.zero, Handles.SphereHandleCap);
    

        float t = 0;
        Vector3 lastPoint = bridge.start.position;
        Handles.color = Color.green;
        while (t < 1)
        {
            Vector3 newPoint = bridge.PointOnCurve(t);
            if((lastPoint - newPoint).sqrMagnitude >= (bridge.minPlankSpacing * bridge.minPlankSpacing)){
                Handles.DrawLine(lastPoint, newPoint);
                lastPoint = newPoint;
            }
            t += bridge.curveScanStep;
        }

  }

    public override void OnInspectorGUI(){

        EditorGUIUtility.TrTextContent("name", "Test", "Good Script", new Texture2D(10,10));
        base.OnInspectorGUI();
        if(GUILayout.Button("Create")){
            bridge.Generate();
        }
        else if(GUILayout.Button("Clear")){
            bridge.Clear();
        }
    }

    private void OnEnable() {
        bridge = target as BridgeInstance;
    }

}
