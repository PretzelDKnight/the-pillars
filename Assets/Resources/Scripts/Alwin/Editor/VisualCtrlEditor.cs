﻿using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
[CustomEditor(typeof(VisualControl))]
public class VisualCtrlEditor : Editor
{
    VisualControl instance;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        instance = (VisualControl)target;
         UpdateGlobalShaderVariables();

        if(GUILayout.Button("Save")){
           instance.SaveColors();
        }
        else if(GUILayout.Button("Load")){
           instance.LoadColors();
        }
        
    }

    void UpdateGlobalShaderVariables(){

        Shader.SetGlobalColor("TOON_SKY_COLOR", instance.SkyColor);
        Shader.SetGlobalColor("TOON_GROUND_COLOR", instance.EarthColor);
        Shader.SetGlobalColor("TOON_SHADOW_COLOR", instance.ShadowColor);

    }




}
