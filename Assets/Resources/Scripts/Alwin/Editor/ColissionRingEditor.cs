﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ColliderRings))]
public class ColissionRingEditor : Editor
{
    public ColliderRings coliderSystem;

    Vector3 tempVec3 = Vector3.zero;
    Vector4 tempVec4 = Vector4.zero;
    Vector3 lastPoint;

    public override void OnInspectorGUI(){
        coliderSystem.rootOwner = (Transform)EditorGUILayout.ObjectField("Root", coliderSystem.rootOwner, typeof(Transform), true);
        coliderSystem.initializeOnStart = EditorGUILayout.Toggle(new GUIContent("Auto Initialize"), coliderSystem.initializeOnStart);
        if(GUILayout.Button("Create")){
            coliderSystem.AddCircle();
        }
        else if(GUILayout.Button("Clear")){
            coliderSystem.ClearCircle();
        }
    }

    private void OnSceneGUI() {

        Event guiEvent = Event.current;

        DrawCtrls();

        if(guiEvent.type == EventType.Layout){
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        }
     
    }

    void DrawCtrls(){
     //   Handles.color = Color.gray;

        // main ring of the system
        CircleCtrl(ref coliderSystem.mainRing, ref coliderSystem.mainRingRadius, Color.black, 0.15f);

        for (int i = 0; i < coliderSystem.colissionRings.Count; i++)
        {
            Vector3 lastCirclePoint = lastPoint;
            tempVec3.x = coliderSystem.colissionRings[i].x;
            tempVec3.y = coliderSystem.colissionRings[i].y;
            tempVec3.z = coliderSystem.colissionRings[i].z;
            float radi = coliderSystem.colissionRings[i].w;
            CircleCtrl(ref tempVec3, ref radi, Color.green, 0.05f);
            tempVec4 = tempVec3;
            tempVec4.w = radi;
            coliderSystem.colissionRings[i] = tempVec4;
            Handles.color = Color.white;
            if(i> 0)Handles.DrawLine(lastCirclePoint, lastPoint);
        }
        tempVec3 = coliderSystem.colissionRings[0];
        tempVec3 += coliderSystem.transform.position;
        Handles.DrawLine(tempVec3, lastPoint);

    }

    void CircleCtrl(ref Vector3 ringPoint, ref float ringRadi, Color handleColor, float handleSize){
        Handles.color = handleColor;
        Vector3 centrePoint = coliderSystem.transform.position + ringPoint;
        ringPoint = Handles.FreeMoveHandle(coliderSystem.transform.position + ringPoint, Quaternion.identity, handleSize, Vector3.zero, Handles.SphereHandleCap);
        ringPoint -= coliderSystem.transform.position;
        ringPoint.y = 0;
        Handles.DrawWireDisc(coliderSystem.transform.position + ringPoint, Vector3.up, ringRadi);
        Vector3 handlePoint = coliderSystem.transform.position + ringPoint + (-Vector3.forward * ringRadi);
        Vector3 shiftRadi = Handles.FreeMoveHandle(handlePoint, Quaternion.identity, handleSize, Vector3.zero, Handles.SphereHandleCap);
        ringRadi = (shiftRadi - (coliderSystem.transform.position + ringPoint)).magnitude;
        Handles.DrawLine(centrePoint, handlePoint);
        lastPoint = centrePoint;
    }

    private void OnEnable() {
        coliderSystem = target as ColliderRings;
    }

}
