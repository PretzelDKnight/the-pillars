﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public float strikeError = 0;
    public GameObject bulletPrefab;
    public int bulletCount = 0;
    public float fireVelocity;
    public Transform fireSpot;
    public float offsetHeight;
    public ParticleSystem gunBlastEffect;

    private void Start() {
    }

    public void Fire(Transform fireTarget){
        Vector3 dir = (fireTarget.position + (Vector3.up * offsetHeight) + (Random.insideUnitSphere * strikeError)) - fireSpot.position;
    //    transform.forward = dir;

        Vector3 H_levelPos = fireTarget.position;
        H_levelPos.y = fireSpot.position.y;

        float Rx = Vector3.Distance(H_levelPos, fireSpot.position);

        float k = 0.5f * Physics.gravity.y * Rx * Rx * (1 / (fireVelocity * fireVelocity));

        float h = fireTarget.position.y - fireSpot.position.y;

        float solution = (Rx * Rx) - (4 * (k * (k - h)));

        if(solution >= 0){

            float strikePhase = Vector3.Dot(dir.normalized, Vector3.up) < 0.5f ? 1 : -1;

            float z = (-Rx + strikePhase * Mathf.Sqrt(solution)) / (2 * k);
            fireSpot.forward = dir;
            fireSpot.localEulerAngles = new Vector3(-(Mathf.Atan(z) * 57.2958f), 0, 0);

            GameObject obj = ObjectPooling.PoolInstantiate(bulletPrefab, fireSpot.position, Quaternion.identity);
            obj.transform.forward = dir;
            obj.GetComponent<Rigidbody>().velocity = fireSpot.forward * fireVelocity;
            gunBlastEffect.Play();
        }
    }
}
