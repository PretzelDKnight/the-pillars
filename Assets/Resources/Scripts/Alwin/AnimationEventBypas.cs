﻿using UnityEngine;
using UnityEngine.Events;

public class AnimationEventBypas : MonoBehaviour
{

    public IntEvent animReciver;

    public void AnimListener(int srcID)
    {
        animReciver.Invoke(srcID);
    }

[System.Serializable]
    public class IntEvent : UnityEvent<int>{}

}
