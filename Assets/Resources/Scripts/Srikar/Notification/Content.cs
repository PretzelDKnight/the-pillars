﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Content : MonoBehaviour
{
    [SerializeField] public Image contentBG;

    [SerializeField] public Text contentTitleText;
    [SerializeField] public Text contentFillText;

    [SerializeField] public CanvasGroup contentGroup;

    float startDist;

    float lifeTime = 0;

    bool temporary;
    bool end;

    public void Assign(bool temp, string title, string fill = null)
    {
        contentTitleText.text = title;

        if(fill != null)
            contentFillText.text = fill;

        temporary = temp;
    }

    private void Start()
    {
        startDist = contentGroup.transform.localPosition.x;

        Vector3 pos = contentGroup.transform.localPosition;
        pos.x = startDist + NotificationManager.instance.contentMaxDist;
        contentGroup.transform.localPosition = pos;

        end = false;
    }

    private void Update()
    {
        if (temporary)
            lifeTime += Time.deltaTime;

        if (lifeTime >= NotificationManager.instance.contentTempTime && !end)
        {
            end = true;
            StartCoroutine(End());
        }

        float interpolation = NotificationManager.instance.contentMoveSpeed * Time.deltaTime;

        float x = Mathf.Lerp(contentGroup.transform.localPosition.x, startDist, interpolation);

        Vector3 pos = contentGroup.transform.localPosition;
        pos.x = x;

        contentGroup.transform.localPosition = pos;
    }

    public void ContentUpdate(string fill)
    {
        if (contentFillText != null)
            contentFillText.text = fill;
    }

    public void EndContent()
    {
        StartCoroutine(End());
    }

    IEnumerator End()
    {
        float time = 0;

        while (time <= 1.1f)
        {
            time += Time.deltaTime * NotificationManager.instance.contentMoveSpeed * 1.5f;

            float x = Mathf.Lerp(startDist, startDist + NotificationManager.instance.contentMaxDist, time * time);

            Vector3 pos = contentGroup.transform.localPosition;
            pos.x = x;

            contentGroup.transform.localPosition = pos;

            yield return null;
        }

        Destroy(gameObject);
    }
}
