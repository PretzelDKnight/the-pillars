﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationManager : MonoBehaviour
{
    static public NotificationManager instance = null;

    [SerializeField] GameObject contentPrefab;
    [SerializeField] GameObject subContentPrefab;
    [SerializeField] Transform contentParent;
    [SerializeField] Transform subContentParent;

    [SerializeField] Image titleBG;
    [SerializeField] Text titleText;
    
    [SerializeField] CanvasGroup titleGroup;

    [SerializeField] float titleWaitTime = 3f;

    [SerializeField] public float contentMaxDist = 400;
    [SerializeField] public float contentMoveSpeed = 1f;
    [SerializeField] public float contentFadeTime = 1f;
    [SerializeField] public float contentTempTime = 4f;

    bool busy;

    // Bool value is for identifying permanent or temporary content
    Queue<string> titleTaskQueue;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
        {
            instance = this;
        }

        Prep();
    }

    private void Update()
    {
        if (titleTaskQueue.Count > 0 && !busy)
        {
            StartCoroutine(Title(titleTaskQueue.Peek()));
        }
    }

    void Prep()
    {
        titleTaskQueue = new Queue<string>();

        ClearNotifications();

        titleGroup.alpha = 0;
        titleGroup.gameObject.SetActive(false);

        busy = false;
    }

    public void AddToTitleStack(string str)
    {
        titleTaskQueue.Enqueue(str);
    }

    public void BeginContentNotification(out Content key, string title, string fill = null, bool temp = false)
    {
        if (fill != null)
        {
            key = Instantiate(contentPrefab, contentParent).GetComponent<Content>();
            key.Assign(temp, title, fill);
        }
        else
        {
            key = Instantiate(subContentPrefab, subContentParent).GetComponent<Content>();
            key.Assign(temp, title, fill);
        }

    }

    IEnumerator Title(string str)
    {
        busy = true;

        titleGroup.gameObject.SetActive(true);
        titleGroup.alpha = 0;

        titleText.text = str;

        float time = 0;

        while(time <= 1.1f)
        {
            time += Time.deltaTime;
            titleGroup.alpha = Mathf.Lerp(0,1, time * time);
            yield return null;
        }

        yield return new WaitForSeconds(titleWaitTime);

        time = 0;

        while (time <= 1.1f)
        {
            time += Time.deltaTime;
            titleGroup.alpha = Mathf.Lerp(1, 0, time * time);
            yield return null;
        }

        titleGroup.gameObject.SetActive(false);

        yield return new WaitForSeconds(0.5f);

        busy = false;

        if(titleTaskQueue.Count > 0)
            titleTaskQueue.Dequeue();
    }

    public void ClearNotifications()
    {
        StopAllCoroutines();

        for (int i = contentParent.transform.childCount - 1; i >= 0; i--)
            contentParent.transform.GetChild(i).GetComponent<Content>().EndContent();

        for (int i = subContentParent.transform.childCount - 1; i >= 0; i--)
            subContentParent.transform.GetChild(i).GetComponent<Content>().EndContent();

        titleTaskQueue.Clear();

        titleGroup.gameObject.SetActive(false);
        busy = false;
    }
}