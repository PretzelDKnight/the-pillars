﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FadeState
{
    Opaque,
    Fading,
    Transparent
}

public class ObjectFadeScript : MonoBehaviour
{
    public Material source;

    Material ogMat;
    Material tempMat;

    MeshRenderer mesh;

    public bool obstructing;

    FadeState state;

    public static float fadeLvl = 0.6f;
    public static float fadeRate = 3f;

    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        ogMat = mesh.material;
        obstructing = false;
        state = FadeState.Opaque;

        if (tempMat == null)
        {
            tempMat = new Material(ogMat);
            // Changing material blendmode to Fade
            tempMat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            tempMat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            tempMat.SetInt("_ZWrite", 0);
            tempMat.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Back);
            tempMat.DisableKeyword("_ALPHATEST_ON");
            tempMat.EnableKeyword("_ALPHABLEND_ON");
            tempMat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            tempMat.renderQueue = 3000;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (obstructing && state == FadeState.Opaque)
        {
            StartCoroutine(FadeOut());
        }
        else if (!obstructing && state != FadeState.Opaque)
        {
            StopCoroutine(FadeOut());
            StartCoroutine(FadeIn());
        }

        obstructing = false;
    }

    IEnumerator FadeOut()
    {
        state = FadeState.Fading;
        float time = 0;

        mesh.material = tempMat;

        Color col = ogMat.color;
        float a = col.a;

        while (time <=1)
        {
            time += Time.deltaTime * fadeRate;
            col.a = Mathf.Lerp(a, fadeLvl, time * time);
            tempMat.color = col;

            yield return null;
        }

        state = FadeState.Transparent;
    }

    IEnumerator FadeIn()
    {
        state = FadeState.Fading;
        float time = 0;

        Color col = ogMat.color;
        float a = col.a;

        while (time <= 1)
        {
            time += Time.deltaTime * fadeRate;
            col.a = Mathf.Lerp(fadeLvl, a, time * time);
            tempMat.color = col;

            yield return null;
        }

        mesh.material = ogMat;

        state = FadeState.Opaque;
    }
}
