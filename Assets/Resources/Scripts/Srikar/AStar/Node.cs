﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Node : MonoBehaviour
{
    protected Node parent;
    public List<Node> children = new List<Node>();
    protected float f;
    protected float g;
    protected float h;

    protected bool visited;

    public float F
    {
        get { return f; }
    }

    public float G
    {
        get {  return g; }
        set { g = value; }
    }

    public float H
    {
        get {  return h; }
        set { h = value; }
    }

    public Node Parent
    {
        get { return parent; }
        set { parent = value; }
    }

    public List<Node> ReturnNeighbours()
    {
        return children;
    }

    public abstract void SetChildren(List<Node> graph);

    public abstract float CalculateDistance(Node other);

    public abstract void CalculateF();

    public void SetVisited()
    {
        visited = true;
    }

    public bool Visited
    {
        get { return visited; }
    }

    public void NodeReset()
    {
        g = 0;
        h = 0;
        f = 0;
        parent = null;
        visited = false;
    }
}
