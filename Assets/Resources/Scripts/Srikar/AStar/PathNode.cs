﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : Node
{
    public LayerMask layer;

    public override float CalculateDistance(Node other)
    {
        return Vector3.Distance(transform.position, other.transform.position);
    }

    public override void CalculateF()
    {
        f = g + h;
    }

    public override void SetChildren(List<Node> graph)
    {
        children.Clear();

        for (int i = 0; i < graph.Count; i++)
        {
            if (graph[i] != this && graph[i] != null)
            {
                RaycastHit hit;
                Vector3 dir = graph[i].transform.position - transform.position;
                if (Physics.Raycast(transform.position, dir, out hit, 7f, layer)) // FIX THE LAYERMASK
                {
                    if (hit.collider.tag == "PathNode")
                    {
                        Node temp = hit.collider.GetComponent<Node>();
                        if (!children.Contains(temp))
                        {
                            children.Add(temp);
                        }
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 0.3f);
    }
}
