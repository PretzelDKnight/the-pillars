﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[SerializeField]
public class Graph
{
    public List<Node> objectList = new List<Node>();

    public bool created;
}

public class AStar : MonoBehaviour
{
    public static AStar instance = null;

    [SerializeField] GameObject nodeGameObject;
    [SerializeField] Vector2 gridValues;
    [SerializeField] public float space = 1;

    List<Node> openList;
    List<Node> closedList;

    Graph graph = new Graph();

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        LoadNodes();
    }

    // AStar
    public List<Node> FindPath(Node start, Node end)
    {
        ResetNodes();

        openList = new List<Node>() { start };
        closedList = new List<Node>();

        start.G = 0;
        start.H = start.CalculateDistance(end);
        start.CalculateF();

        // AStar Black Magic
        while (openList.Count > 0)
        {
            Node current = GetLowestF(openList);

            openList.Remove(current);
            closedList.Add(current);
            current.SetVisited();

            if (current == end)
                break;

            foreach (var neighbour in current.ReturnNeighbours())
            {
                if (neighbour.Visited) { }
                else if (openList.Contains(neighbour))
                {
                    float tempG = current.G + current.CalculateDistance(neighbour);

                    if (tempG < neighbour.G)
                    {
                        neighbour.Parent = current;
                        neighbour.G = tempG;
                        neighbour.CalculateF();
                    }
                }
                else
                {
                    neighbour.Parent = current;
                    neighbour.G = current.G + current.CalculateDistance(neighbour);
                    neighbour.CalculateF();
                    openList.Add(neighbour);
                }
            }
        }

        // Going up parents in closed list
        List<Node> tempList = new List<Node>();
        Node node = closedList[closedList.Count - 1];
        while (node != null)
        {
            tempList.Insert(0, node);
            node = node.Parent;
        }

        return tempList;

    }

    Node GetLowestF(List<Node> list)
    {
        Node lowest = list[0];
        foreach (var item in list)
        {
            if (item.F < lowest.F)
                lowest = item;
        }

        return lowest;
    }

    public void GenerateGrid()
    {
        DeleteGrid();

        for (int i = 0; i < gridValues.x; i++)
        {
            for (int j = 0; j < gridValues.y; j++)
            {
                GameObject obj;
                obj = Instantiate(nodeGameObject, new Vector3(transform.position.x + (-gridValues.x/ 2 + i) * space, transform.position.y, transform.position.z + (-gridValues.y/ 2 + j) * space), Quaternion.identity, transform);
                graph.objectList.Add(obj.GetComponent<Node>());
            }
        }

        //GraphFormatting(graph);
    }

    public void GraphFormatting()
    {
        List<Node> list = graph.objectList;

        Debug.Log(list.Count);

        Debug.Log("Graph generated");

        foreach (var item in list)
        {
            item.SetChildren(list);
            Debug.Log(item.children.Count);
        }

        graph.created = true;
        Debug.Log("Created graph");
    }

    void DeleteGrid()
    {
        if(graph.objectList.Count > 0)
        for (int i = graph.objectList.Count - 1; i >= 0; i--)
        {
            var temp = graph.objectList[i];
            graph.objectList.Remove(temp);
            DestroyImmediate(temp);
        }

        graph.objectList.Clear();
        graph.created = false;
    }

    void ResetNodes()
    {
        List<Node> list = graph.objectList;

        for (int i = list.Count - 1; i >= 0; i--)
        {
            list[i].NodeReset();
        }
    }

    public List<Node> ReturnGraph()
    {
        Debug.Log(graph.objectList.Count);
        Debug.Log(graph.created.ToString());
        return graph.objectList;
    }

    public bool Created
    {
        get { return graph.created; }
    }

    public void SetNodes()
    {
        DeleteGrid();

        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            graph.objectList.Add(transform.GetChild(i).GetComponent<Node>());
        }

        Debug.Log("Node list filled!");
        Debug.Log(graph.objectList.Count);
    }

    public void LoadNodes()
    {
        if (File.Exists("Assets/Resources/pathnodes.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open("Assets/Resources/pathnodes.save", FileMode.Open);
            string json = (string)bf.Deserialize(file);
            file.Close();

            graph = JsonUtility.FromJson<Graph>(json);
        }
    }

    public void SaveNodes()
    {
        string json = JsonUtility.ToJson(graph);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create("Assets/Resources/pathnodes.save");
        bf.Serialize(file, json);
        file.Close();

        Debug.Log("Saved!");
    }
}
