﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFindingAgent : MonoBehaviour
{
    public bool pathfindingEnabled;

    public Vector3 targetPos;

    Node currentNode;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //FindCurrentNode();
    }

    protected void FindCurrentNode()
    {
        List<Node> graph = AStar.instance.ReturnGraph();

        Node nearest = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (var item in graph)
        {
            Vector3 directionToTarget = item.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                nearest = item;
            }
        }

        currentNode = nearest;
    }

    public Node ReturnCurrentNode()
    {
        return currentNode;
    }
}