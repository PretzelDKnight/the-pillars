﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    Rigidbody rb;

    // Enemy Values
    [SerializeField] protected int health;
    [SerializeField] protected float moveSpeed;
    [SerializeField] protected float climbSpeed;
    [SerializeField] protected float mass;
    [SerializeField] protected float maxForce;

    [SerializeField] protected Weapon primary;

    [SerializeField] Node currentNode;

    protected int rt_health;
    protected float rt_moveSpeed;
    protected float rt_climbSpeed;
    protected Vector3 velocity;

    protected int currentHealth;

    public JumpState jumpState;
    public float currentHeightScale;
    public float halfHeight;

    Vector3 position;
    bool wounded;

    public Player target;
    public ProgressBar progressBar;
    public int bloodGained;

    [SerializeField] float attackRange;
    static float rt_attackRange;

    // Start is called before the first frame update
    protected void Start()
    {
        CalcRunTimeValues();
        Prep();
     //   Flocker.instance.AddNeighbour(this);
        //Formation.instance.AddMember(this);
        //FindCurrentNode();
    }

    private void Awake()
    {
        progressBar = FindObjectOfType<ProgressBar>();
    }

    // Update is called once per frame
    void Update()
    {
        PlatformerJuicer.instance.GroundCheck(this);
        Move();
        EnemyDie();
    }

    protected void CalcRunTimeValues()
    {
        rt_health = health;
        rt_moveSpeed = moveSpeed;
        rt_climbSpeed = climbSpeed;
    }

    public void Move()
    {
         //if (!AStar.instance.Created)
             //Seek(target.transform.position);
         /*else
         {
             List<Node> path = AStar.instance.FindPath(currentNode, target.currentNode);
             Seek(FollowPath(path));
         }*/
        //Seek(position);
    }
/*
    void Seek(Vector3 point)  // Remove later
    {
        Vector3 desiredVelocity = point - transform.position;
        desiredVelocity = desiredVelocity.normalized * rt_moveSpeed;

        Vector3 steering = desiredVelocity - velocity;
        steering = Vector3.ClampMagnitude(steering, maxForce);
        steering /= mass;

        velocity = Vector3.ClampMagnitude(velocity + steering + Flocker.instance.FlockCalculation(this), rt_moveSpeed);
        transform.position += velocity * Time.deltaTime;

        Vector3 dir = velocity.normalized;
        dir.y = 0;
        transform.forward = dir;
    }
*/
    public void AssignPosition(Vector3 pos)
    {
        position = pos;
    }
    
    Vector3 FollowPath(List<Node> path)
    {
        Vector3 nextPos = Vector3.zero;

        if (path != null)
        {
            int i = path.IndexOf(currentNode) + 1;
            Node target;

            if (i >= path.Count)
                target = path[path.Count - 1];
            else
                target = path[i];

            if (Vector3.Distance(transform.position, target.transform.position) >= rt_attackRange)
            {
                currentNode = target;
            }

            nextPos = target.transform.position;
        }

        return nextPos;
    }

    public void Climb()
    {

    }

    public void UsePrimary()
    {
        primary.Fire(target.transform.position);
    }

    protected void RecieveDamage()
    {
        health -= 1;
    }

    protected void Prep()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        currentHealth = rt_health;

        jumpState = JumpState.Grounded;

        rb = GetComponent<Rigidbody>();
        halfHeight = GetComponent<Collider>().bounds.size.y / 2;
        currentHeightScale = transform.localScale.y;

        rt_attackRange = attackRange;
        velocity = Vector3.zero;
    }

    protected void OnEnable()
    {
        Prep();
    }

    protected void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == target.tag)
        {
            target.RecieveDamage(transform.position);
            ObjectPooling.PoolDestroy(gameObject);
        }
    }

    protected void FindCurrentNode()
    {
        List<Node> graph = AStar.instance.ReturnGraph();

        Node nearest = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (var item in graph)
        {
            Vector3 directionToTarget = item.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                nearest = item;
            }
        }

        currentNode = nearest;
    }

    public Vector3 Velocity
    {
        get { return velocity; }
        set { velocity = value; }
    }

    public void TakeDamageEnemy()
    {
        currentHealth -= 1;
    }

    void EnemyDie()
    {
        if (currentHealth <= 0)
        {
            //progressBar.currentProg += bloodGained;
            ObjectPooling.PoolDestroy(gameObject);
            currentHealth = health;
        }
    }
}