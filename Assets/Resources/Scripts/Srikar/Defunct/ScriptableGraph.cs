﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Graph", menuName = "ScriptableObjects/ScriptableGraph", order = 1)]
public class ScriptableGraph : ScriptableObject
{
    public List<Node> graph;
    public bool created = false;

}
