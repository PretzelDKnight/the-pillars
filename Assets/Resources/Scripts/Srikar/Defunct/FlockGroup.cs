﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockGroup : MonoBehaviour
{
    List<Enemy> members;

    public float cohesionRange = 6f;
    public float seperationRange = 4f;
    public float alignmentRange = 6f;

    public float cohesionFactor = 0.2f;
    public float alignmentFactor = 0.4f;
    public float seperationFactor = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FlockaFlocka(Enemy boid)
    {
        if(members.Contains(boid))
            boid.Velocity += (Cohesion(boid) + Alignment(boid) + Seperation(boid)) * Time.deltaTime;
    }

    public void AssignMember(Enemy boid)
    {
        members.Add(boid);
    }

    Vector3 Cohesion(Enemy boid)
    {
        Vector3 temp = Vector3.zero;
        int count = 0;
        foreach(var member in members)
        {
            if (boid != member)
            {
                if (Vector3.Distance(boid.transform.position, member.transform.position) < cohesionRange)
                {
                    count++;
                    temp += member.transform.position;
                }
            }
        }

        temp /= count;

        return (temp - boid.transform.position).normalized * cohesionFactor;
    }

    Vector3 Seperation(Enemy boid)
    {
        Vector3 temp = Vector3.zero;

        foreach(var member in members)
        {
            if(boid != member)
            {
                if (Vector3.Distance(boid.transform.position, member.transform.position) < seperationRange)
                {
                    temp -= member.transform.position - boid.transform.position;
                }
            }
        }

        return temp.normalized * seperationFactor;
    }

    Vector3 Alignment(Enemy boid)
    {
        Vector3 temp = Vector3.zero;
        int count = 0;

        foreach(var member in members)
        {
            if(boid != member)
            {
                if (Vector3.Distance(boid.transform.position, member.transform.position) < alignmentRange)
                {
                    count++;
                    temp += member.Velocity.normalized;
                }
            }
        }

        temp /= count;

        return (temp - boid.Velocity).normalized * alignmentFactor;
    }
}
