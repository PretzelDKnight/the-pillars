﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorScript : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] Player player;
    [SerializeField] GameObject indicator;

    [SerializeField] float maxLength;

    float mag;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = player.transform.position;
        Cursor.lockState = CursorLockMode.Confined;

        mag = Vector3.Distance(transform.position, indicator.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
        //CamToWorldRaycast();
        MousePositionToDirection();
    }

    void FollowPlayer()
    {
        transform.position = player.transform.position;
    }

    void MousePositionToDirection()
    {
        Vector2 mousePos = Input.mousePosition;
        Vector2 relativeCentre = cam.WorldToScreenPoint(player.transform.position);

        Vector3 forward = cam.transform.forward;
        forward.y = 0;
        forward = forward.normalized;

        Vector3 right = cam.transform.right;
        right.y = 0;
        right = right.normalized;

        Vector3 up = Vector3.up;

        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        mousePos = mousePos - relativeCentre;

        float ratio = screenWidth / screenHeight;

        Vector3 aimPoint = new Vector3(mousePos.x, 0, mousePos.y);

        Vector3 point = player.transform.position;
        point += forward * (aimPoint.z);
        point += right * (aimPoint.x);
        point += up * (aimPoint.y);

        Vector3 aimDir = point - player.transform.position;

        indicator.transform.localPosition = aimDir.normalized * mag / 2f;
    }

    void RotateToMouseDirection(GameObject obj, Vector3 destination)
    {
        Vector3 direction = destination - obj.transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);

        obj.transform.localRotation = Quaternion.Lerp(obj.transform.rotation, rotation, 1);
    }
}
