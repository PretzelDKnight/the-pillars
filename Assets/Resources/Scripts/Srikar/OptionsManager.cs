﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    static public OptionsManager instance = null;

    [SerializeField] Dropdown resolutionDrop;
    [SerializeField] Dropdown fullscreenDrop;
 
    [SerializeField] Slider sfxSlider;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider masterSlider;

    [SerializeField] public AudioSource buttonSFX;
    [SerializeField] public AudioSource confirmSFX;
    [SerializeField] AudioSource bgMenu;

    [SerializeField] Button up;
    [SerializeField] Button down;
    [SerializeField] Button left;
    [SerializeField] Button right;
    [SerializeField] Button jump;
    [SerializeField] Button primary;
    [SerializeField] Button secondary;
    [SerializeField] Button openUpgrade;
    [SerializeField] Button upgrade1;
    [SerializeField] Button upgrade2;
    [SerializeField] Button upgrade3;
    [SerializeField] Button confirm;

    [SerializeField] GameObject keyWaitObject;

    [SerializeField] Text pressText;

    Dictionary<Button, string> keybindDictionary = new Dictionary<Button, string>();

    float volumeSFX;
    float volumeBG;
    float volumeMaster;

    float og_buttonV;
    float og_confirmV;
    float og_bgV;

    static bool settingKey;

    Event keyEvent;

    KeyCode tempCode;

    int fullscreen;
    int resolution;

    bool bindsLoaded;

    // Start is called before the first frame update
    void Start()
    {

        if (instance != null)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(this);
            instance = this;
        }

        Prep();

        CompileInputFieldDictionary();

        bindsLoaded = false;

        RetrieveFromPref();
    }

    private void Update()
    {
    }

    void Prep()
    {
        og_buttonV = buttonSFX.volume;
        og_confirmV = confirmSFX.volume;
        og_bgV = bgMenu.volume;

        AudioStartSet(bgMenu, true);
        AudioStartSet(confirmSFX, false);
        AudioStartSet(buttonSFX, false);

        tempCode = KeyCode.None;

        keyWaitObject.SetActive(false);
    }

    public void UpdateSFXVolume(float val)
    {
        volumeSFX = val;

        buttonSFX.volume = og_buttonV * volumeSFX * volumeMaster;
        confirmSFX.volume = og_confirmV * volumeSFX * volumeMaster;
        PlayerPrefs.SetFloat("SFXVolume", volumeSFX);
        PlayerPrefs.Save();
    }

    public void UpdateMusicVolume(float val)
    {
        volumeBG = val;

        bgMenu.volume = og_bgV * volumeBG * volumeMaster;
        PlayerPrefs.SetFloat("MusicVolume", volumeBG);
        PlayerPrefs.Save();
    }

    public void UpdateMasterVolume(float val)
    {
        volumeMaster= val;

        UpdateSFXVolume(volumeSFX);
        UpdateMusicVolume(volumeBG);
        PlayerPrefs.SetFloat("MasterVolume", volumeMaster);
        PlayerPrefs.Save();
    }

    public void UpdateResolution(int val)
    {
        resolution = val;
        PlayerPrefs.SetInt("Screen", resolution);
        PlayerPrefs.Save();
        UpdateScreen();
    }

    public void UpdateFullscreen(int val)
    {
        fullscreen = val;
        PlayerPrefs.SetInt("Fullscreen", fullscreen);
        PlayerPrefs.Save();
        UpdateScreen();
    }

    void AudioStartSet(AudioSource audio, bool val)
    {
        audio.loop = val;
        audio.playOnAwake = val;
    }

    void RetrieveFromPref()
    {
        if (!PlayerPrefs.HasKey("MusicVolume"))
        {
            PlayerPrefs.SetFloat("MusicVolume", 1);
            volumeBG = 1;
        }
        else
        {
            volumeBG = PlayerPrefs.GetFloat("MusicVolume");
            musicSlider.value = volumeBG;
        }

        if (!PlayerPrefs.HasKey("SFXVolume"))
        {
            PlayerPrefs.SetFloat("SFXVolume", 1);
            volumeSFX = 1;
        }
        else
        {
            volumeSFX = PlayerPrefs.GetFloat("SFXVolume");
            sfxSlider.value = volumeSFX;
        }


        if (!PlayerPrefs.HasKey("MasterVolume"))
        {
            PlayerPrefs.SetFloat("MasterVolume", 1);
            volumeMaster = 1;
        }
        else
        {
            volumeMaster = PlayerPrefs.GetFloat("MasterVolume");
            masterSlider.value = volumeMaster;
        }

        if (!PlayerPrefs.HasKey("Screen"))
        {
            PlayerPrefs.SetInt("Screen", 1);
            resolution = 1;
            resolutionDrop.value = resolution;
        }
        else
        {
            resolution = PlayerPrefs.GetInt("Screen");
            resolutionDrop.value = resolution;
        }

        if (!PlayerPrefs.HasKey("Fullscreen"))
        {
            PlayerPrefs.SetInt("Fullscreen", 1);
            fullscreen = 1;
            fullscreenDrop.value = fullscreen;
        }
        else
        {
            fullscreen = PlayerPrefs.GetInt("Fullscreen");
            fullscreenDrop.value = fullscreen;
        }

        PlayerPrefs.Save();
        UpdateScreen();
        UpdateMasterVolume(volumeMaster);
    }

    public void UpdateScreen()
    {
        bool tempBool = false;

        if (fullscreen == 1)
            tempBool = true;

        switch(resolution)
        {
            case 1:
                Screen.SetResolution(1280, 720, tempBool);
                break;
            case 2:
                Screen.SetResolution(1920, 1080, tempBool);
                break;
            case 3:
                Screen.SetResolution(3440, 1440, tempBool);
                break;
            default:
                Debug.LogError("Uh Oh Stinky!");
                break;
        }
    }

    void CompileInputFieldDictionary()
    {
        keybindDictionary.Clear();

        keybindDictionary.Add(up, "Up");
        keybindDictionary.Add(down, "Down");
        keybindDictionary.Add(left, "Left");
        keybindDictionary.Add(right, "Right");
        keybindDictionary.Add(jump, "Jump");
        keybindDictionary.Add(primary, "Primary");
        keybindDictionary.Add(secondary, "Secondary");
        keybindDictionary.Add(openUpgrade, "OpenUpgrade");
        keybindDictionary.Add(upgrade1, "Upgrade1");
        keybindDictionary.Add(upgrade2, "Upgrade2");
        keybindDictionary.Add(upgrade3, "Upgrade3");
        keybindDictionary.Add(confirm, "Confirm");
    }

    public void ResetToDefaultKeybinds()
    {
        confirmSFX.Play();

        InputManager.instance.DictionarySetDefault();

        foreach (var item in InputManager.ReturnDictionary())
        {
            PlayerPrefs.SetString(item.Key, item.Value.ToString());

            foreach (var botton in keybindDictionary)
            {
                if (botton.Value == item.Key)
                {
                    botton.Key.GetComponentInChildren<Text>().text = item.Value.ToString();
                    break;
                }
            }
        }

        PlayerPrefs.Save();
    }

    public void KeybindsFromPrefs()
    {
        foreach(var item in keybindDictionary)
        {
            if (!PlayerPrefs.HasKey(item.Value))
            {
                PlayerPrefs.SetString(item.Value, InputManager.GetKey(item.Value).ToString());
            }
            else
            {
                InputManager.SetKey(item.Value, (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(item.Value, InputManager.GetKey(item.Value).ToString())));

                KeyCode temp = InputManager.GetKey(item.Value);
                if (temp != KeyCode.None)
                    item.Key.GetComponentInChildren<Text>().text = temp.ToString();
                else
                    item.Key.GetComponentInChildren<Text>().text = "";
            }
        }

        PlayerPrefs.Save();
    }

    public void ChangeKey(Button bttn)
    {
        Debug.Log("Calling!");
        if (keybindDictionary.ContainsKey(bttn))
        {
            Debug.Log(keybindDictionary[bttn]);
            StartCoroutine(AssignKey(bttn));
        }
        else
            Debug.LogError("Keybinds dont have the key!");
    }

    IEnumerator AssignKey(Button bttn)
    {
        settingKey = true;
        Text txt = bttn.GetComponentInChildren<Text>();
        keyWaitObject.SetActive(true);
        pressText.text = "Press " + InputManager.Back.ToString() + " to exit";

        tempCode = KeyCode.None;

        while (true)
        {
            if (tempCode != KeyCode.None)
            {
                break;
            }

            yield return null;
        }

        if (tempCode == InputManager.Back)
        {
            keyWaitObject.SetActive(false);
            settingKey = false;
            yield break;
        }

        foreach (var item in InputManager.ReturnDictionary())
        {
            if (item.Value == tempCode)
            {
                InputManager.SetKey(item.Key, KeyCode.None);

                foreach (var botton in keybindDictionary)
                {
                    if (botton.Value == item.Key)
                    {
                        botton.Key.GetComponentInChildren<Text>().text = "";
                        PlayerPrefs.SetString(item.Key, KeyCode.None.ToString());
                        break;
                    }
                }
                break;
            }
        }

        InputManager.SetKey(keybindDictionary[bttn], tempCode);

        PlayerPrefs.SetString(keybindDictionary[bttn], tempCode.ToString());
        PlayerPrefs.Save();

        txt.text = tempCode.ToString();

        keyWaitObject.SetActive(false);
        tempCode = KeyCode.None;
        settingKey = false;
    }
    
    static public bool Allow()
    {
        return !settingKey;
    }

    private void OnGUI()
    {
        keyEvent = Event.current;

        if (keyEvent.isKey && settingKey)
        {
            tempCode = keyEvent.keyCode;
        }
    }
}