﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] protected GameObject firePoint;
    [SerializeField] protected GameObject bullet;

    protected float TimeToFire = 0;
    [SerializeField] protected float fireRate;
    [SerializeField] protected float rechargeTime;

    [SerializeField] protected int damage = 1;

    protected float rt_fireRate;
    protected float rt_rechargeTime;

    public AudioClip gunshotSound;
    public float pitchRandomization;
    public AudioSource sfx;

    // Start is called before the first frame update
    void Start()
    {
        ResetValues();
    }

    public virtual void Fire(Vector3 target)
    {
        if (Time.time >= TimeToFire)
        {
            TimeToFire = Time.time + 1 / rt_fireRate;
            SpawnVFX(target);
            FireSound();
        }
    }

    public void FireSound()
    {
        sfx.Stop();
        sfx.clip = gunshotSound;
        sfx.pitch = 1 - pitchRandomization + Random.Range(-pitchRandomization, pitchRandomization);
        sfx.Play();
    }
    protected virtual void SpawnVFX(Vector3 target)
    {
        GameObject vfx;

        if (firePoint != null)
        {
            vfx = ObjectPooling.PoolInstantiate(bullet, firePoint.transform.position, Quaternion.identity);
            vfx.transform.localRotation = firePoint.transform.rotation;
            vfx.GetComponent<ProjectileMove>().AssignDamage(damage);
        }
        else
        {
            Debug.Log("No Fire Point");
        }
    }

    public virtual string ReturnType()
    {
        return "Weapon";
    }

    public void ResetValues()
    {
        rt_fireRate = fireRate;
        rt_rechargeTime = rechargeTime;
    }

    public float FireRate
    {
        get { return rt_fireRate; }
        set 
        { 
            rt_fireRate = value;

            if (rt_fireRate <= 0.1f)
                rt_fireRate = 0.1f;
        }
    }

    public float RechargeTime
    {
        get { return rt_rechargeTime; }
        set 
        { 
            rt_rechargeTime = value;

            if (rt_rechargeTime <= 0.1f)
                rt_rechargeTime = 0.1f;
        }
    }

    public virtual void WeaponUpdate()
    {

    }
}
