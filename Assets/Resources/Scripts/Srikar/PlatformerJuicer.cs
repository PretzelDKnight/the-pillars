﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerJuicer : MonoBehaviour
{
    public static PlatformerJuicer instance;

    [SerializeField] public float groundAllowance = 0.005f;
    [SerializeField] public float scaleWarpTime = 5f;
    [SerializeField] public float minScaleWarp = 0.075f;

    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    public void GroundCheck(Enemy enemy)
    {
        if (enemy.jumpState != JumpState.Jumping)
        {
            Ray ray = new Ray(enemy.transform.position, -transform.up);
            if (Physics.Raycast(ray, enemy.halfHeight + groundAllowance))
            {
                if (enemy.jumpState == JumpState.Falling)
                {
                    StartCoroutine(ScaleWarp(enemy));
                }
                enemy.jumpState = JumpState.Grounded;
            }
            else
            {
                enemy.jumpState = JumpState.Falling;
            }
        }
    }

    public IEnumerator ScaleWarp(Player player)
    {
        float t = 0;
        float temp;

        while (t <= 1f)
        {
            t += Time.deltaTime * scaleWarpTime;
            float val = Mathf.Lerp(0, Mathf.PI, t);

            temp = Mathf.Sin(val);

            Vector3 scale = player.transform.localScale;
            scale.y = player.currentHeightScale - (temp * minScaleWarp);
            player.transform.localScale = scale;

            yield return null;
        }
    }

    public IEnumerator ScaleWarp(Enemy enemy)
    {
        float t = 0;
        float temp;

        while (t <= 1f)
        {
            t += Time.deltaTime * scaleWarpTime;
            float val = Mathf.Lerp(0, Mathf.PI, t);

            temp = Mathf.Sin(val);

            Vector3 scale = enemy.transform.localScale;
            scale.y = enemy.currentHeightScale - (temp * minScaleWarp);
            enemy.transform.localScale = scale;

            yield return null;
        }
    }
}
