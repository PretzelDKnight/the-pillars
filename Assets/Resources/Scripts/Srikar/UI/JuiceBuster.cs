﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JuiceBuster : MonoBehaviour
{
    static public JuiceBuster instance = null;

    public float fadeSpeed = 0.2f;
    public float scaleSpeed = 0.2f;

    static float rt_fadeSpeed;
    static float rt_scaleSpeed;

    static Dictionary<GameObject, bool> objectList;

    // Start is called before the first frame update
    void Start()
    {
        if(instance != null)
            Destroy(gameObject);
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        Prep();
    }

    void Prep()
    {
        objectList = new Dictionary<GameObject, bool>();

        rt_fadeSpeed = fadeSpeed;
        rt_scaleSpeed = scaleSpeed;
    }

    static bool CheckObjectList(GameObject obj)
    {
        if (objectList.ContainsKey(obj))
        {
            if (objectList[obj])
            {
                objectList[obj] = false;
                return true;
            }
            else
            {
                // Object still being lerped, Do not disturb
                return false;
            }
        }

        objectList.Add(obj, false);
        return true;
    }

    static void ResetObject(GameObject obj)
    {
        if (objectList.ContainsKey(obj))
        {
            if (!objectList[obj])
                objectList[obj] = true;
        }
    }

    public void LerpTextScale(Text txt, float scale, float time = -1)
    {
        if (CheckObjectList(txt.gameObject))
        {
            StartCoroutine(TextScale(txt, scale, time));
        }
    }

    public void LerpImageScale(Image img, float scale, float time = -1)
    {
        if (CheckObjectList(img.gameObject))
        {
            StartCoroutine(ImageScale(img, scale, time));
        }
    }

    public void LerpImageScale(Image img, Vector2 scale, float time = -1)
    {
        if (CheckObjectList(img.gameObject))
        {
            StartCoroutine(ImageScale(img, scale, time));
        }
    }

    public void LerpButtonScale(Button bttn, float scale, float time = -1)
    {
        if (CheckObjectList(bttn.gameObject))
        {
            Text txt = bttn.GetComponentInChildren<Text>();
            Image img = bttn.GetComponent<Image>();

            StartCoroutine(TextScale(txt, scale, time, false));
            StartCoroutine(ImageScale(img, scale, time));
        }
    }

    public void LerpTextFade(Text txt, float alpha, float time = -1)
    {
        if (CheckObjectList(txt.gameObject))
        {
            StartCoroutine(TextFade(txt, alpha, time));
        }
    }

    public void LerpImageFade(Image img, float alpha, float time = -1)
    {
        if (CheckObjectList(img.gameObject))
        {
            StartCoroutine(ImageFade(img, alpha, time));
        }
    }

    public void LerpImageDeactivate(Image img, float alpha, float time = -1)
    {
        if (CheckObjectList(img.gameObject))
        {
            StartCoroutine(ImageFadeDeactivate(img, alpha, time));
        }
    }

    public void LerpButtonFade(Button bttn, float alpha, float time = -1)
    {
        if (CheckObjectList(bttn.gameObject))
        {
            Text txt = bttn.GetComponentInChildren<Text>();
            Image img = bttn.GetComponent<Image>();

            StartCoroutine(TextFade(txt, alpha, time, false));
            StartCoroutine(ImageFade(img, alpha, time));
        }
    }

    public void LerpCanvasGroupFade(CanvasGroup group, float alpha, float time = -1)
    {
        if(CheckObjectList(group.gameObject))
        {
            StartCoroutine(GroupFade(group, alpha, time));
        }
    }

    public void LerpCanvasGroupDeactivate(CanvasGroup group, float time = -1)
    {
        if (CheckObjectList(group.gameObject))
        {
            StartCoroutine(GroupDeactivate(group, time));
        }
    }

    static IEnumerator GroupFade(CanvasGroup group, float alpha, float speedFactor = -1)
    {
        float time = 0;
        float ogAlpha = group.alpha;

        group.interactable = false;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_fadeSpeed;
            else
                time += Time.deltaTime / speedFactor;

            group.alpha = Mathf.Lerp(ogAlpha, alpha, time * time);

            yield return null;
        }

        group.interactable = true;

        ResetObject(group.gameObject);
    }

    static IEnumerator GroupDeactivate(CanvasGroup group, float speedFactor = -1)
    {
        float time = 0;
        float ogAlpha = group.alpha;
        group.interactable = false;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_fadeSpeed;
            else
                time += Time.deltaTime / speedFactor;

            group.alpha = Mathf.Lerp(ogAlpha, 0, time * time);

            yield return null;
        }

        group.gameObject.SetActive(false);
        group.interactable = true;

        ResetObject(group.gameObject);
    }

    static IEnumerator ImageFade(Image img, float alpha, float speedFactor = -1)
    {
        float time = 0;
        float ogAlpha = img.color.a;
        Color col = img.color;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_fadeSpeed;
            else
                time += Time.deltaTime / speedFactor;

            col.a = Mathf.Lerp(ogAlpha, alpha, time * time);
            img.color = col;
            yield return null;
        }

        ResetObject(img.gameObject);
    }

    static IEnumerator ImageFadeDeactivate(Image img, float alpha, float speedFactor = -1)
    {
        float time = 0;
        float ogAlpha = img.color.a;
        Color col = img.color;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_fadeSpeed;
            else
                time += Time.deltaTime / speedFactor;

            col.a = Mathf.Lerp(ogAlpha, alpha, time * time);
            img.color = col;
            yield return null;
        }

        img.gameObject.SetActive(false);
        ResetObject(img.gameObject);
    }

    static IEnumerator TextFade(Text txt, float alpha, float speedFactor = -1, bool listVal = true)
    {
        if (!listVal)
            CheckObjectList(txt.gameObject);

        float time = 0;
        float ogAlpha = txt.color.a;
        Color col = txt.color;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_fadeSpeed;
            else
                time += Time.deltaTime / speedFactor;

            col.a = Mathf.Lerp(ogAlpha, alpha, time * time);
            txt.color = col;
            yield return null;
        }

        ResetObject(txt.gameObject);
    }

    static IEnumerator ImageScale(Image img, float scale, float speedFactor = -1)
    {
        float time = 0;
        Vector2 rect = img.rectTransform.sizeDelta;
        Vector2 ogrect = rect;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_scaleSpeed;
            else
                time += Time.deltaTime / speedFactor;

            rect.x = Mathf.Lerp(ogrect.x, ogrect.x * scale, time * time);
            rect.y = Mathf.Lerp(ogrect.y, ogrect.y * scale, time * time);
            img.rectTransform.sizeDelta = rect;
            yield return null;
        }

        ResetObject(img.gameObject);
    }

    static IEnumerator ImageScale(Image img, Vector2 scale, float speedFactor = -1)
    {
        float time = 0;
        Vector2 rect = img.rectTransform.sizeDelta;
        Vector2 ogrect = rect;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_scaleSpeed;
            else
                time += Time.deltaTime / speedFactor;

            rect.x = Mathf.Lerp(ogrect.x, scale.x, time * time);
            rect.y = Mathf.Lerp(ogrect.y, scale.y, time * time);
            img.rectTransform.sizeDelta = rect;
            yield return null;
        }

        ResetObject(img.gameObject);
    }

    static IEnumerator TextScale(Text txt, float scale, float speedFactor = -1, bool listVal = true)
    {
        if (!listVal)
            CheckObjectList(txt.gameObject);

        float time = 0;
        Vector2 rect = txt.rectTransform.sizeDelta;
        Vector2 ogrect = rect;
        float textSize = txt.fontSize;
        float ogSize = textSize;

        while (time <= 1.1f)
        {
            if (speedFactor == -1)
                time += Time.deltaTime / rt_scaleSpeed;
            else
                time += Time.deltaTime / speedFactor;

            rect.x = Mathf.Lerp(ogrect.x, ogrect.x * scale, time * time);
            rect.y = Mathf.Lerp(ogrect.y, ogrect.y * scale, time * time);
            textSize = Mathf.Lerp(ogSize, ogSize * scale, time * time);
            txt.rectTransform.sizeDelta = rect;
            txt.fontSize = (int)textSize;
            yield return null;
        }

        ResetObject(txt.gameObject);
    }

    public bool CheckNotBusy(GameObject obj)
    {
        if (objectList.ContainsKey(obj))
        {
            return objectList[obj];
        }

        return true;
    }

    public void Purge()
    {
        StopAllCoroutines();
        objectList.Clear();
    }
}