﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameCanvas : MonoBehaviour
{
    CanvasGroup group;
    Player player;

    bool deactivate;

    // Start is called before the first frame update
    void Start()
    {
        group = gameObject.GetComponent<CanvasGroup>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        group.alpha = 0;

        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);

        deactivate = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.ReturnHealth() <= 0 && !deactivate)
        {
            deactivate = true;
            JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
        }
    }
}
