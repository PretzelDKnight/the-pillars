﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    CanvasGroup group;

    [SerializeField] Image fill;
    [SerializeField] GameObject glow;

    [SerializeField] float fillRate = 1;

    [SerializeField] Vector2 minMaxFade;
    [SerializeField] float fadeTime = 1f;

    float total;

    public bool active;

    // Start is called before the first frame update
    void Start()
    {
        group = GetComponent<CanvasGroup>();
        Empty();
        active = true;
        glow.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            if (JuiceBuster.instance.CheckNotBusy(group.gameObject))
            {
                if (total == 1)
                {
                    if (!glow.gameObject.activeSelf)
                        glow.gameObject.SetActive(true);

                    if (group.alpha == minMaxFade.x)
                        JuiceBuster.instance.LerpCanvasGroupFade(group, minMaxFade.y, fadeTime);
                    else if (group.alpha == minMaxFade.y)
                        JuiceBuster.instance.LerpCanvasGroupFade(group, minMaxFade.x, fadeTime);
                }
                else
                {
                    if (glow.gameObject.activeSelf)
                        glow.gameObject.SetActive(false);

                    if (group.alpha != minMaxFade.x)
                        JuiceBuster.instance.LerpCanvasGroupFade(group, minMaxFade.x);
                }
            }
        }

        float interpolation = fillRate * Time.deltaTime;

        fill.fillAmount = Mathf.Lerp(fill.fillAmount, total, interpolation);
    }

    public void IncreaseFill(float val)
    {
        total += val;

        if (total >= 1)
            total = 1;
    }

    public void Empty()
    {
        total = 0;
        fill.fillAmount = 0;
    }

    public bool FillCheck()
    {
        if (total >= 1)
            return true;

        return false;
    }
}