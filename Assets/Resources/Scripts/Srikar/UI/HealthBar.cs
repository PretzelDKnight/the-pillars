﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] Image healthIcon;
    Color ogCol;
    [SerializeField] Color shieldCol;

    Player player;
    int health;

    List<Image> hearts;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        health = player.ReturnHealth();

        hearts = new List<Image>();
        Debug.Log(health);
        for (int i = 0; i < health; i++)
        {
            Image img = Instantiate(healthIcon.gameObject, transform).GetComponent<Image>();
            hearts.Add(img);
        }

        ogCol = hearts[0].color;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.ReturnHealth() != health)
            UpdateHealth(player.ReturnHealth());
    }

    void UpdateHealth(int count)
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].color = ogCol;
        }

        if (count > hearts.Count)
        {
            for (int i = hearts.Count; i < player.MaxHealth() && i < count; i++)
            {
                Image img = Instantiate(healthIcon.gameObject, transform).GetComponent<Image>();
                hearts.Add(img);
            }
        }
        else if (count < hearts.Count)
        {
            for (int i = hearts.Count - 1; i >= count; i--)
            {
                var temp = hearts[i];
                hearts.RemoveAt(i);
                Destroy(temp.gameObject);
            }
        }

        int shields = count - player.MaxHealth();

        if (shields > 0)
        {
            for (int i = 0; i < shields; i++)
            {
                hearts[i].color = shieldCol;
            }
        }

        health = count;
    }
}
