﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShift : MonoBehaviour
{
    [SerializeField] float shiftAmount = 30f;
    [SerializeField] float shiftSpeed = 20f;

    Vector2 ogPos;

    private void Start()
    {
        ogPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePos = Input.mousePosition;

        float width = Screen.width;
        float height = Screen.height;

        float interpolation = shiftSpeed * Time.deltaTime;

        Vector2 position;

        position.y = Mathf.Lerp(transform.position.y, ogPos.y + (-mousePos.y / height * shiftAmount), interpolation);
        position.x = Mathf.Lerp(transform.position.x, ogPos.x + (-mousePos.x / width * shiftAmount), interpolation);

        transform.position = position;
    }
}
