﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargerateBuff : Buff
{
    public RechargerateBuff()
    {
        permanent = true;
        key = UpgradeManager.upgradeKey;
        definition = "SP Recharge Up";
        sprite = UpgradeManager.instance.rechargeRateImg;
    }

    public override void ApplyBuff(Player player)
    {
        player.secondary.RechargeTime -= 0.1f;
    }
}
