﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Buff
{
    protected bool permanent;
    protected string key;
    protected string definition;
    protected Sprite sprite;

    virtual public string Key
    { get { return key; } }

    virtual public bool Permanent
    { get { return permanent; } }

    virtual public string Definition
    { get { return definition; } }

    virtual public Sprite ReturnSprite()
    { return sprite; }

    abstract public void ApplyBuff(Player player);
}
