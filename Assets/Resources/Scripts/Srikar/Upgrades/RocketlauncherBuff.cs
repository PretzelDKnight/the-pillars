﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketlauncherBuff : Buff
{
    public RocketlauncherBuff()
    {
        permanent = true;
        key = UpgradeManager.secondaryKey;
        definition = "Rocket Launcher";
        sprite = UpgradeManager.instance.rocketlauncherImg;
    }

    public override void ApplyBuff(Player player)
    {
        if (player.secondary != player.rocketlauncher)
        {
            player.secondary = player.rocketlauncher;
            UpgradeManager.instance.RemoveUpgrades();
        }
    }
}
