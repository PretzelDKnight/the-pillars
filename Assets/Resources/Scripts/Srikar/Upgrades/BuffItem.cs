﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffItem : MonoBehaviour
{
    [SerializeField] Image img;
    [SerializeField] Text txt;

    public void Assign(Buff buff, int num)
    {
        img.sprite = buff.ReturnSprite();
        txt.text = "(" + num + ") " + buff.Definition;
    }
}
