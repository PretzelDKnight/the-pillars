﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunBuff : Buff
{
    public ShotgunBuff()
    {
        permanent = true;
        key = UpgradeManager.secondaryKey;
        definition = "Shotgun";
        sprite = UpgradeManager.instance.shotgunImg;
    }

    public override void ApplyBuff(Player player)
    {
        if (player.secondary != player.shotgun)
        {
            player.secondary = player.shotgun;
            UpgradeManager.instance.RemoveUpgrades();
        }
    }
}
