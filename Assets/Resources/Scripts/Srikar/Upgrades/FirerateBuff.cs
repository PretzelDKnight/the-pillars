﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirerateBuff : Buff
{
    public FirerateBuff()
    {
        permanent = true;
        key = UpgradeManager.upgradeKey;
        definition = "SP Firerate Up";
        sprite = UpgradeManager.instance.fireRateImg;
    }

    public override void ApplyBuff(Player player)
    {
        player.secondary.FireRate -= 0.1f;
    }
}
