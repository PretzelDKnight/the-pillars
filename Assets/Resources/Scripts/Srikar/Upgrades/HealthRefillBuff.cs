﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRefillBuff : Buff
{
    public HealthRefillBuff()
    {
        permanent = false;
        key = UpgradeManager.playerKey;
        definition = "Refill Health";
        sprite = UpgradeManager.instance.healthRefillImg;
    }

    public override void ApplyBuff(Player player)
    {
        player.ReplenishHealth();
    }
}
