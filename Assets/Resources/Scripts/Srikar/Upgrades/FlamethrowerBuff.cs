﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamethrowerBuff : Buff
{
    public FlamethrowerBuff()
    {
        permanent = true;
        key = UpgradeManager.secondaryKey;
        definition = "Flamethrower";
        sprite = UpgradeManager.instance.flamethrowerImg;
    }

    public override void ApplyBuff(Player player)
    {
        if (player.secondary != player.flamethrower)
        {
            player.secondary = player.flamethrower;
            UpgradeManager.instance.RemoveUpgrades();
        }
    }
}
