﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBuff : Buff
{
    public ShieldBuff()
    {
        permanent = false;
        key = UpgradeManager.playerKey;
        definition = "Gain 1 Shield";
        sprite = UpgradeManager.instance.shieldImg;
    }

    public override void ApplyBuff(Player player)
    {
        player.AddShield();
    }
}
