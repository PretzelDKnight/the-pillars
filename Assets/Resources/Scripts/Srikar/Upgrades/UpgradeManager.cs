﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UpgradeManager : MonoBehaviour
{
    static public UpgradeManager instance = null;

    [SerializeField] ProgressBar upgradeBar;
    [SerializeField] float amountGained;
    [SerializeField] float maxAmount = 10f;
    [SerializeField] int timesToRemind = 4;
    [SerializeField] float maxRemindDuration = 10f;

    [SerializeField] Transform upgradeParent;
    [SerializeField] CanvasGroup group;
    [SerializeField] GameObject buffPrefab;

    float rt_amountGained;

    bool upgradeActive;
    bool upgradeSelected;

    bool generated;

    Content content;

    [NonSerialized] public Player player;

    [SerializeField] public Sprite shotgunImg;
    [SerializeField] public Sprite flamethrowerImg;
    [SerializeField] public Sprite freezerayImg;
    [SerializeField] public Sprite rocketlauncherImg;
    [SerializeField] public Sprite healthRefillImg;
    [SerializeField] public Sprite shieldImg;
    [SerializeField] public Sprite shotgunUniqueImg;
    [SerializeField] public Sprite flamethrowerUniqueImg;
    [SerializeField] public Sprite freezerayUniqueImg;
    [SerializeField] public Sprite rocketlauncherUniqueImg;
    [SerializeField] public Sprite rechargeRateImg;
    [SerializeField] public Sprite fireRateImg;

    Dictionary<string, List<Buff>> theBuffDictionary;
    Dictionary<string, List<Buff>> appliedBuffs;
    List<KeyValuePair<string, Buff>> displayedBuffs;

    public const string secondaryKey = "Secondary";
    public const string upgradeKey = "Upgrade";
    public const string playerKey = "Player";

    Buff shieldBuff;
    Buff fillHealth;

    bool display;
    bool busy;

    int reminded;
    float remindTime;

    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        player = FindObjectOfType<Player>();

        theBuffDictionary = new Dictionary<string, List<Buff>>();
        appliedBuffs = new Dictionary<string, List<Buff>>();
        displayedBuffs = new List<KeyValuePair<string, Buff>>();

        CompileDictionary();
        CalculateAmount();
        ClearBuffs();
        reminded = 0;
        busy = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!busy)
        {
            if (!upgradeActive && upgradeBar.FillCheck())
            {
                if (reminded < timesToRemind)
                {
                    reminded += 1;
                    NotificationManager.instance.BeginContentNotification(out content, "Upgrade available        (" + InputManager.OpenUpgrade + ")");
                }
                upgradeActive = true;
            }

            if (upgradeActive && content == null)
            {
                if (remindTime >= maxRemindDuration)
                    NotificationManager.instance.BeginContentNotification(out content, "Upgrade available        (" + InputManager.OpenUpgrade + ")");
                else
                    remindTime += Time.deltaTime;
            }

            if (PushDown.instance == null)
            {
                if (Input.GetKeyUp(InputManager.OpenUpgrade))
                    OpenUpgrade();

                if (Input.GetKeyUp(InputManager.Back))
                    CloseUpgrade();

                if (CheckOpen())
                {
                    if (Input.GetKeyUp(InputManager.Upgrade1))
                        Buff1();
                    else if (Input.GetKeyUp(InputManager.Upgrade2))
                        Buff2();
                    else if (Input.GetKeyUp(InputManager.Upgrade3))
                        Buff3();
                }
            }
        }
    }

    void CalculateAmount()
    {
        rt_amountGained = amountGained / maxAmount;
    }

    public void Increment()
    {
        upgradeBar.IncreaseFill(rt_amountGained);
    }

    public void EndFunction()
    {
        upgradeBar.active = false;
        upgradeActive = false;
        StopAllCoroutines();
        if (display)
            StartCoroutine(BuffSelectClose());
        player.Death();
        busy = true;
    }

    public void OpenUpgrade()
    {
        if (!busy)
        {
            if (!display)
                if (upgradeActive && upgradeBar.FillCheck())
                {
                    if (!generated)
                    {
                        generated = true;
                        DisplayAvailableBuffs();
                    }

                    StartCoroutine(Display());
                }
        }
    }

    public void CloseUpgrade()
    {
        if (!busy)
        {
            if (display)
                StartCoroutine(Close());
        }
    }

    void DisplayAvailableBuffs()
    {
        // Doesnt contain secondary weapon
        if (!appliedBuffs.ContainsKey(secondaryKey))
        {
            List<Buff> tempList = theBuffDictionary[secondaryKey];

            int[] indices = new int[2];

            for (int i = 0; i < indices.Length; i++)
            {
                indices[i] = UnityEngine.Random.Range(0, tempList.Count);

                for (int j = 0; j < i; j++)
                {
                    if (indices[i] == indices[j])
                        i -= 1;
                }
            }

            // Adding Weapons to available upgrades
            for (int i = 0; i < indices.Length; i++)
            {
                BuffToItem(tempList[indices[i]]);
            }
        }
        else // Contains secondary weapon
        {
            List<Buff> tempList = new List<Buff>(theBuffDictionary[secondaryKey]);

            for (int i = tempList.Count - 1; i >= 0; i--)
            {
                if (tempList[i] == appliedBuffs[secondaryKey][0])
                    tempList.RemoveAt(i);
            }

            int index = UnityEngine.Random.Range(0, tempList.Count);

            // Adding a weapon to available upgrade
            BuffToItem(tempList[index]);

            tempList = theBuffDictionary[upgradeKey];

            index = UnityEngine.Random.Range(0, tempList.Count);

            // Adding an upgrade to available upgrades
            BuffToItem(tempList[index]);
        }


        if (player.ReturnHealth() < player.MaxHealth())
            BuffToItem(fillHealth);
        else if (player.ReturnHealth() < player.MaxHealth() * 2)
            BuffToItem(shieldBuff);
    }

    void ClearBuffs()
    {
        group.gameObject.SetActive(true);

        for (int i = upgradeParent.childCount - 1; i >= 0; i--)
        {
            Destroy(upgradeParent.GetChild(i).gameObject);
        }

        displayedBuffs.Clear();
        generated = false;
        group.gameObject.SetActive(false);
    }

    IEnumerator Display()
    {
        busy = true;
        display = true;

        group.gameObject.SetActive(true);
        group.alpha = 0;

        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);

        yield return new WaitForSeconds(JuiceBuster.instance.fadeSpeed);

        busy = false;
    }

    IEnumerator Close()
    {
        busy = true;

        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);

        yield return new WaitForSeconds(JuiceBuster.instance.fadeSpeed);

        group.gameObject.SetActive(false);

        display = false;
        busy = false;
    }

    IEnumerator BuffSelectClose()
    {
        busy = true;

        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);

        if(content != null)
            content.EndContent();

        upgradeBar.Empty();
        upgradeActive = false;

        yield return new WaitForSeconds(JuiceBuster.instance.fadeSpeed);

        ClearBuffs();

        display = false;
        busy = false;
    }

    void BuffToItem(Buff buff)
    {
        if (buff != null)
        {
            displayedBuffs.Add(new KeyValuePair<string, Buff>(buff.Key, buff));

            // Insert code to translate buff to prefab (TO DO)
            Instantiate(buffPrefab, upgradeParent).GetComponent<BuffItem>().Assign(buff, displayedBuffs.Count);
        }
    }

    public void Buff1()
    {
        AddBuff(displayedBuffs[0]);
        ApplyAllBuffs();
        StartCoroutine(BuffSelectClose());
    }

    public void Buff2()
    {
        AddBuff(displayedBuffs[1]);
        ApplyAllBuffs();
        StartCoroutine(BuffSelectClose());
    }

    public void Buff3()
    {
        AddBuff(displayedBuffs[2]);
        ApplyAllBuffs();
        StartCoroutine(BuffSelectClose());
    }

    void AddBuff(KeyValuePair<string, Buff> kvpBuff)
    {
        if (kvpBuff.Value.Permanent)
        {
            if (kvpBuff.Key == secondaryKey)
            {
                if (appliedBuffs.ContainsKey(kvpBuff.Key))
                {
                    appliedBuffs[secondaryKey].Clear();
                    appliedBuffs[secondaryKey].Add(kvpBuff.Value);
                }
                else
                {
                    appliedBuffs.Add(kvpBuff.Key, new List<Buff>() { kvpBuff.Value });
                }

                RemoveUpgrades();
            }
            else
            {
                if (appliedBuffs.ContainsKey(kvpBuff.Key))
                {
                    appliedBuffs[kvpBuff.Key].Add(kvpBuff.Value);
                }
                else
                {
                    appliedBuffs.Add(kvpBuff.Key, new List<Buff>() { kvpBuff.Value });
                }
            }
        }
        else
            kvpBuff.Value.ApplyBuff(player);
    }

    public bool CheckOpen()
    {
        return display && !busy;
    }

    public void RemoveUpgrades()
    {
        if (appliedBuffs.ContainsKey(upgradeKey))
            appliedBuffs[upgradeKey].Clear();
    }

    void ApplyAllBuffs()
    {
        player.ResetRunTimeValues();

        // Applying weapon changes
        if (appliedBuffs.ContainsKey(secondaryKey))
            appliedBuffs[secondaryKey][0].ApplyBuff(player);

        // Resetting base weapon values
        if (player.secondary != null)
            player.secondary.ResetValues();

        // Applying Weapon buffs
        if (appliedBuffs.ContainsKey(upgradeKey))
            foreach (var buff in appliedBuffs[upgradeKey])
            {
                buff.ApplyBuff(player);
            }
    }

    void CompileDictionary()
    {
        theBuffDictionary.Clear();
        theBuffDictionary.Add(secondaryKey, new List<Buff>() { new ShotgunBuff(), new FlamethrowerBuff(), new RocketlauncherBuff(), new FreezerayBuff()});
        theBuffDictionary.Add(upgradeKey, new List<Buff>() { new FirerateBuff(), new RechargerateBuff()});

        shieldBuff = new ShieldBuff();
        fillHealth = new HealthRefillBuff();
    }
}