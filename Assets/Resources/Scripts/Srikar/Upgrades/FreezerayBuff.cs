﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezerayBuff : Buff
{
    public FreezerayBuff()
    {
        permanent = true;
        key = UpgradeManager.secondaryKey;
        definition = "Freeze-Ray";
        sprite = UpgradeManager.instance.freezerayImg;
    }

    public override void ApplyBuff(Player player)
    {
        if (player.secondary != player.freezeray)
        {
            player.secondary = player.freezeray;
            UpgradeManager.instance.RemoveUpgrades();
        }
    }
}
