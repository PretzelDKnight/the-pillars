﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quit : GameState
{
    Button quit;
    CanvasGroup group;

    float startHeight;
    float heightChange;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(quit, 0.25f);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(quit, 4);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.quitGroup.gameObject;
        group = pushDown.quitGroup;
        stateObject.SetActive(true);

        group.alpha = 0;
        quit = pushDown.quitBttn;

        PushDown.instance.ResetButtonBaseSize(quit);

        int count = pushDown.menuObject.transform.childCount;
        int index = quit.transform.GetSiblingIndex();

        int currentIndex = 0;

        for (int i = 0; i <= count; i++)
        {
            if (pushDown.menuObject.transform.GetChild(i).gameObject.activeSelf != false)
            {
                currentIndex++;
                if (pushDown.menuObject.transform.GetChild(i).gameObject == quit.gameObject)
                {
                    break;
                }
            }
        }

        startHeight = pushDown.menuObject.transform.localPosition.y - ((currentIndex - 1) * (40 + 15)) - (40 * 4 + 15) - 20;

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;

        heightChange = 0;
    }

    public override string Type()
    {
        return "Quit";
    }

    public override void UpdateState(PushDown pushDown)
    {
        float interpolation = pushDown.scrollSpeed * Time.deltaTime;

        float y = Mathf.Lerp(stateObject.transform.localPosition.y, startHeight + (-heightChange), interpolation);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = y;

        stateObject.transform.localPosition = pos;
    }
}