﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class GameState
{
    protected const float MaxHeight = -300f;

    protected GameObject stateObject;

    public abstract void OpenState();

    public abstract void UpdateState(PushDown pushDown);

    public abstract void CloseState();

    public abstract void Prep(PushDown pushDown);

    public abstract string Type();

    public void SetStateObjectFalse()
    {
        if(stateObject != null)
            stateObject.SetActive(false);
    }
}
