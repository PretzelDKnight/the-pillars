﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Start : GameState
{
    Text title;
    Text continueText;

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.startObject;
        stateObject.SetActive(true);

        title = pushDown.titleText;
        continueText = pushDown.continueText;
    }

    public override void CloseState()
    {
        JuiceBuster.instance.LerpTextFade(title, 0);
        JuiceBuster.instance.LerpTextFade(continueText, 0);
    }

    public override void OpenState()
    {
        stateObject.SetActive(true);
        JuiceBuster.instance.LerpTextFade(title, 1);
        JuiceBuster.instance.LerpTextFade(continueText, 1);
    }

    public override void UpdateState(PushDown pushDown)
    {
        if (JuiceBuster.instance.CheckNotBusy(continueText.gameObject))
        {
            if (!pushDown.Busy)
            {
                if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape))
                {
                    pushDown.PushToStack(new Menu());

                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(pushDown.beginBttn.gameObject);

                    OptionsManager.instance.confirmSFX.Play();
                }
            }
        }
    }

    public override string Type()
    {
        return "Start";
    }
}