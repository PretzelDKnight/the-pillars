﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : GameState
{
    Button leaderboard;
    CanvasGroup group;

    float startHeight;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(leaderboard, 0.25f);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(leaderboard, 4);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.leaderboardGroup.gameObject;
        group = pushDown.leaderboardGroup;
        stateObject.SetActive(true);

        startHeight = stateObject.transform.localPosition.y;

        group.alpha = 0;
        leaderboard = pushDown.leaderboardBttn;

        PushDown.instance.ResetButtonBaseSize(leaderboard);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;

        ScoreManager.instance.UpdateScoresOnLeaderboard();
    }

    public override string Type()
    {
        return "Leaderboard";
    }

    public override void UpdateState(PushDown pushDown)
    {
        float interpolation = pushDown.scrollSpeed * Time.deltaTime;

        float y = Mathf.Lerp(stateObject.transform.localPosition.y, startHeight, interpolation);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = y;

        stateObject.transform.localPosition = pos;
    }
}
