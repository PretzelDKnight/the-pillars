﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Begin : GameState
{
    Button begin;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(begin, 0.25f);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(begin, 4);
    }

    public override void Prep(PushDown pushDown)
    {
        begin = pushDown.beginBttn;
        PushDown.instance.ResetButtonBaseSize(begin);
    }

    public override string Type()
    {
        return "Begin";
    }

    public override void UpdateState(PushDown pushDown)
    {

    }
}
