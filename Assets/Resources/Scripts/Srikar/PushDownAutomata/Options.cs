﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : GameState
{
    Button options;
    CanvasGroup group;

    GameObject holder;

    float startHeight;
    float heightChange;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(options, 0.25f);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(options, 4);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.optionsGroup.gameObject;
        group = pushDown.optionsGroup;
        options = pushDown.optionsBttn;
        stateObject.SetActive(true);

        holder = pushDown.optionsHolder;

        int count = pushDown.menuObject.transform.childCount;
        int currentIndex = 0;

        for (int i = 0; i <= count; i++)
        {
            if (pushDown.menuObject.transform.GetChild(i).gameObject.activeSelf != false)
            {
                currentIndex++;
                if (pushDown.menuObject.transform.GetChild(i).gameObject == options.gameObject)
                {
                    break;
                }
            }
        }

        float holderHeight = pushDown.menuObject.transform.localPosition.y - ((currentIndex - 1) * (40 + 15)) - 30;

        Vector3 temp = holder.transform.localPosition;
        temp.y = holderHeight;
        holder.transform.localPosition = temp;

        startHeight = stateObject.transform.localPosition.y;

        group.alpha = 0;
        options = pushDown.optionsBttn;

        PushDown.instance.ResetButtonBaseSize(options);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;

        heightChange = 0;
    }

    public override string Type()
    {
        return "Options";
    }

    public override void UpdateState(PushDown pushDown)
    {
        float interpolation = pushDown.scrollSpeed * Time.deltaTime;

        float y = Mathf.Lerp(stateObject.transform.localPosition.y, startHeight + (-heightChange), interpolation);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = y;

        stateObject.transform.localPosition = pos;
    }
}
