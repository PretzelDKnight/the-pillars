﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PushDown : MonoBehaviour
{
    static public PushDown instance = null;

    Stack<GameState> pushStack;
    [SerializeField] float startTime = 1f;
    [SerializeField] float beforeStateTime = 2f;
    bool busy;
    bool back;
    bool entered;

    // Menu GameObject fields
    [SerializeField] public GameObject startObject;
    [SerializeField] public GameObject menuObject;

    // Menu Canvas Group
    [SerializeField] public CanvasGroup leaderboardGroup;
    [SerializeField] public CanvasGroup optionsGroup;
    [SerializeField] public CanvasGroup creditsGroup;
    [SerializeField] public CanvasGroup returnGroup;
    [SerializeField] public CanvasGroup quitGroup;
    [SerializeField] public CanvasGroup entryField;
    [SerializeField] public CanvasGroup restartGroup;

    // Options stuff
    [SerializeField] public CanvasGroup keybindGroup;
    [SerializeField] public CanvasGroup generalGroup;
    [SerializeField] public GameObject optionsHolder;
    [SerializeField] public Button keybindBttn;
    [SerializeField] public Button generalBttn;

    // Menu UI fields
    [SerializeField] public Text titleText;
    [SerializeField] public Text continueText;
    [SerializeField] public Button beginBttn;
    [SerializeField] public Button leaderboardBttn;
    [SerializeField] public Button optionsBttn;
    [SerializeField] public Button creditsBttn;
    [SerializeField] public Button quitBttn;
    [SerializeField] public Button exitBttn;
    [SerializeField] public Button finalScoreBttn;
    [SerializeField] public Button restartBttn;

    AudioSource bttnSound;
    AudioSource confirmSound;

    [SerializeField] public float scrollAmount;
    [SerializeField] public float scrollSpeed;

    [SerializeField] public Image graphic;
    [SerializeField] public Image deathFader;
    [SerializeField] public Image pauseFader;
    [SerializeField] public Image startFader;
    [SerializeField] public Image hitFader;

    [SerializeField] string gameScene;
    [SerializeField] string menuScene;

    [SerializeField] InputField inputField;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(this);
            instance = this;
        }

        Prep();
    }

    void Prep()
    {
        startObject.SetActive(false);
        menuObject.SetActive(false);

        leaderboardGroup.gameObject.SetActive(false);
        optionsGroup.gameObject.SetActive(false);
        creditsGroup.gameObject.SetActive(false);
        returnGroup.gameObject.SetActive(false);
        quitGroup.gameObject.SetActive(false);
        entryField.gameObject.SetActive(false);
        restartGroup.gameObject.SetActive(false);

        // Options prep
        keybindGroup.gameObject.SetActive(false);
        generalGroup.gameObject.SetActive(true);
        keybindGroup.alpha = 0;
        ResetButtonBaseSize(generalBttn, 2);

        TextSetZero(titleText);
        TextSetZero(continueText);
        ButtonSetZero(beginBttn);
        ButtonSetZero(leaderboardBttn);
        ButtonSetZero(optionsBttn);
        ButtonSetZero(creditsBttn);
        ButtonSetZero(quitBttn);
        ButtonSetZero(exitBttn);
        ButtonSetZero(finalScoreBttn);
        ButtonSetZero(restartBttn);

        graphic.gameObject.SetActive(true);
        startFader.gameObject.SetActive(true);
        pauseFader.gameObject.SetActive(false);
        hitFader.gameObject.SetActive(false);
        deathFader.gameObject.SetActive(false);

        Color col = startFader.color;
        col.a = 1;
        startFader.color = col;

        col.a = 0;
        pauseFader.color = col;

        col = hitFader.color;
        col.a = 0;
        hitFader.color = col;

        col = deathFader.color;
        col.a = 0;
        deathFader.color = col;

        bttnSound = OptionsManager.instance.buttonSFX;
        confirmSound = OptionsManager.instance.confirmSFX;
        
        StartCoroutine(StartWait());
        busy = true;
        back = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!busy)
        {
            pushStack.Peek().UpdateState(this);

            if (back)
                if (Input.GetKeyUp(KeyCode.Escape) && !busy && OptionsManager.Allow())
                {
                    back = false;
                    DirectPop();
                }
        }
    }

    public void PushToStack(GameState state)
    {
        StartCoroutine(StackPushWait(state));
    }

    public void DirectPush(GameState state)
    {
        pushStack.Push(state);
        pushStack.Peek().Prep(this);
        pushStack.Peek().OpenState();
    }

    public void DirectPop()
    {
        StartCoroutine(StackPopWait());
    }

    public void PopFromStack(int times)
    {
        StartCoroutine(StackPopWaitScreen(times));
    }

    public void PopFromStack(GameState state)
    {
        StartCoroutine(StackPopWait(state));
    }

    IEnumerator StackPushWait(GameState state)
    {
        busy = true;
        pushStack.Peek().CloseState();
        yield return new WaitForSeconds(beforeStateTime);
        pushStack.Peek().SetStateObjectFalse();

        pushStack.Push(state);
        pushStack.Peek().Prep(this);
        pushStack.Peek().OpenState();
        busy = false;
    }

    IEnumerator StackPopWaitScreen(int times)
    {
        busy = true;

        startFader.gameObject.SetActive(true);
        JuiceBuster.instance.LerpImageFade(startFader, 1);

        yield return new WaitForSeconds(beforeStateTime);

        for (int i = 0; i < times; i++)
        {
            pushStack.Peek().CloseState();
            yield return new WaitForSeconds(beforeStateTime);
            pushStack.Peek().SetStateObjectFalse();
            pushStack.Pop();
        }

        if (pushStack.Count <= 0)
        {
            pushStack.Push(new Start());
            pushStack.Push(new Menu());
        }

        pushStack.Peek().Prep(this);

        Color col = startFader.color;
        col.a = 1;
        startFader.color = col;

        pushStack.Peek().OpenState();
        busy = false;
    }

    IEnumerator StackPopWait()
    {
        busy = true;

        pushStack.Peek().CloseState();
        yield return new WaitForSeconds(beforeStateTime);
        pushStack.Peek().SetStateObjectFalse();
        pushStack.Pop();

        pushStack.Peek().CloseState();
        yield return new WaitForSeconds(beforeStateTime);
        pushStack.Peek().SetStateObjectFalse();
        pushStack.Pop();

        yield return null;
        busy = false;
    }

    IEnumerator StackPopWait(GameState state)
    {
        yield return null;
        busy = true;
        pushStack.Peek().CloseState();
        pushStack.Peek().SetStateObjectFalse();

        pushStack.Pop();

        pushStack.Push(state);
        pushStack.Peek().Prep(this);
        pushStack.Peek().OpenState();
        busy = false;
    }

    public bool Busy
    {
        get { return busy; }
    }

    void ButtonSetZero(Button bttn)
    {
        Text txt = bttn.GetComponentInChildren<Text>();
        Image img = bttn.GetComponent<Image>();

        Color col = txt.color;
        col.a = 0;
        txt.color = col;

        col = img.color;
        col.a = 0;
        img.color = col;

        ResetButtonBaseSize(bttn);

        bttn.gameObject.SetActive(false);
    }

    public void ResetButtonBaseSize(Button bttn, int ratio = 1)
    {
        Text txt = bttn.GetComponentInChildren<Text>();
        Image img = bttn.GetComponent<Image>();

        // Resetting base size values
        txt.fontSize = 50 * ratio;
        Vector2 temp = txt.rectTransform.sizeDelta;
        temp.y = 50 * ratio;

        if (ratio == 1)
        {
            if (temp.x >= 180)
                temp.x = 180;
        }
        else
            temp.x *= ratio;

        txt.rectTransform.sizeDelta = temp;
        temp.y = 40 * ratio;
        img.rectTransform.sizeDelta = temp;
    }

    void TextSetZero(Text txt)
    {
        Color col = txt.color;
        col.a = 0;
        txt.color = col;
    }

    void ImageScaleZero(Image img)
    {
        img.rectTransform.sizeDelta = Vector2.zero;
    }

    public void SetButtonsFalse()
    {
        beginBttn.gameObject.SetActive(false);
        ButtonSetZero(beginBttn);
        leaderboardBttn.gameObject.SetActive(false);
        ButtonSetZero(leaderboardBttn);
        optionsBttn.gameObject.SetActive(false);
        ButtonSetZero(optionsBttn);
        exitBttn.gameObject.SetActive(false);
        ButtonSetZero(exitBttn);
        creditsBttn.gameObject.SetActive(false);
        ButtonSetZero(creditsBttn);
        quitBttn.gameObject.SetActive(false);
        ButtonSetZero(quitBttn);
        finalScoreBttn.gameObject.SetActive(false);
        ButtonSetZero(finalScoreBttn);
        restartBttn.gameObject.SetActive(false);
        ButtonSetZero(restartBttn);
    }

    public void PushBegin()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "Begin")
            {
                PopFromStack(new Begin());

                bttnSound.Play();
            }
            else
            {
                PopFromStack(new Game());
                confirmSound.Play();
            }
        }
    }

    public void PushLeaderboard()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "Leaderboard")
            {
                PopFromStack(new Leaderboard());

                bttnSound.Play();
            }
        }
    }

    public void PushOptions()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "Options")
            {
                PopFromStack(new Options());

                bttnSound.Play();
            }
        }
    }

    public void PushCredits()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "Credits")
            {
                PopFromStack(new Credits());

                bttnSound.Play();
            }
        }
    }

    public void PushQuit()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "Quit")
            {
                PopFromStack(new Quit());

                bttnSound.Play();
            }
            else
            {
                // Insert code to quit
                confirmSound.Play();
            }
        }
    }

    public void PushReturn()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "Return")
            {
                PopFromStack(new Return());

                bttnSound.Play();
            }
            else
            {
                WaveManager.instance.EndFunction();
                UpgradeManager.instance.EndFunction();
                NotificationManager.instance.ClearNotifications();

                PopFromStack(3);
                confirmSound.Play();
            }
        }
    }

    public void PushFinalScore()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "FinalScore")
            {
                PopFromStack(new FinalScore());

                bttnSound.Play();
            }
        }
    }

    public void PushRestart()
    {
        if (ButtonBusyCheck() && !busy && OptionsManager.Allow())
        {
            if (pushStack.Peek().Type() != "Restart")
            {
                PopFromStack(new Restart());

                bttnSound.Play();
            }
            else
            {
                // Insert Restart functionality
                WaveManager.instance.EndFunction();
                UpgradeManager.instance.EndFunction();
                NotificationManager.instance.ClearNotifications();

                PopFromStack(2);
                confirmSound.Play();
            }
        }
    }

    public void ShowScreenSound()
    {
        if (!busy && OptionsManager.Allow())
        {
            if (OptionsBusyCheck() && pushStack.Peek().Type() == "Options")
            {
                if (!generalGroup.gameObject.activeSelf)
                {
                    confirmSound.Play();
                    JuiceBuster.instance.LerpCanvasGroupDeactivate(keybindGroup);
                    generalGroup.gameObject.SetActive(true);
                    JuiceBuster.instance.LerpCanvasGroupFade(generalGroup, 1);

                    ResetButtonBaseSize(generalBttn);

                    JuiceBuster.instance.LerpButtonScale(generalBttn, 2f);
                    JuiceBuster.instance.LerpButtonScale(keybindBttn, 0.5f);
                }
            }
        }
    }

    public void ShowKeybind()
    {
        if (!busy && OptionsManager.Allow())
        {
            if (OptionsBusyCheck() && pushStack.Peek().Type() == "Options")
            {
                if (!keybindGroup.gameObject.activeSelf)
                {
                    confirmSound.Play();
                    JuiceBuster.instance.LerpCanvasGroupDeactivate(generalGroup);
                    keybindGroup.gameObject.SetActive(true);
                    JuiceBuster.instance.LerpCanvasGroupFade(keybindGroup, 1);

                    ResetButtonBaseSize(keybindBttn);

                    JuiceBuster.instance.LerpButtonScale(keybindBttn, 2f);
                    JuiceBuster.instance.LerpButtonScale(generalBttn, 0.5f);
                }
            }
        }
    }

    public void SetName()
    {
        if(JuiceBuster.instance.CheckNotBusy(entryField.gameObject) && !busy)
        {
            bttnSound.Play();

            SetEntered = true;

            ScoreManager.instance.EndPlayScore(inputField.text);

            JuiceBuster.instance.LerpCanvasGroupFade(entryField, 0, 0.5f);
        }
    }

    IEnumerator StartWait()
    {
        yield return new WaitForSeconds(startTime);

        JuiceBuster.instance.LerpImageDeactivate(startFader, 0);

        yield return new WaitForSeconds(beforeStateTime);

        pushStack = new Stack<GameState>();

        pushStack.Push(new Start());
        pushStack.Peek().Prep(this);
        pushStack.Peek().OpenState();
        busy = false;
    }

    public void LoadGameScene()
    {
        StartCoroutine(GameSceneLoader());
    }

    public void LoadMenuScene()
    {
        StartCoroutine(MenuSceneLoader());
    }

    IEnumerator GameSceneLoader()
    {
        graphic.gameObject.SetActive(false);
        menuObject.SetActive(false);
        startFader.gameObject.SetActive(true);

        yield return SceneManager.LoadSceneAsync(gameScene, LoadSceneMode.Single);

        JuiceBuster.instance.LerpImageDeactivate(startFader, 0, 1f);

        ScoreManager.instance.NewPlayScore();

        while (WaveManager.instance == null)
        {
            yield return null;
        }

        if (pushStack.Peek().Type() == "Game")
        {
            Game temp = pushStack.Peek() as Game;
            temp.SetLoaded();
        }

        WaveManager.instance.NewWave();
    }

    IEnumerator MenuSceneLoader()
    {
        startFader.gameObject.SetActive(true);
        menuObject.SetActive(true);
        yield return SceneManager.LoadSceneAsync(menuScene, LoadSceneMode.Single);

        graphic.gameObject.SetActive(true);
        JuiceBuster.instance.LerpImageFade(graphic, 0.2f);

        yield return JuiceBuster.instance.CheckNotBusy(graphic.gameObject);

        JuiceBuster.instance.LerpImageDeactivate(startFader, 0, 1f);
    }

    public bool SetBack
    {
        get { return back; }
        set { back = value; }
    }

    public bool SetEntered
    {
        get { return entered; }
        set { entered = value; }
    }

    public string ReturnMenuState()
    {
        return pushStack.Peek().Type();
    }

    bool ButtonBusyCheck()
    {
        if (JuiceBuster.instance.CheckNotBusy(beginBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(leaderboardBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(optionsBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(creditsBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(quitBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(exitBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(finalScoreBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(restartBttn.gameObject))
            return true;

        return false;
    }

    bool OptionsBusyCheck()
    {
        if (JuiceBuster.instance.CheckNotBusy(generalGroup.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(keybindGroup.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(generalBttn.gameObject) &&
            JuiceBuster.instance.CheckNotBusy(keybindBttn.gameObject))
            return true;

        return false;
    }

    public void HitScreen()
    {
        StartCoroutine(HitEffect());
    }

    IEnumerator HitEffect()
    {
        hitFader.gameObject.SetActive(true);
        JuiceBuster.instance.LerpImageFade(hitFader, 0.5f, 0.1f);

        Camera.main.GetComponent<CameraScript>().CameraShake();

        yield return new WaitForSeconds(0.2f);

        JuiceBuster.instance.LerpImageDeactivate(hitFader, 0, 0.5f);
    }
}