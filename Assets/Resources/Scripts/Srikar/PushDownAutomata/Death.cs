﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Death : GameState
{
    Image bgFader;
    Image graphic;

    Button exit;
    Button quit;
    Button score;
    Button restart;

    bool activate;

    public override void CloseState()
    {
        exit.interactable = false;
        quit.interactable = false;
        score.interactable = false;
        restart.interactable = false;

        JuiceBuster.instance.LerpImageDeactivate(graphic, 0);
        JuiceBuster.instance.LerpImageDeactivate(bgFader, 0);
        JuiceBuster.instance.LerpButtonFade(quit, 0);
        JuiceBuster.instance.LerpButtonFade(exit, 0);
        JuiceBuster.instance.LerpButtonFade(score, 0);
        JuiceBuster.instance.LerpButtonFade(restart, 0);
    }

    public override void OpenState()
    {
        exit.interactable = false;
        quit.interactable = false;
        score.interactable = false;
        restart.interactable = false;

        JuiceBuster.instance.LerpImageFade(bgFader, 0.8f);
    }

    public override void Prep(PushDown pushDown)
    {
        bgFader = pushDown.deathFader;

        stateObject = pushDown.menuObject;
        stateObject.SetActive(true);

        graphic = pushDown.graphic;
        graphic.gameObject.SetActive(true);

        pushDown.SetButtonsFalse();

        exit = pushDown.exitBttn;
        quit = pushDown.quitBttn;
        score = pushDown.finalScoreBttn;
        restart = pushDown.restartBttn;

        exit.gameObject.SetActive(true);
        quit.gameObject.SetActive(true);
        score.gameObject.SetActive(true);
        restart.gameObject.SetActive(true);

        Color col = bgFader.color;
        col.a = 0;
        bgFader.color = col;

        bgFader.gameObject.SetActive(true);
        activate = false;
    }

    public override string Type()
    {
        return "Death";
    }

    public override void UpdateState(PushDown pushDown)
    {
        if(JuiceBuster.instance.CheckNotBusy(bgFader.gameObject))
        {
            if (!activate)
            {
                JuiceBuster.instance.LerpImageFade(graphic, 0.2f);
                JuiceBuster.instance.LerpButtonFade(quit, 1);
                JuiceBuster.instance.LerpButtonFade(exit, 1);
                JuiceBuster.instance.LerpButtonFade(score, 1);
                JuiceBuster.instance.LerpButtonFade(restart, 1);
                activate = true;
            }
            else
            {
                if (JuiceBuster.instance.CheckNotBusy(score.gameObject))
                {
                    exit.interactable = true;
                    quit.interactable = true;
                    score.interactable = true;
                    restart.interactable = true;

                    pushDown.DirectPush(new FinalScore());
                }
            }
        }
    }
}
