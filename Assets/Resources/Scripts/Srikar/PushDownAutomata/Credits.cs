﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credits : GameState
{
    Button credits;
    CanvasGroup group;
    Scrollbar bar;

    float startHeight;
    float heightChange;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(credits, 0.25f);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(credits, 4);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.creditsGroup.gameObject;
        group = pushDown.creditsGroup;
        stateObject.SetActive(true);

        startHeight = stateObject.transform.localPosition.y;

        group.alpha = 0;
        credits = pushDown.creditsBttn;

        PushDown.instance.ResetButtonBaseSize(credits);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;

        heightChange = 0;
    }

    public override string Type()
    {
        return "Credits";
    }

    public override void UpdateState(PushDown pushDown)
    {
        float interpolation = pushDown.scrollSpeed * Time.deltaTime;

        float y = Mathf.Lerp(stateObject.transform.localPosition.y, startHeight + (-heightChange), interpolation);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = y;

        stateObject.transform.localPosition = pos;
    }
}
