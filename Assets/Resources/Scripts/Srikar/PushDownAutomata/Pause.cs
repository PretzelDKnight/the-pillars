﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : GameState
{
    Button options;
    Button exit;
    Button quit;
    Button restart;

    Image graphic;
    Image fader;

    float startHeight;
    float heightChange;

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.menuObject;
        stateObject.SetActive(true);

        pushDown.SetButtonsFalse();

        graphic = pushDown.graphic;
        graphic.gameObject.SetActive(true);

        fader = pushDown.pauseFader;
        fader.gameObject.SetActive(true);

        options = pushDown.optionsBttn;
        exit = pushDown.exitBttn;
        quit = pushDown.quitBttn;
        restart = pushDown.restartBttn;

        options.gameObject.SetActive(true);
        exit.gameObject.SetActive(true);
        quit.gameObject.SetActive(true);
        restart.gameObject.SetActive(true);

        PushDown.instance.SetBack = false;
    }

    public override void CloseState()
    {
        options.interactable = false;
        exit.interactable = false;
        quit.interactable = false;
        restart.interactable = false;

        JuiceBuster.instance.LerpImageDeactivate(graphic, 0);
        JuiceBuster.instance.LerpImageDeactivate(fader, 0);
        JuiceBuster.instance.LerpButtonFade(options, 0);
        JuiceBuster.instance.LerpButtonFade(exit, 0);
        JuiceBuster.instance.LerpButtonFade(quit, 0);
        JuiceBuster.instance.LerpButtonFade(restart, 0);
    }

    public override void OpenState()
    {
        options.interactable = false;
        exit.interactable = false;
        quit.interactable = false;
        restart.interactable = false;

        JuiceBuster.instance.LerpImageFade(graphic, 0.2f);
        JuiceBuster.instance.LerpImageFade(fader, 0.4f);
        JuiceBuster.instance.LerpButtonFade(options, 1);
        JuiceBuster.instance.LerpButtonFade(quit, 1);
        JuiceBuster.instance.LerpButtonFade(exit, 1);
        JuiceBuster.instance.LerpButtonFade(restart, 1);
    }

    public override string Type()
    {
        return "Pause";
    }

    public override void UpdateState(PushDown pushDown)
    {
        if (JuiceBuster.instance.CheckNotBusy(options.gameObject))
        {
            options.interactable = true;
            exit.interactable = true;
            quit.interactable = true;
            restart.interactable = true;

            pushDown.DirectPush(new Restart());
            PushDown.instance.SetBack = true;
        }
    }
}