﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Return : GameState
{
    Button exit;
    CanvasGroup group;

    float startHeight;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(exit, 0.25f);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(exit, 4);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.returnGroup.gameObject;
        group = pushDown.returnGroup;
        stateObject.SetActive(true);

        group.alpha = 0;
        exit = pushDown.exitBttn;

        PushDown.instance.ResetButtonBaseSize(exit);

        int count = pushDown.menuObject.transform.childCount;

        int currentIndex = 0;

        for (int i = 0; i <= count; i++)
        {
            if (pushDown.menuObject.transform.GetChild(i).gameObject.activeSelf != false)
            {
                currentIndex++;
                if (pushDown.menuObject.transform.GetChild(i).gameObject == exit.gameObject)
                {
                    break;
                }
            }
        }

        startHeight = pushDown.menuObject.transform.localPosition.y - ((currentIndex - 1) * (40 + 15)) - (40 + 15) - 20;

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;
    }

    public override string Type()
    {
        return "Return";
    }

    public override void UpdateState(PushDown pushDown)
    {
        float interpolation = pushDown.scrollSpeed * Time.deltaTime;

        float y = Mathf.Lerp(stateObject.transform.localPosition.y, startHeight, interpolation);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = y;

        stateObject.transform.localPosition = pos;
    }
}