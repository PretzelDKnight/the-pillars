﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Restart : GameState
{
    Button restart;
    CanvasGroup group;

    float startHeight;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(restart, 0.25f);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(restart, 4);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.restartGroup.gameObject;
        group = pushDown.restartGroup;
        stateObject.SetActive(true);

        group.alpha = 0;
        restart = pushDown.restartBttn;

        PushDown.instance.ResetButtonBaseSize(restart);

        int count = pushDown.menuObject.transform.childCount;
        int index = restart.transform.GetSiblingIndex();

        int currentIndex = 0;

        for (int i = 0; i <= count; i++)
        {
            if (pushDown.menuObject.transform.GetChild(i).gameObject.activeSelf != false)
            {
                currentIndex++;
                if (pushDown.menuObject.transform.GetChild(i).gameObject == restart.gameObject)
                {
                    break;
                }
            }
        }

        startHeight = pushDown.menuObject.transform.localPosition.y - ((currentIndex - 1) * (40 + 15)) - (40 + 15) - 20;

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;
    }

    public override string Type()
    {
        return "Restart";
    }

    public override void UpdateState(PushDown pushDown)
    {
        float interpolation = pushDown.scrollSpeed * Time.deltaTime;

        float y = Mathf.Lerp(stateObject.transform.localPosition.y, startHeight, interpolation);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = y;

        stateObject.transform.localPosition = pos;
    }
}
