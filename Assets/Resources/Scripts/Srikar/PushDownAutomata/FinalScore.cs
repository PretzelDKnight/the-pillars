﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalScore : GameState
{
    Button score;
    CanvasGroup group;
    CanvasGroup entryGroup;

    float startHeight;
    float heightChange;

    bool entered;

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonScale(score, 0.25f);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 0);
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpButtonScale(score, 4);
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }

    public override void Prep(PushDown pushDown)
    {
        entryGroup = pushDown.entryField;

        score = pushDown.finalScoreBttn;

        PushDown.instance.ResetButtonBaseSize(score);

        if (pushDown.SetEntered)
            PrepScores(pushDown);
        else
            PrepEntry(pushDown);
    }

    public override string Type()
    {
        return "FinalScore";
    }

    public override void UpdateState(PushDown pushDown)
    {
        float interpolation = pushDown.scrollSpeed * Time.deltaTime;

        float y = Mathf.Lerp(stateObject.transform.localPosition.y, startHeight + (-heightChange), interpolation);

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = y;

        stateObject.transform.localPosition = pos;

        if (JuiceBuster.instance.CheckNotBusy(entryGroup.gameObject))
        {
            if(Input.GetKeyUp(InputManager.Confirm))
            {
                pushDown.SetName();
            }
        }

        if (entryGroup.alpha == 0 && !entered)
        {
            entered = true;
            entryGroup.gameObject.SetActive(false);
            PrepScores(pushDown);
            OpenScores();
        }
    }

    void PrepScores(PushDown pushDown)
    {
        stateObject = pushDown.leaderboardGroup.gameObject;
        group = pushDown.leaderboardGroup;

        group.alpha = 0;

        stateObject.SetActive(true);

        startHeight = stateObject.transform.localPosition.y;

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;

        heightChange = 0;

        entered = true;

        ScoreManager.instance.UpdateScoresOnLeaderboard();
    }

    void PrepEntry(PushDown pushDown)
    {
        stateObject = pushDown.entryField.gameObject;
        group = pushDown.entryField;

        group.alpha = 0;

        stateObject.SetActive(true);

        startHeight = stateObject.transform.localPosition.y;

        Vector3 pos = stateObject.transform.localPosition;
        pos.y = startHeight - MaxHeight;
        stateObject.transform.localPosition = pos;

        heightChange = 0;

        entered = false;
    }

    void OpenScores()
    {
        JuiceBuster.instance.LerpCanvasGroupFade(group, 1);
    }
}