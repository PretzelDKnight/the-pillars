﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : GameState
{
    Button begin;
    Button leaderboard;
    Button options;
    Button credits;
    Button quit;

    public override void Prep(PushDown pushDown)
    {
        stateObject = pushDown.menuObject;
        stateObject.SetActive(true);

        pushDown.SetButtonsFalse();

        begin = pushDown.beginBttn;
        leaderboard = pushDown.leaderboardBttn;
        options = pushDown.optionsBttn;
        credits = pushDown.creditsBttn;
        quit = pushDown.quitBttn;

        begin.gameObject.SetActive(true);
        leaderboard.gameObject.SetActive(true);
        options.gameObject.SetActive(true);
        credits.gameObject.SetActive(true);
        quit.gameObject.SetActive(true);
    }

    public override void CloseState()
    {
        JuiceBuster.instance.LerpButtonFade(begin, 0);
        JuiceBuster.instance.LerpButtonFade(leaderboard, 0);
        JuiceBuster.instance.LerpButtonFade(options, 0);
        JuiceBuster.instance.LerpButtonFade(credits, 0);
        JuiceBuster.instance.LerpButtonFade(quit, 0);

        begin.interactable = false;
        leaderboard.interactable = false;
        options.interactable = false;
        credits.interactable = false;
        quit.interactable = false;
    }

    public override void OpenState()
    {
        begin.interactable = false;
        leaderboard.interactable = false;
        options.interactable = false;
        credits.interactable = false;
        quit.interactable = false;

        JuiceBuster.instance.LerpButtonFade(begin, 1);
        JuiceBuster.instance.LerpButtonFade(leaderboard, 1);
        JuiceBuster.instance.LerpButtonFade(options, 1);
        JuiceBuster.instance.LerpButtonFade(credits, 1);
        JuiceBuster.instance.LerpButtonFade(quit, 1);
    }

    public override void UpdateState(PushDown pushDown)
    {
        if (JuiceBuster.instance.CheckNotBusy(begin.gameObject))
        {
            begin.interactable = true;
            leaderboard.interactable = true;
            options.interactable = true;
            credits.interactable = true;
            quit.interactable = true;

            pushDown.DirectPush(new Begin());
        }
    }

    public override string Type()
    {
        return "Menu";
    }
}
