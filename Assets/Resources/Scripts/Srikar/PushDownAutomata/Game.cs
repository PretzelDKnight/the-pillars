﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : GameState
{
    Image fader;
    bool loaded;
    bool sceneSet;

    Player player;

    public override void CloseState()
    {
        PushDown.instance.LoadMenuScene(); 
        PushDown.instance.beginBttn.interactable = true;
    }

    public override void OpenState()
    {
        JuiceBuster.instance.LerpImageFade(fader, 1, 1f);
    }

    public override void Prep(PushDown pushDown)
    {
        fader = pushDown.startFader;
        fader.gameObject.SetActive(true);

        pushDown.beginBttn.interactable = false;

        Color col = fader.color;
        col.a = 0;
        fader.color = col;

        loaded = false;
        sceneSet = false;

        pushDown.SetEntered = false;
    }

    public override string Type()
    {
        return "Game";
    }

    public override void UpdateState(PushDown pushDown)
    {
        if (JuiceBuster.instance.CheckNotBusy(fader.gameObject) && !sceneSet)
        {
            sceneSet = true;
            pushDown.LoadGameScene();
        }

        if (loaded)
        {
            Inputs(pushDown);

            if (player == null)
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

            if (player.ReturnHealth() <= 0)
            {
                WaveManager.instance.EndFunction();
                UpgradeManager.instance.EndFunction();
                NotificationManager.instance.ClearNotifications();

                // Moving to death menu
                PushDown.instance.DirectPush(new Death());
            }
        }
    }

    public void SetLoaded()
    {
        loaded = true;
    }

    void Inputs(PushDown pushDown)
    {
        if (UpgradeManager.instance.CheckOpen())
        {
            if (Input.GetKeyUp(InputManager.Upgrade1))
                UpgradeManager.instance.Buff1();
            else if (Input.GetKeyUp(InputManager.Upgrade2))
                UpgradeManager.instance.Buff2();
            else if (Input.GetKeyUp(InputManager.Upgrade3))
                UpgradeManager.instance.Buff3();
        }

        if (Input.GetKeyUp(InputManager.Back))
        {
            if (UpgradeManager.instance.CheckOpen())
                UpgradeManager.instance.CloseUpgrade();
            else
                pushDown.DirectPush(new Pause());
        }
        else if (Input.GetKeyUp(InputManager.OpenUpgrade))
            UpgradeManager.instance.OpenUpgrade();
    }
}
