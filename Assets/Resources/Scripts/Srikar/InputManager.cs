﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager instance = null;

    [SerializeField] KeyCode confirm = KeyCode.Return;

    [SerializeField] KeyCode up = KeyCode.W;
    [SerializeField] KeyCode down = KeyCode.S;
    [SerializeField] KeyCode left = KeyCode.A;
    [SerializeField] KeyCode right = KeyCode.D;

    [SerializeField] KeyCode jump = KeyCode.Space;

    [SerializeField] KeyCode primary = KeyCode.Mouse0;
    [SerializeField] KeyCode secondary = KeyCode.Mouse1;

    [SerializeField] KeyCode upgradeInit = KeyCode.Q;
    [SerializeField] KeyCode back = KeyCode.Escape;

    static KeyCode backKey;

    [SerializeField] KeyCode upgrade1 = KeyCode.Alpha1;
    [SerializeField] KeyCode upgrade2 = KeyCode.Alpha2;
    [SerializeField] KeyCode upgrade3 = KeyCode.Alpha3;

    static Dictionary<string, KeyCode> keycodeDictionary;

    bool bindsLoaded;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        keycodeDictionary = new Dictionary<string, KeyCode>();

        DictionarySetDefault();
        bindsLoaded = false;
    }

    private void Update()
    {
        if (!bindsLoaded)
        {
            bindsLoaded = true;
            if(OptionsManager.instance != null)
                OptionsManager.instance.KeybindsFromPrefs();
        }
    }

    public void DictionarySetDefault()
    {
        keycodeDictionary.Clear();

        keycodeDictionary.Add("Up", up);
        keycodeDictionary.Add("Down", down);
        keycodeDictionary.Add("Left", left);
        keycodeDictionary.Add("Right", right);
        keycodeDictionary.Add("Jump", jump);
        keycodeDictionary.Add("Primary", primary);
        keycodeDictionary.Add("Secondary", secondary);
        keycodeDictionary.Add("OpenUpgrade", upgradeInit);
        keycodeDictionary.Add("Upgrade1", upgrade1);
        keycodeDictionary.Add("Upgrade2", upgrade2);
        keycodeDictionary.Add("Upgrade3", upgrade3);
        keycodeDictionary.Add("Confirm", confirm);

        backKey = back;
    }

    public static KeyCode Up
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Up"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Down
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Down"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Left
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Left"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Right
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Right"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Jump
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Jump"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Primary
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Primary"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Secondary
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Secondary"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode OpenUpgrade
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["OpenUpgrade"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Upgrade1
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Upgrade1"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Upgrade2
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Upgrade2"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Upgrade3
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Upgrade3"];
            else
                return KeyCode.None;
        }
    }

    public static KeyCode Back
    {
        get 
        {
                return backKey;
        }
    }

    public static KeyCode Confirm
    {
        get 
        {
            if (OptionsManager.Allow())
                return keycodeDictionary["Confirm"];
            else
                return KeyCode.None;
        }
    }

    public static void SetKey(string str, KeyCode keyCode)
    {
        if (keycodeDictionary.ContainsKey(str))
        {
            keycodeDictionary[str] = keyCode;
        }
        else
            Debug.LogError("Dictionary doesnt contain this key! Cannot Set");
    }

    public static KeyCode GetKey(string str)
    {
        if (keycodeDictionary.ContainsKey(str))
        {
            return keycodeDictionary[str];
        }
        else
            Debug.LogError("Dictionary doesnt contain this key! Cannot Get");

        return KeyCode.None;
    }

    static public Dictionary<string, KeyCode> ReturnDictionary()
    {
        return keycodeDictionary;
    }
}