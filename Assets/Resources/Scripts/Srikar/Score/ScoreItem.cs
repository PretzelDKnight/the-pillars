﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreItem : MonoBehaviour
{
    [SerializeField] Text position;
    [SerializeField] Text name;
    [SerializeField] Text waves;
    [SerializeField] Text score;

    public void CopyFromScore(Score scoreObj)
    {
        position.text = scoreObj.position.ToString();
        name.text = scoreObj.name;
        waves.text = scoreObj.waves.ToString();
        score.text = scoreObj.score.ToString();
    }
}
