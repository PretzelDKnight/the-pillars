﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class Score
{
    public int position;
    public string name;
    public int waves;
    public int score;
}

public class ScoreList
{
    public List<Score> list = new List<Score>();
}

public class ScoreManager : MonoBehaviour
{
    static public ScoreManager instance = null;

    [SerializeField] GameObject scoreObject;
    [SerializeField] Text lastPlay;
    [SerializeField] GameObject empty;
    [SerializeField] GameObject scoreParent;
    [SerializeField] int scoreItemLimit = 8;
    [SerializeField] float scoreSpacing = 10;

    ScoreList scoreList;

    Score currentScore;

    bool played;

    bool save;

    float scoreItemHeight;

    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(this);
            instance = this;
        }

        Prep();
        LoadScores();
    }

    void Prep()
    {
        played = false;
        save = false;
        scoreList = new ScoreList();
        scoreItemHeight = scoreObject.GetComponent<RectTransform>().sizeDelta.y + scoreSpacing;
    }

    public void LoadScores()
    {
        if (File.Exists(Application.persistentDataPath + "/gamescores.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamescores.save", FileMode.Open);
            string json = (string)bf.Deserialize(file);
            file.Close();

            scoreList = JsonUtility.FromJson<ScoreList>(json);
        }
    }

    public void SaveNewScores()
    {
        if (save)
        {
            save = false;

            string json = JsonUtility.ToJson(scoreList);
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/gamescores.save");
            bf.Serialize(file, json);
            file.Close();
        }
    }

    public void NewPlayScore()
    {
        played = true;

        currentScore = new Score();

        currentScore.position = 0;
        currentScore.name = "Anonymous Egg";
        currentScore.waves = 0;
        currentScore.score = 0;
    }

    public void UpdateScoresOnLeaderboard()
    {
        // Removing current scores in scoreboard
        for (int i = scoreParent.transform.childCount - 1; i >= 0; i--)
            Destroy(scoreParent.transform.GetChild(i).gameObject);

        for(int i = 0; i < scoreItemLimit; i++)
        {
            if (i <= scoreList.list.Count - 1)
            {
                ScoreItem item = Instantiate(scoreObject, scoreParent.transform).GetComponent<ScoreItem>();
                item.CopyFromScore(scoreList.list[i]);
            }
        }

        if (scoreList.list.Count == 0)
        {
            Instantiate(empty, scoreParent.transform);
        }

        if (played)
        {
            Instantiate(lastPlay, scoreParent.transform);
            ScoreItem temp = Instantiate(scoreObject, scoreParent.transform).GetComponent<ScoreItem>();
            temp.CopyFromScore(currentScore);
        }
    }

    public void EndPlayScore(string name)
    {
        currentScore.name = name;

        save = true;
        ScoreSort();
        SaveNewScores();
    }

    void ScoreSort()
    {
        int pos = 0;
        if (played)
        {
            scoreList.list.Add(currentScore);
            pos = scoreList.list.Count - 1;
        }

        if (scoreList.list.Count > 0)
        {
            // Sorting positions
            for (int j = scoreList.list.Count - 1; j > 0; j--)
            {
                for (int i = 0; i < j; i++)
                {
                    if (scoreList.list[i].score < scoreList.list[i + 1].score)
                    {
                        Score temp = scoreList.list[i];
                        scoreList.list[i] = scoreList.list[i + 1];
                        scoreList.list[i + 1] = temp;

                        if(played)
                        if (pos == i + 1)
                            pos = i;
                    }
                }
            }

            // Assigning new Positions
            for (int i = 0; i < scoreList.list.Count; i++)
            {
                Score temp = scoreList.list[i];
                temp.position = i + 1;
                scoreList.list[i] = temp;
            }

            if (played)
            {
                currentScore.position = pos + 1;
            }
        }

    }

    public float ScoreListHeight()
    {
        return (scoreList.list.Count - 1 ) * scoreItemHeight;
    }

    public void UpdateScore(int score)
    {
        currentScore.score += score;
    }

    public void UpdateWave()
    {
        currentScore.waves += 1;
    }

    public int CurrentWave()
    {
        return currentScore.waves + 1;
    }
}