﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using System;

public enum JumpState
{
    Grounded,
    Jumping,
    Falling
}

public class Player : MonoBehaviour
{
    public static Rigidbody rb;

    // Player Values
    [SerializeField] int health;
    [SerializeField] float moveSpeed;
    [SerializeField] float airTime = 1f;
    [SerializeField] float jumpHeight = 1f;
    [SerializeField] public JumpState jumpState;
    [SerializeField] LayerMask sphereHitLayer;
    [SerializeField] LayerMask raycastLayer;
    [SerializeField] float maxRayCastLength = 40f;
    [SerializeField] float speedUpFactor;
    [SerializeField] bool invulnerable = false;
    [SerializeField] float invulerabilityTimer = 3;
    [SerializeField] float blinkTime = 1;
    [SerializeField] float kbForce = 3;
    [SerializeField] float upMultiplier = 2;

    // Runtime Variables
    int rt_health;
    float rt_moveSpeed;
    float rt_airTime;
    float rt_jumpHeight;

    // Renderer Variables
    MeshRenderer renderer;

    // Current Values
    int currentHealth;

    // Direction Variables (no touch)
    Vector3 velocity;
    Vector3 forward;
    Vector3 right;
    public Node currentNode;

    // Processing Variables (no touch)
    float jump;
    public bool doubleJump;
    public float halfHeight;
    public float currentHeightScale;
    float appliedSpeed;

    [SerializeField] GameObject head;
    [SerializeField] Weapon primary;
    [NonSerialized] public Weapon secondary;

    [SerializeField] public Shotgun shotgun;
    [SerializeField] public Flamethrower flamethrower;
    [SerializeField] public FreezeRay freezeray;
    [SerializeField] public RocketLauncher rocketlauncher;
    [SerializeField] public float aimSphereRadius = 1f;

    Vector3 target;

    CameraScript cam;
    bool alive;

    private void Awake()
    {
        rt_health = health;
        Debug.Log(rt_health);
    }

    // Start is called before the first frame update
    void Start()
    {
        cam = FindObjectOfType<CameraScript>();

        Prep();

        ResetRunTimeValues();
        //rt_health = health;
        //Debug.Log(rt_health);

        //secondary = rocketlauncher;
    }

    // Update is called once per frame
    void Update()
    {
        if (alive)
        {
            GroundCheck();
            Jump();
            Aim();
            UseWeapons();

            if (rt_health <= 0)
            {
                alive = false;
            }
        }
    }

    private void FixedUpdate()
    {
        if (alive)
        {
            Move();
            //FindCurrentNode();
        }
        else
        {
            rb.velocity = Vector3.zero;
        }

        ApplyGravity();
    }

    public void ResetRunTimeValues()
    {
        //rt_health = health;
        rt_moveSpeed = moveSpeed;
        rt_airTime = airTime;
        rt_jumpHeight = jumpHeight;
    }

    private void Move()
    {
        float horizontal = 0;
        float vertical = 0;

        //Debug.DrawRay(transform.position, rb.velocity, Color.blue);

        if (Input.GetKey(InputManager.Up))
            vertical += 1f;
        if (Input.GetKey(InputManager.Down))
            vertical -= 1f;
        if (Input.GetKey(InputManager.Right))
            horizontal += 1f;
        if (Input.GetKey(InputManager.Left))
            horizontal -= 1f;

        velocity = new Vector3(horizontal, 0, vertical);

        Vector3 currentVel = rb.velocity;
        currentVel.y = 0;

        if (currentVel.magnitude <= 0.1f)
            appliedSpeed = 0;

        float interpolation = speedUpFactor * Time.deltaTime;

        appliedSpeed = Mathf.Lerp(appliedSpeed, rt_moveSpeed, interpolation);

        velocity = velocity.normalized * appliedSpeed;

        float verticalSpeed = rb.velocity.y;

        rb.velocity = forward * velocity.z;
        rb.velocity += right * velocity.x;

        rb.velocity += Vector3.up * verticalSpeed;

        float animVal = Vector3.Dot(head.transform.forward, velocity);
    }

    private void ApplyGravity()
    {
        // Gravity formula (no touch)
        float gravity = -2f * rt_jumpHeight / (rt_airTime * rt_airTime) * Time.deltaTime;

        rb.velocity += Vector3.up * gravity;
    }

    private void Jump()
    {
        if (Input.GetKeyDown(InputManager.Jump))
        {
            if (jumpState == JumpState.Grounded && doubleJump)
                StartCoroutine(JumpRoutine());
            else if (doubleJump)
            {
                doubleJump = false;
                StopCoroutine(JumpRoutine()); // Stopping Coroutine
                Vector3 vl = rb.velocity;
                vl.y = 0;
                rb.velocity = vl;
                StartCoroutine(JumpRoutine()); // Starting it again
            }
        }
    }

    private void Aim()
    {
        RaycastHit hit;
        var mousePosition = Input.mousePosition;
        Ray rayMouse = Camera.main.ScreenPointToRay(mousePosition);

        if (Physics.Raycast(rayMouse.origin, rayMouse.direction, out hit, maxRayCastLength, raycastLayer))
        {
            Collider[] temp = Physics.OverlapSphere(hit.point, aimSphereRadius, sphereHitLayer);

            if (temp.Length != 0)
            {
                float nearest = Mathf.Infinity;
                Collider tempTarget = temp[0];

                foreach (var collider in temp)
                {
                    float sqrDist = Vector3.SqrMagnitude(hit.point - collider.transform.position);

                    if (sqrDist < nearest)
                    {
                        tempTarget = collider;
                        nearest = sqrDist;
                    }
                }

                target = tempTarget.transform.position;
                RotateToMouseDirection(head, target);

                return;
            }
        }

        Vector2 mousePos = Input.mousePosition;
        Vector2 relativeCentre = Camera.main.WorldToScreenPoint(transform.position);

        Vector3 forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = forward.normalized;

        Vector3 right = Camera.main.transform.right;
        right.y = 0;
        right = right.normalized;

        Vector3 up = Vector3.up;

        float screenWidth = Screen.width;
        float screenHeight = Screen.height;

        mousePos = mousePos - relativeCentre;

        float ratio = screenWidth / screenHeight;

        Vector3 aimPoint = new Vector3(mousePos.x, 0, mousePos.y);

        Vector3 point = transform.position;
        point += forward * (aimPoint.z);
        point += right * (aimPoint.x);
        point += up * (aimPoint.y);

        head.transform.LookAt(point);

        target = point;
    }

    private void UseWeapons()
    {
        if (secondary != null)
            secondary.WeaponUpdate();

        if (Input.GetKey(InputManager.Primary))
        {
            primary.Fire(target);
        }
        else 
        {
            if (secondary != null)
            {
                if (secondary == flamethrower)
                {
                    if (Input.GetKeyDown(InputManager.Secondary))
                        (secondary as Flamethrower).Activate();
                    else if (Input.GetKeyUp(InputManager.Secondary))
                        (secondary as Flamethrower).Deactivate();
                }
                else
                {
                    if (Input.GetKey(InputManager.Secondary))
                        secondary.Fire(target);
                }
            }
        }
    }

    IEnumerator JumpRoutine()
    {
        StartCoroutine(PlatformerJuicer.instance.ScaleWarp(this));
        jumpState = JumpState.Jumping;
        jump = (2 * rt_jumpHeight) / rt_airTime;
        rb.velocity += Vector3.up * jump;

        float t = 0;

        while(t <= rt_airTime)
        {
            t += Time.deltaTime;
            yield return null;
        }

        jumpState = JumpState.Falling;
    }

    void RotateToMouseDirection(GameObject obj, Vector3 destination)
    {
        Vector3 direction = destination - obj.transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);

        obj.transform.localRotation = Quaternion.Lerp(obj.transform.rotation, rotation, 1);
    }

    public void RecieveDamage(Vector3 dir)
    {
        if (invulnerable == false)
        {
            if (PushDown.instance != null)
                PushDown.instance.HitScreen();
            else
                cam.isShaking = true;

            rt_health -= 1;

            if (rt_health <= 0)
                rt_health = 0;

            Knockback(dir);
            StartCoroutine(InvulnerableState());
        }
    }

    IEnumerator InvulnerableState()
    {
        Color targetCol = Color.red;
        Color defaultCol = renderer.sharedMaterial.color;

        invulnerable = true;

        float time = 0;
        float bTime = 0;
        bool colSwitch = true;

        while (time <= invulerabilityTimer)
        {
            time += Time.deltaTime;
            bTime += Time.deltaTime;

            if(colSwitch)
            {
                Color col = defaultCol;
                col = Color.Lerp(defaultCol, targetCol, bTime);
                renderer.sharedMaterial.color = col;
            }
            else
            {
                Color col = targetCol;
                col = Color.Lerp(targetCol, defaultCol, bTime);
                renderer.sharedMaterial.color = col;
            }

            if (bTime >= blinkTime)
            {
                bTime = 0;

                if (colSwitch)
                    colSwitch = false;
                else
                    colSwitch = true;
            }

            yield return null;
        }

        if (renderer.sharedMaterial.color != defaultCol)
        {
            Color ogCol = renderer.sharedMaterial.color;
            time = 0;
            while (time <= 1.1f)
            {
                time += Time.deltaTime * 4;

                Color col = ogCol;
                col = Color.Lerp(ogCol, Color.white, bTime);
                renderer.sharedMaterial.color = col;

                yield return null;
            }
        }

        renderer.sharedMaterial.color = Color.white;
        invulnerable = false;
    }

    void Prep()
    {
        rb = GetComponent<Rigidbody>();
        forward = Camera.main.transform.forward;
        right = Camera.main.transform.right;
        forward.y = 0;
        right.y = 0;
        forward = forward.normalized;
        right = right.normalized;

        jumpState = JumpState.Grounded;
        doubleJump = true;

        halfHeight = GetComponent<Collider>().bounds.size.y / 2;

        currentHeightScale = transform.localScale.y;

        renderer = GetComponent<MeshRenderer>();

        health = 3;
        invulnerable = false;
        alive = true;
    }

    public void GroundCheck()
    {
        if (jumpState != JumpState.Jumping)
        {
            Ray ray = new Ray(transform.position, -transform.up);
            if (Physics.Raycast(ray, halfHeight + PlatformerJuicer.instance.groundAllowance))
            {
                if (jumpState == JumpState.Falling)
                {
                    StartCoroutine(PlatformerJuicer.instance.ScaleWarp(this));
                }
                jumpState = JumpState.Grounded;
                doubleJump = true;
            }
            else
            {
                StartCoroutine(ChangeJumpState());
            }
        }
    }

    void Knockback(Vector3 enemyPos)
    {
        Vector3 knockbackDir = (-1 * (enemyPos - transform.position)) + (transform.up * upMultiplier);
        Vector3 newVeloKB = rb.velocity + (knockbackDir.normalized * kbForce);
        rb.AddForce(newVeloKB, ForceMode.Impulse);
    }

    IEnumerator ChangeJumpState()
    {
        yield return new WaitForSeconds(0.1f);
        jumpState = JumpState.Falling;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "EnemyAttack" && !invulnerable)
        {
            // Knockback force and direction is to be added here in below function or seperate function but here
            RecieveDamage(collision.transform.position);

            if (collision.transform.GetComponent<ObjectPooling>() != null)
                ObjectPooling.PoolDestroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Collectable")
        {
            if (WaveManager.instance.questActive)
            {
                WaveManager.instance.CollectUp();
                ObjectPooling.PoolDestroy(collision.gameObject);
            }
        }
    }

    public int ReturnHealth()
    {
        return rt_health;
    }

    public void Death()
    {
        rt_health = 0;
    }

    public void ReplenishHealth()
    {
        rt_health = health;
    }

    public void AddShield()
    {
        rt_health += 1;
    }

    public int MaxHealth()
    {
        return health;
    }
}