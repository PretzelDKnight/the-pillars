﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class PixelateEffect : MonoBehaviour
{
    public Material pixel;

    // Update is called once per frame
    void Update()
    {
        pixel.SetFloat("_ScreenHeight", Screen.height);
        pixel.SetFloat("_ScreenWidth", Screen.width);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, pixel);
    }
}
