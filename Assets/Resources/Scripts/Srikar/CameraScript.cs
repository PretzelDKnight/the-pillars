﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject player;

    [Range (-10, 10)] public float heightDiff = 8f;
    public float adjustedFactor = 3f;

    Camera cam;

    Vector3 ogPosition;
    Vector3 offset;
    public float magnitude;
    public bool isShaking = false;
    public float shakePower = 1.0f;
    public float shakeDuration = 1.0f;
    public float shakeSlowAmount = 1.0f;
    public float camSpeed = 2f;

    float initialDuration;

    void Start()
    {
        initialDuration = shakeDuration;
        ogPosition = transform.localPosition;
        offset = (transform.position - player.transform.position ).normalized;

        offset = offset.normalized;
        transform.position = player.transform.position + offset * magnitude;

        cam = Camera.main;
    }

    void Update()
    {
        LerpFunction();
        CheckObstruction();
        //CameraShake();
    }

    void LerpFunction()
    {
        float interpolation = camSpeed * Time.deltaTime;

        Vector3 position = transform.position;

        float heightAdjust = heightDiff / 2;
        heightAdjust = (-player.transform.position.y + heightAdjust) * adjustedFactor;

        position.y = Mathf.Lerp(transform.position.y, player.transform.position.y + offset.y * magnitude + heightAdjust, interpolation);
        position.x = Mathf.Lerp(transform.position.x, player.transform.position.x + offset.x * magnitude, interpolation);
        position.z = Mathf.Lerp(transform.position.z, player.transform.position.z + offset.z * magnitude, interpolation);

        transform.position = position;
        ogPosition = position;
    }

    void CheckObstruction()
    {
        Ray ray = new Ray(transform.position, (player.transform.position - transform.position).normalized);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Obstacle")
            {
                hit.collider.GetComponent<ObjectFadeScript>().obstructing = true;
            }
        }
    }

    public void CameraShake()
    {
        //if (isShaking == true)
        //{
            if (shakeDuration > 0)
            {
                transform.localPosition = ogPosition + Random.insideUnitSphere * shakePower;
                shakeDuration -= Time.deltaTime * shakeSlowAmount;
            }
            else
            {
                isShaking = false;
                shakeDuration = initialDuration;
                transform.localPosition = ogPosition;
            }
        //}

    }
}
