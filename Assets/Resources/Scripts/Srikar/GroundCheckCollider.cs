﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheckCollider : MonoBehaviour
{
    bool grounded;
    public Collider coll;

    private void Start()
    {
        grounded = true;
        coll = GetComponent<Collider>();
    }

    private void Update()
    {
        Debug.Log(grounded);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag != "Player" || other.gameObject.tag != "Enemy")
        {
            grounded = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Player" || other.gameObject.tag != "Enemy")
        {
            grounded = false;
        }
    }

    public bool Grounded
    {
        get { return grounded; }
    }
}
