﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
 #if UNITY_EDITOR
using UnityEditorInternal;
#endif
using UnityEngine;

 #if UNITY_EDITOR
[CustomEditor(typeof(AStar))]
public class AStarInspector : Editor
{
    SerializedProperty _nodeObject;
    SerializedProperty _gridValues;
    SerializedProperty _space;

    SerializedProperty _progress;
    int rt_progress;

    AStar script;

    private void OnEnable()
    {
        script = FindObjectOfType<AStar>();

        _nodeObject = serializedObject.FindProperty("nodeGameObject");
        _gridValues = serializedObject.FindProperty("gridValues");
        _space = serializedObject.FindProperty("space");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_nodeObject, new GUIContent("NodeGameObject"));
        EditorGUILayout.PropertyField(_gridValues, new GUIContent("Grid Values"));
        EditorGUILayout.PropertyField(_space, new GUIContent("NodePadding"));

        if (GUILayout.Button("Generate Grid"))
        {
            script.GenerateGrid();
        }

        if (GUILayout.Button("Set Children"))
        {
            script.GraphFormatting();
        }

        if (GUILayout.Button("Set Nodes"))
        {
            script.SetNodes();
        }

        if (GUILayout.Button("Save"))
        {
            script.SaveNodes();
        }

        if (GUILayout.Button("Load"))
        {
            script.LoadNodes();
        }

        //EditorGUI.ProgressBar(new Rect(20, 88, 300, 20), rt_progress / 100, "Progress");

        serializedObject.ApplyModifiedProperties();
    }
}

 #endif