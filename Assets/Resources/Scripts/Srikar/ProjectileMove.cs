﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMove : MonoBehaviour
{
    public float Speed;
    public float time;
    public int damage = 1;

    float lifetime;

    // Start is called before the first frame update
    void Start()
    {
        lifetime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        lifetime += Time.deltaTime;

        if (Speed != 0)
        {
            transform.position += transform.forward * (Speed * Time.deltaTime);
        }
        else
        {
            Debug.Log("No Speed");
        }

        if (lifetime >= time)
        {
            ObjectPooling.PoolDestroy(gameObject);
        }
    }

    public void AssignDamage(int dmg)
    {
        damage = dmg;
    }

    public void OnEnable()
    {
        lifetime = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<AdvEnemyAIBase>().TakeDamage(damage, Vector3.up);
            ObjectPooling.PoolDestroy(gameObject);
        }
        //ObjectPooling.PoolDestroy(gameObject);
    }

    //void OnCollisionEnter(Collision collision)
    //{
    //    Speed = 0;
    //    ContactPoint contact = collision.contacts[0];
    //    Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
    //    Vector3 position = contact.point;
    //
    //    ObjectPooling.PoolDestroy(gameObject);
    //}
}
