﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collect : Quest
{
    int num;

    public Collect(WaveManager mng)
    {
        manager = mng;
        num = Random.Range((int)manager.collectablesMinMax.x, (int)manager.collectablesMinMax.y + 1);
    }

    public override bool QuestCondition()
    {
        return num == 0;
    }

    public override void SpawnQuest()
    {
        NotificationManager.instance.BeginContentNotification(out questContent, "Collect the fallen Artifacts!", "Artifacts left : " + num);
        manager.SetEnemyRatio(manager.collectEnemyTypeRatio);
        manager.SetEnemySpawnTime(manager.collectEnemySpawnTime);
        manager.SpawnCollectables(num);
    }

    public override void Update()
    {
        if (questContent != null)
            questContent.ContentUpdate("Artifacts left : " + num);

        if (manager.questActive)
            num = manager.LeftToCollect;
    }
}