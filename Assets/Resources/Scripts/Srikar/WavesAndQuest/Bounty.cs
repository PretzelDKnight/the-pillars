﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounty : Quest
{
    public Bounty(WaveManager mng)
    {
        manager = mng;
    }

    public override bool QuestCondition()
    {
        return true;
    }

    public override void SpawnQuest()
    {
        NotificationManager.instance.BeginContentNotification(out questContent, "Kill the unique enemy!", "Target : ");
        //manager.SetEnemyRatio(manager.bountyRatio);
        //manager.SetEnemySpawnTime(manager.bountyTime);
    }

    public override void Update()
    {
        throw new System.NotImplementedException();
    }
}