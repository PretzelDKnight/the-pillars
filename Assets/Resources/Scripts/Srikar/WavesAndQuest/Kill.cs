﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kill : Quest
{
    int killCount;

    public Kill(WaveManager mng)
    {
        manager = mng;
    }

    public override bool QuestCondition()
    {
        return killCount >= manager.killAmount;
    }

    public override void SpawnQuest()
    {
        NotificationManager.instance.BeginContentNotification(out questContent, "Kill " + manager.killAmount + "  enemies!", "Kills : " + killCount);
        manager.SetEnemyRatio(manager.killEnemyTypeRatio);
        manager.SetEnemySpawnTime(manager.killEnemySpawnTime);
    }

    public override void Update()
    {
        if (questContent != null)
            questContent.ContentUpdate("Kills : " + killCount);

        if (manager.questActive)
            killCount = manager.KillCount;
    }
}
