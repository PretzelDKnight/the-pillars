﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class WaveManager : MonoBehaviour
{
    static public WaveManager instance = null;

    int wave = 1;
    int lastQuest = 0;

    Quest currentQuest;

    [SerializeField] public SpawnInRandomArea spawner;

    [SerializeField] public GameObject collectable;

    [SerializeField] public float timeTakenToPurge = 1.5f;

    [SerializeField] [Range(0, 1)] public float killEnemyTypeRatio;
    [SerializeField] [Range(0, 1)] public float collectEnemyTypeRatio;
    [SerializeField] [Range(0, 1)] public float surviveEnemyTypeRatio;

    [SerializeField] public float killEnemySpawnTime;
    [SerializeField] public float collectEnemySpawnTime;
    [SerializeField] public float surviveEnemySpawnTime;

    [SerializeField] [Range(5, 100)] public int killAmount;
    [SerializeField] public Vector2 collectablesMinMax;
    [SerializeField] [Range(5, 500)] public int surviveTimeLimit;
 
    int killCount;
    int leftToCollect;

    [NonSerialized] public bool questActive;

    [SerializeField] ProgressBar bloodBar;
    [SerializeField] float bloodGained;
    [SerializeField] float totalBloodAmount = 20f;

    float rt_bloodGained;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        if (PushDown.instance == null)
        {
            NewWave();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (currentQuest != null)
        {
            currentQuest.Update(); 
            
            if (questActive)
            {
                if (currentQuest.QuestCondition())
                {
                    NotificationManager.instance.AddToTitleStack("Wave Cleared");
                    bloodBar.Empty();

                    spawner.SetSpawn = false;

                    if (ScoreManager.instance != null)
                        ScoreManager.instance.UpdateWave();
                    wave += 1;

                    currentQuest.EndQuest();
                    questActive = false;

                    Vector3 originPos = GameObject.FindGameObjectWithTag("Player").transform.position;
                    spawner.PurgeEnemies(originPos, timeTakenToPurge);

                    NewWave();
                }
            }
            else
            {
                if (bloodBar.FillCheck())
                {
                    questActive = true;
                    currentQuest.SpawnQuest();
                }
            }
        }
    }

    void GenerateQuest()
    {
        currentQuest = null;

        int rand = lastQuest;

        while (true)
        {
            rand = UnityEngine.Random.Range(1, 4); // Not including Bounty atm since need Special enemy
            if (rand != lastQuest)
                break;
        }

        switch (rand)
        {
            case 1:
                currentQuest = new Collect(this);
                break;
            case 2:
                currentQuest = new Kill(this);
                break;
            case 3:
                currentQuest = new Survive(this);
                break;
            case 4:
                currentQuest = new Bounty(this);
                break;
            default:
                Debug.Log("Somethins Smellyyyyy.....");
                break;
        }

        lastQuest = rand;

        leftToCollect = 0;
        killCount = 0;
    }

    public void NewWave()
    {
        GenerateQuest();

        CalculateBlood();

        spawner.SetSpawn = false;

        StartCoroutine(WaveWaiter());
    }

    public void SetEnemyRatio(float ratio)
    {
        spawner.SetRatio = (int)(ratio * 100);
    }

    public void SetEnemySpawnTime(float time)
    {
        spawner.SetSpawnTime = time;
    }

    IEnumerator WaveWaiter()
    {
        yield return new WaitForSeconds(3f);

        if (NotificationManager.instance != null)
            NotificationManager.instance.AddToTitleStack("Wave " + wave);

        yield return new WaitForSeconds(2f);

        spawner.SetSpawn = true;
    }

    public void SpawnCollectables(int number)
    {
        spawner.SpawnItems(collectable, number);
        leftToCollect = number;
    }

    public void KillUp()
    {
        if (questActive)
            killCount += 1;
        else
            bloodBar.IncreaseFill(rt_bloodGained);

        if (UpgradeManager.instance != null)
            UpgradeManager.instance.Increment();

        if(ScoreManager.instance != null)
            ScoreManager.instance.UpdateScore(100);
    }

    public void CollectUp()
    {
        leftToCollect -= 1;
    }

    public int KillCount
    {
        get { return killCount; }
    }

    public int LeftToCollect
    {
        get { return leftToCollect; }
    }

    public void EndFunction()
    {
        StopAllCoroutines();
        spawner.SetSpawn = false;
        questActive = false;
        currentQuest = null;
        bloodBar.active = false;
    }

    void CalculateBlood()
    {
        rt_bloodGained =  bloodGained / totalBloodAmount;
    }
}