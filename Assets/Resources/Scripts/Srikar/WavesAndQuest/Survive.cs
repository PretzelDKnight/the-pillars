﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Survive : Quest
{
    float time;

    public Survive(WaveManager mng)
    {
        manager = mng;
        time = manager.surviveTimeLimit;
    }

    public override bool QuestCondition()
    {
        return time <= 0;
    }

    public override void SpawnQuest()
    {
        manager.SetEnemyRatio(manager.surviveEnemyTypeRatio);
        manager.SetEnemySpawnTime(manager.surviveEnemySpawnTime);
        NotificationManager.instance.BeginContentNotification(out questContent, "Survive till the time runs out!", "Time : " + (int)time);
    }

    public override void Update()
    {
        if (questContent != null)
            questContent.ContentUpdate("Time : " + (int)time);

        if (manager.questActive)
            time -= Time.deltaTime;
    }
}