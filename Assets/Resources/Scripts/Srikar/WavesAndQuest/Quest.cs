﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Quest
{
    protected WaveManager manager;

    protected Content questContent;

    abstract public void Update();
    abstract public void SpawnQuest();
    abstract public bool QuestCondition();

    public void EndQuest()
    {
        questContent.EndContent();
    }
}
