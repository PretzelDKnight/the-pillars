﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedWeapon : Weapon
{
    protected override void SpawnVFX(Vector3 target)
    {
        Vector3 Vo = CalculateVelocity(target, firePoint.transform.position, 1f);
        GameObject bulletTest = Instantiate(bullet, firePoint.transform.position, Quaternion.identity);
        bulletTest.GetComponent<Rigidbody>().velocity = Vo;
    }

    Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
    {
        //horizontal
        Vector3 distance = target - origin;
        Vector3 distanceXZ = distance;
        distanceXZ.y = 0f;

        //distances/vertdist
        float verticalDist = distance.y;
        float horizontalDist = distanceXZ.magnitude;

        //velocitys
        float Vxz = horizontalDist / time;
        float Vy = verticalDist / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 outcome = distanceXZ.normalized;
        outcome *= Vxz;
        outcome.y = Vy;

        return outcome;
    }
}
