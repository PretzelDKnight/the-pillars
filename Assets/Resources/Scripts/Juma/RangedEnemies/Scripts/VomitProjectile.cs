﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class VomitProjectile : MonoBehaviour
{
    //projectile/trajectory
    [SerializeField] Rigidbody bulletPrefabs;
    [SerializeField] GameObject targetPos;
    [SerializeField] Transform barrelPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 Vo = CalculateVelocity(targetPos.transform.position, barrelPos.position, 1f);
        transform.rotation = Quaternion.LookRotation(Vo);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Rigidbody bulletTest = Instantiate(bulletPrefabs, barrelPos.position, Quaternion.identity);
            bulletTest.velocity = Vo;
        }
    }

    void VomitProjectileToTarget()
    {
        Vector3 Vo = CalculateVelocity(targetPos.transform.position, barrelPos.position, 1f);
    }

    Vector3 CalculateVelocity (Vector3 target, Vector3 origin, float time)
    {
        //horizontal
        Vector3 distance = target - origin;
        Vector3 distanceXZ = distance;
        distanceXZ.y = 0f;

        //store values in a float which holds info about distances/vert dist
        float verticalDist = distance.y;
        float horizontalDist = distanceXZ.magnitude;

        //velocitys
        float Vxz = horizontalDist / time;
        float Vy = verticalDist / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        //normalize the direction then multiply the horizontal plane velo and set the y value to the y velo 
        Vector3 outcome = distanceXZ.normalized;
        outcome *= Vxz;
        outcome.y = Vy;

        //return the final velocity/result
        return outcome;
    }
}
