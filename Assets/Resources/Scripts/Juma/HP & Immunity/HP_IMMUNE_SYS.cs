﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class HP_IMMUNE_SYS : MonoBehaviour
{

    public int playerHP = 3;
    public bool invulnerable = false;
    public int invulTimer = 3;  //3 is the invulnerable timer when he is hit (so the player is invincible for 3 seconds), 
                                //Change the 3 to whatever number that would suit the games pace (DONT FORGET ABOUT INSPECTOR).
    float maxSpeed = 10f;

    float playerSpeed = 5f;
    float maxvelo = 10f;
    public Vector3 velocity;
    [SerializeField]bool moving;
    [SerializeField]float maxSpeedMultiplier;
    [SerializeField] float buttonTimer;
    public float smoothTime = 0;

    [SerializeField, Range(1, 3)] int movementScriptSelector;

    [SerializeField] Color defaultColor;
    Renderer rendForCol;
    Rigidbody pRB;

    public float kbForce = 3;
    public float upMultiplier = 2;

    // Start is called before the first frame update
    void Start()
    {
        rendForCol = GetComponent<Renderer>();
        pRB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //PLAYER HEALTH SYSTEM
        if (Input.GetKeyDown(KeyCode.Space))
        {
            DamagePlayer();
        }

        //MOVEMENT
        if (movementScriptSelector == 1)
        {
            MovementTest1();
        }
        if (movementScriptSelector == 2)
        {
            MovementTest2();
        }
        if (movementScriptSelector == 3)
        {
            Debug.Log("not using any movement script");
        }

        Debug.DrawRay(transform.position, pRB.velocity,Color.blue);
        //pRB.rotation = Quaternion.LookRotation(pRB.velocity * Time.deltaTime);

    }

    public void DamagePlayer()
    {
        if (invulnerable == false)
        {
            playerHP -= 1;
            rendForCol.material.color = Color.red;
            pRB.AddForce(-transform.forward * kbForce,ForceMode.Impulse);
            invulnerable = true;
            Invoke("InvulnerableReset", invulTimer);
        }
    }

    void InvulnerableReset ()
    {
        invulnerable = false;
        rendForCol.material.color = defaultColor;
    }

    //MOVEMENT TESTS
    // #1
    void MovementTest1()
    {
        float horizontal = 0;
        float vertical = 0;

        if (Input.GetKey(KeyCode.W))
        {
            vertical += 1f;
            moving = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            horizontal -= 1f;
            moving = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
            vertical -= 1f;
            moving = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            horizontal += 1f;
            moving = true;
        }

        if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D))
        {
            buttonTimer += 0.2f * Time.deltaTime;
            if (buttonTimer >= 0.05f)
            {
                moving = false;
                buttonTimer = 0.05f;
            }
        }
        else
        {
            buttonTimer = 0;
        }

        Debug.Log(moving);

        if (pRB.velocity.magnitude < 0.2f)
        {
            //moving = false;
        }

        if (moving == true)
        {
            smoothTime += maxSpeedMultiplier * Time.deltaTime;
        }
        if (moving == false)
        {
            smoothTime = 0f;
        }
        if (smoothTime >= 1f)
        {
            smoothTime = 1f;
        }
        velocity = new Vector3(horizontal, 0, vertical).normalized * (Mathf.Lerp(0,maxSpeed, smoothTime));

        pRB.velocity = transform.forward * velocity.z;
        pRB.velocity += transform.right * velocity.x;

        //pRB.position = Vector3.Lerp(pRB.position, , Time.deltaTime);

        //pRB.position = Vector3.SmoothDamp(pRB.position, transform.forward * velocity.z, ref velocity, maxSpeed);
    }

    // #2
    void MovementTest2()
    {
        if (Input.GetKey(KeyCode.W))
            pRB.velocity += transform.forward * playerSpeed;
        if (Input.GetKey(KeyCode.A))
            pRB.velocity += -transform.right * playerSpeed;
        if (Input.GetKey(KeyCode.S))
            pRB.velocity += -transform.forward * playerSpeed;
        if (Input.GetKey(KeyCode.D))
            pRB.velocity += transform.right * playerSpeed;
        if (pRB.velocity.magnitude >= maxvelo)
            pRB.velocity = pRB.velocity.normalized * maxvelo;
    }

    public void Knockback(Vector3 enemyPos)
    {
        Vector3 knockbackDir = (-1 * (enemyPos - transform.position)) + (transform.up * upMultiplier);
        Vector3 newVeloKB = pRB.velocity + (knockbackDir.normalized * kbForce);
        pRB.AddForce(newVeloKB, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        /*if (collision.transform.tag == "EnemyAttack") //&& !invulnerable)
        {
            // Knockback force and direction is to be added here in below function or seperate function but here
            DamageSource damageSource = collision.other.gameObject.GetComponent<DamageSource>();
            Knockback(damageSource.source.position);
            Debug.Log("detect col");
        }*/

        if (collision.gameObject.layer == 12)
        {
            DamageSource damageSource = collision.gameObject.GetComponent<DamageSource>();
            Knockback(damageSource.source.position);
        }

        Debug.Log(collision.collider.gameObject.layer);
    }
}
