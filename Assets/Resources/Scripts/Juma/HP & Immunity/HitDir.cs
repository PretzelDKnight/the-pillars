﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class HitDir : MonoBehaviour
{

    public GameObject player;
    public float speedtest = 0.5f;
    Rigidbody playerRB;
    HP_IMMUNE_SYS accessVel;
    Vector3 newVeloKB;
    Vector3 knockbackDir;
    public float kbForce;
    public float upMultiplier = 1;
    // Start is called before the first frame update
    void Start()
    {
        playerRB = player.GetComponent<Rigidbody>();
        accessVel = player.GetComponent<HP_IMMUNE_SYS>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(new Vector3(player.transform.position.x,transform.position.y, player.transform.position.z));
        transform.position += transform.forward * speedtest * Time.deltaTime;
        knockbackDir = (player.transform.position - gameObject.transform.position) + (transform.up * upMultiplier);
        Debug.DrawRay(transform.position, knockbackDir.normalized * 10f);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //playerRB.AddForce(knockbackFull.normalized * kbForce, ForceMode.Impulse);
            //newVeloKB = accessVel.velocity + (knockbackDir.normalized * kbForce);
            //playerRB.AddForce(newVeloKB, ForceMode.Impulse);
        }
    }
}
