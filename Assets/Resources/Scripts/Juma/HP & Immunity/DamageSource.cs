﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSource : MonoBehaviour
{
    public Transform source;

    public Transform getSource()
    {

        return source;
    }

    public void setSource(Transform src)
    {

        this.source = src;
    }

}
