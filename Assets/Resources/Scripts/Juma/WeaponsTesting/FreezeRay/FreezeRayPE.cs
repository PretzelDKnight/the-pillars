﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FreezeRayPE : MonoBehaviour
{
    [SerializeField] float freezeTimer;
    [SerializeField] int maxClipSize = 5;
    public float freezeRayReloadTime;
    public float burstTimer;
    [SerializeField] int currentClip;
    [SerializeField] bool isReloading;
    [SerializeField] bool isFiring;
    [SerializeField] ParticleSystem freezeRayParticles;

    // Start is called before the first frame update
    void Start()
    {
        currentClip = maxClipSize;
        freezeTimer = 3;
        isReloading = false;
        freezeRayParticles.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (isFiring == false && isReloading == false)
            {
                StartCoroutine(FireFreezeRay2());
            }
        }
        if (currentClip == 0)
        {
            ReloadMethod();
        }
    }

    IEnumerator FireFreezeRay2()
    {
        if (isReloading == false)
        {
            isFiring = true;
            currentClip--;
            freezeRayParticles.Play();
            yield return new WaitForSeconds(burstTimer);
            freezeRayParticles.Stop();
            isFiring = false;
        }
    }

    void OnParticleCollision(GameObject other)
    {
        
        if (other.gameObject.tag == "Enemy")
        {
            StartCoroutine(FreezeEnemies());
        }

        IEnumerator FreezeEnemies()
        {
            other.GetComponent<HealthTester>().isMovingTests = false;
            other.GetComponent<MeshRenderer>().material.color = Color.cyan;
            yield return new WaitForSeconds(freezeTimer);
            other.GetComponent<HealthTester>().isMovingTests = true;
            //other.GetComponent<HealthTester>().FreezeKill();
        }
    }

    IEnumerator FireFreezeRay()
    {
        if (isReloading == false)
        {
            isFiring = true;
            currentClip--;
            freezeRayParticles.Play();
            yield return new WaitForSeconds(0.1f);
            freezeRayParticles.Stop();
            isFiring = false;
        }
    }

    IEnumerator ReloadGunCor()
    {
        isReloading = true;
        yield return new WaitForSeconds(freezeRayReloadTime);
        currentClip = maxClipSize;
        isReloading = false;
    }

    void ReloadMethod()
    {
        if (isReloading == false)
        {
            StartCoroutine(ReloadGunCor());
        }
    }


}
