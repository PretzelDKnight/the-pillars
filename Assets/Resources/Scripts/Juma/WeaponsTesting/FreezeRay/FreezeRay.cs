﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FreezeRay : Weapon
{
    [SerializeField] float freezeTimer;
    [SerializeField] int maxClipSize;
    int currentClip;
    bool isReloading;
    bool isFiring;
    [SerializeField] ParticleSystem freezeRayParticles;

    // Start is called before the first frame update
    void Start()
    {
        currentClip = maxClipSize;
        isReloading = false;
        freezeRayParticles.Stop();
        ResetValues();
    }

    // Update is called once per frame
    public override void WeaponUpdate()
    {
        if (currentClip == 0)
        {
            ReloadMethod();
        }
    }

    public override void Fire(Vector3 target)
    {
        if (isFiring == false && isReloading == false)
        {
            StartCoroutine(FireFreezeRayBurst());
        }
    }

    public override string ReturnType()
    {
        return "Freezeray";
    }

    IEnumerator FireFreezeRayBurst()
    {
        if (isReloading == false)
        {
            isFiring = true;
            currentClip--;
            freezeRayParticles.Play();
            FireSound();
            yield return new WaitForSeconds(fireRate);
            freezeRayParticles.Stop();
            isFiring = false;
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag == "Enemy"){
            other.GetComponent<AdvEnemyAIBase>().Freeze(freezeTimer);
        }
    }

    IEnumerator ReloadGunCor()
    {
        isReloading = true;
        yield return new WaitForSeconds(rechargeTime);
        currentClip = maxClipSize;
        isReloading = false;
    }

    void ReloadMethod()
    {
        if (isReloading == false)
        {
            StartCoroutine(ReloadGunCor());
        }
    }

}
