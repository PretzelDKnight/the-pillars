﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FreezeRay_Test : MonoBehaviour
{
    [SerializeField] float freezeTimer;
    [SerializeField] float rechargeTimer;
    MeshCollider coneCol;
    bool isRecharging;
    bool isFiring;
    [SerializeField] ParticleSystem freezeRayParticles;

    // Start is called before the first frame update
    void Start()
    {
        coneCol = GetComponent<MeshCollider>();
        coneCol.enabled = false;
        freezeTimer = 3;
        rechargeTimer = 1;
        isRecharging = false;
        freezeRayParticles.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            StartCoroutine(FireFreezeRay());
        }
    }

    IEnumerator FireFreezeRay()
    {
        if (isRecharging == false)
        {
            coneCol.enabled = true;
            freezeRayParticles.Play();
            yield return new WaitForSeconds(0.1f);
            freezeRayParticles.Stop();
            coneCol.enabled = false;
            RechargeFreezeRay();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            //other.GetComponent<HealthTester>().enabled = false;
            //other.GetComponent<HealthTester>().isMovingTests = false;
            StartCoroutine(FreezeEnemies());
        }

        IEnumerator FreezeEnemies()
        {
            other.GetComponent<HealthTester>().isMovingTests = false;
            other.GetComponent<MeshRenderer>().material.color = Color.cyan;
            yield return new WaitForSeconds(freezeTimer);
            other.GetComponent<HealthTester>().isMovingTests = true;
            //other.GetComponent<HealthTester>().FreezeKill();
        }
    }

    void RechargeFreezeRay()
    {
        if (isRecharging == false)
        {
            StartCoroutine(RechargeTime());
        }
    }

    IEnumerator RechargeTime()
    {
        isRecharging = true;
        yield return new WaitForSeconds(rechargeTimer);
        isRecharging = false;
    }

}
