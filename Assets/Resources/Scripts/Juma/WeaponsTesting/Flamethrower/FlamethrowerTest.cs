﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamethrowerTest : MonoBehaviour
{
    ParticleSystem flamethrowerParticle;

    public int numberOfTicks = 3;
    public float tickDamageRate = 1f;

    public float heatMeter;
    public bool overheated;
    [SerializeField] int cooldownMultiplier = 10;
    [SerializeField] int heatingMultiplier = 10;

    Dictionary<HealthTester, Tick> tickEnemies;

    // Start is called before the first frame update
    void Start()
    {
        heatMeter = 0;
        overheated = false;
        flamethrowerParticle = GetComponent<ParticleSystem>();

        tickEnemies = new Dictionary<HealthTester, Tick>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartFlamethrower();
        }

        if(Input.GetMouseButton(0))
        {
            if (overheated == false)
            {
                HeatManager();
            }
            else if (overheated == true)
            {
                EndFlamethrower();
                CoolDown();
            }
        }
        else
        {
            CoolDown();
        }

        if (Input.GetMouseButtonUp(0))
        {
            EndFlamethrower();
        }

        UpdateDamageTick();
    }

    void StartFlamethrower()
    {
        if (overheated == false)
        {
            flamethrowerParticle.Play();
        }
    }

    void EndFlamethrower()
    {
        flamethrowerParticle.Stop();
    }

    void HeatManager()
    {
        if(heatMeter >= 0)
        {
            heatMeter = heatMeter + heatingMultiplier * Time.deltaTime;
            if (heatMeter >= 100)
            {
                heatMeter = 100;
                overheated = true;
                return;
            }
        }
    }

    void CoolDown()
    {
        heatMeter = heatMeter - cooldownMultiplier * Time.deltaTime;
        if (heatMeter <= 0)
        {
            heatMeter = 0;
            overheated = false;
            return;
        }
    }

    public void AddEnemy(HealthTester test)
    {
        // When readding enemies to the list, refreshes dmg tick duration
        if (tickEnemies.ContainsKey(test))
        {
            if (tickEnemies[test].times < numberOfTicks)
            {
                tickEnemies[test].times += numberOfTicks;
                Debug.Log("Tick damage reset");
            }
        }
        else
            tickEnemies.Add(test, new Tick(numberOfTicks, tickDamageRate));
    }

    void UpdateDamageTick()
    {
        foreach (var item in tickEnemies)
        {
            if (tickEnemies[item.Key].times > 0)
            {
                tickEnemies[item.Key].tickRate -= Time.deltaTime;

                if (tickEnemies[item.Key].tickRate <= 0)
                {
                    tickEnemies[item.Key].times -= 1;
                    tickEnemies[item.Key].tickRate = tickDamageRate;
                    item.Key.Damage(1);
                    Debug.Log("Tick damage dealt");
                }
            }
        }

        Debug.Log(tickEnemies.Count);
    }
}
