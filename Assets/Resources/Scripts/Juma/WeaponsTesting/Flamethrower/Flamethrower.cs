﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tick
{
    public Tick(int num, float val)
    {
        times = num;
        tickRate = val;
    }

    public int times;
    public float tickRate;
}

public class Flamethrower : Weapon
{
    ParticleSystem flamethrowerParticle;

    public int numberOfTicks = 3;

    public float heatMeter;
    bool overheated;
    [SerializeField] int heatFactor = 10;

    Dictionary<AdvEnemyAIBase, Tick> tickEnemies;

    bool fire;

    // Start is called before the first frame update
    void Start()
    {
        heatMeter = 0;
        overheated = false;
        flamethrowerParticle = GetComponent<ParticleSystem>();

        tickEnemies = new Dictionary<AdvEnemyAIBase, Tick>();
        fire = false;

        ResetValues();
    }

    // Update is called once per frame
    public override void WeaponUpdate()
    {
        if(fire)
            HeatUp();
        else
            CoolDown();

        if (overheated)
        {
            Deactivate();
        }

        UpdateDamageTick();
    }

    public override void Fire(Vector3 target)
    {
        //base.Fire(target);
        //if (!overheated)
        //{
        //    fire = true;
        //    flamethrowerParticle.Play();
        //}
        //else
        //{
        //    fire = false;
        //    flamethrowerParticle.Stop();
        //}
    }

    public void Activate()
    {
        if (!overheated)
        {
            fire = true;
            flamethrowerParticle.Play();
            FireSound();
        }
    }

    public void Deactivate()
    {
        fire = false;
        flamethrowerParticle.Stop();
    }

    public override string ReturnType()
    {
        return "Flamethrower";
    }

    void HeatUp()
    {
        if (heatMeter < 100)
            heatMeter += heatFactor * Time.deltaTime;

        if (heatMeter >= 100)
        {
            heatMeter = 100;
            overheated = true;
            return;
        }
    }

    void CoolDown()
    {
        if (heatMeter > 0)
            heatMeter -= Time.deltaTime * 10 / rechargeTime;
        
        if (heatMeter <= 0)
        {
            heatMeter = 0;
            overheated = false;
            return;
        }
    }

    void UpdateDamageTick()
    {
        foreach (var item in tickEnemies)
        {
            if (item.Key != null)
            {
                if (tickEnemies[item.Key].times > 0)
                {
                    tickEnemies[item.Key].tickRate -= Time.deltaTime;

                    if (tickEnemies[item.Key].tickRate <= 0)
                    {
                        tickEnemies[item.Key].times -= 1;
                        tickEnemies[item.Key].tickRate = rt_fireRate;
                        item.Key.TakeDamage(damage, Vector3.zero);
                    }
                }
            }
        }
    }

    public void AddEnemy(AdvEnemyAIBase test)
    {
        // When readding enemies to the list, refreshes dmg tick duration
        if (tickEnemies.ContainsKey(test))
        {
            if (tickEnemies[test].times < numberOfTicks)
            {
                tickEnemies[test].times += numberOfTicks;
            }
        }
        else
            tickEnemies.Add(test, new Tick(numberOfTicks, rt_fireRate));
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            AddEnemy(other.GetComponent<AdvEnemyAIBase>());
        }
    }
}