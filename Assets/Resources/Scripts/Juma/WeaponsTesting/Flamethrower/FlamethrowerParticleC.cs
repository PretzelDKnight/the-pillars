﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamethrowerParticleC : MonoBehaviour
{
    [SerializeField] Flamethrower flameThrower;
    ParticleSystem flamethrowerParticles;

    // Start is called before the first frame update
    void Start()
    {
        flamethrowerParticles = GetComponent<ParticleSystem>();
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            flameThrower.AddEnemy(other.GetComponent<AdvEnemyAIBase>());
        }
    }

}
