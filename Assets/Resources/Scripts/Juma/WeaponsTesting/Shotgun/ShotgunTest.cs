﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEngine;

public class ShotgunTest : MonoBehaviour
{
    public GameObject sgBullets;
    public Transform BarrelEnd;
    public float spreadValue = 0.5f;
    public float bulletForce;

    public int sg_Ammo = 3;
    public int currentAmmo;
    public bool isReloading = false;

    // Start is called before the first frame update
    void Start()
    {
        currentAmmo = sg_Ammo;
    }

    // Update is called once per frame
    void Update()
    {
        DebugSpread();
        if (Input.GetMouseButtonDown(0))
        {
            FireShotgun();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(ReloadShotgun());
        }

    }

    void FireShotgun()
    {
        if (currentAmmo > 0)
        {
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    Vector3 barrelDir = new Vector3((transform.localPosition.x + (x * spreadValue + Random.Range(-0.3f, 0.3f))), (transform.localPosition.y + (y * spreadValue + Random.Range(-0.3f, 0.3f))), (transform.localPosition.z + 1));
                    Debug.DrawRay(BarrelEnd.position, barrelDir);
                    GameObject bullet = Instantiate(sgBullets, BarrelEnd.position, Quaternion.LookRotation(barrelDir));
                    bullet.GetComponent<Rigidbody>().AddForce(barrelDir * bulletForce, ForceMode.Impulse);
                    Destroy(bullet, 1);
                }
            }
            currentAmmo--;
        }

        if (currentAmmo <= 0)
        {
            Debug.Log("shotgun is empty");
            //put reload func here also ?
        }
    }

    IEnumerator ReloadShotgun()
    {
        Debug.Log("reloading 3 secs");
        yield return new WaitForSeconds(3);
        currentAmmo = sg_Ammo;
    }

    void DebugSpread()
    {
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                Vector3 barrelDir = new Vector3((transform.localPosition.x + (x * spreadValue)), (transform.localPosition.y + (y * spreadValue)), (transform.localPosition.z + 1));
                Debug.DrawRay(BarrelEnd.position, barrelDir);
            }
        }
    }
}
