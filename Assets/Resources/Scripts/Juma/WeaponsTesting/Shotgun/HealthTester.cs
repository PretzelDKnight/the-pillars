﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthTester : MonoBehaviour
{

    public int enemyHPTest;
    public int currentEHPT;
    public int flamethrowerDamageTick = 1;
    [SerializeField] int damageTick = 0;
    public bool isMovingTests;


    // Start is called before the first frame update
    void Start()
    {
        enemyHPTest = 3;
        currentEHPT = enemyHPTest;
        isMovingTests = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentEHPT <= 0)
        {
            print("dead enemy");
            isMovingTests = false;
        }

        if (isMovingTests == true)
        {
            transform.Rotate(new Vector3(0, 3, 0));
        }
        else
        {

        }

    }

    public void ShotgunDMG(int dmg)
    {
        currentEHPT -= dmg;
    }

    public void FlamethrowerDMG(int dmgMultiplier)
    {
        damageTick = damageTick + dmgMultiplier;
        if (damageTick >= 10)
        {
            currentEHPT -= flamethrowerDamageTick;
            damageTick = 0;
        }
    }

    public void Damage(float amount)
    {
        currentEHPT -= (int)amount;
    }

    public void RocketDMG(int dmg)
    {
        currentEHPT -= dmg;
    }

    public void FreezeKill()
    {
        currentEHPT -= currentEHPT;
    }
}
