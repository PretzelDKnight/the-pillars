﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    MeshCollider coneCol;
    MeshRenderer mesh;

    [SerializeField] int maxClipSize = 2;

    [SerializeField] float upwardsForce = 5;
    [SerializeField] float horizontalForce = 10;
    [SerializeField] float angleBetweenPellets = 5;
    [SerializeField] int maxNumOfEnemiesHit = 10;
    [SerializeField] int pierce = 0;

    int currentClip;
    bool busy;
    bool fire;

    List<GameObject> enemyObjects;

    // Start is called before the first frame update
    void Start()
    {
        coneCol = GetComponent<MeshCollider>();
        mesh = GetComponent<MeshRenderer>();
        mesh.enabled = false;
        coneCol.enabled = false;
        currentClip = maxClipSize;
        busy = false;
        fire = false;

        enemyObjects = new List<GameObject>();

        ResetValues();
    }

    // Update is called once per frame
    public override void WeaponUpdate()
    {
        if (currentClip <= 0)
        {
            StartCoroutine(ReloadGunCor());
        }

    }

    public override void Fire(Vector3 target)
    {
        StartCoroutine(ShotgunConeCoroutine());
    }

    public override string ReturnType()
    {
        return "Shotgun";
    }

    IEnumerator ShotgunConeCoroutine()
    {
        if (!busy && !fire)
        {
            if (currentClip > 0)
            {
                fire = true;
                FireSound();
                busy = true;
                enemyObjects.Clear();
                mesh.enabled = true;
                coneCol.enabled = true;
                currentClip -= 1;

                yield return new WaitForSeconds(0.09f);

                coneCol.enabled = false;
                mesh.enabled = false;

                EnemyHit();

                yield return new WaitForSeconds(rt_fireRate);

                fire = false;
                busy = false;
            }

        }
    }

    IEnumerator ReloadGunCor()
    {
        if (!busy)
        {
            busy = true;
            yield return new WaitForSeconds(rt_rechargeTime);
            currentClip = maxClipSize;
            busy = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            if (enemyObjects.Count < maxNumOfEnemiesHit)
                enemyObjects.Add(other.gameObject);
        }
    }

    void EnemyHit()
    {
        Vector3 current;
        Vector3 otherVector;

        foreach(var obj in enemyObjects)
        {
            current = (obj.transform.position - firePoint.transform.position);

            bool hit = true;
            int pierceCounts = pierce;

            foreach(var otherObj in enemyObjects)
            {
                if (obj != otherObj)
                {
                    otherVector = (otherObj.transform.position - firePoint.transform.position);

                    // If angle between both enemies is lesser than minimum allowed; i.e If they are too close to each other
                    if (Vector3.Angle(current, otherVector) < angleBetweenPellets)
                    {
                        // if the current enemy is behind the other enemy
                        if (current.magnitude > otherVector.magnitude)
                        {
                            // Checks the amount of pierce available
                            if (pierceCounts > 0)
                                pierceCounts -= 1;
                            else
                                hit = false;
                        }
                    }
                }
            }

            if (hit)
            {
                obj.GetComponent<AdvEnemyAIBase>().TakeDamage(damage, (-transform.forward * horizontalForce + transform.up * upwardsForce));
            }
        }
    }
}