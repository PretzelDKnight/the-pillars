﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunC : MonoBehaviour
{
    MeshCollider coneCol;
    RaycastHit hit;

    [SerializeField] int maxClipSize = 2;
    [SerializeField] int totalAmmo = 10;
    [SerializeField] int currentClip;
    [SerializeField] bool isReloading;
    [SerializeField] bool isFiring;
    [SerializeField] int shotgunDamage;
    [SerializeField] ParticleSystem shotgunParticles;
    [SerializeField] ParticleSystem shotgunMuzzleFlashParticles;
    private List<Collider> colliders = new List<Collider>();

    // Start is called before the first frame update
    void Start()
    {
        coneCol = GetComponent<MeshCollider>();
        coneCol.enabled = false;
        currentClip = maxClipSize;
        isFiring = false;
        isReloading = false;
        shotgunParticles.Stop();
        shotgunMuzzleFlashParticles.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            StartCoroutine(ShotgunConeCoroutine());
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadMethod();
        }

        GetColliders();
    }

    IEnumerator ShotgunConeCoroutine()
    {
        //Shoot if no ammo left reload
        if (currentClip <= 0)
        {
            //ReloadMethod();
        }
        else
        {
            if (isFiring == false && isReloading == false)
            {
                isFiring = true;
                coneCol.enabled = true;
                currentClip--;
                shotgunParticles.Play();
                shotgunMuzzleFlashParticles.Play();
                yield return new WaitForSeconds(0.1f);
                shotgunMuzzleFlashParticles.Stop();
                shotgunParticles.Stop();
                coneCol.enabled = false;
                isFiring = false;
                //
                                                                                            //AUTO RELOAD
                                                                                            //if (currentClip <= 0)
                                                                                            //{
                                                                                            //    ReloadMethod();
                                                                                            //}
            }
        }
    }

    IEnumerator ReloadGunCor()
    {
        if (totalAmmo > 0)
        {
            isReloading = true;
            yield return new WaitForSeconds(2f);
            int currentClipFilled;
            int ammoToFill;
            ammoToFill = maxClipSize - currentClip;
            totalAmmo -= ammoToFill;
            currentClipFilled = ammoToFill + currentClip;
            currentClip = currentClipFilled;
            isReloading = false;
        }
    }

    void ReloadMethod()
    {
        if (isReloading == false && currentClip != 2)
        {
            StartCoroutine(ReloadGunCor());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        /*
        if (other.gameObject.tag == "Enemy")
        {
            other.GetComponent<Rigidbody>().AddForce(-transform.forward * 10 + transform.up * 5, ForceMode.Impulse);
            other.GetComponent<HealthTester>().ShotgunDMG(shotgunDamage);
            Debug.Log("is in my trigger cone");
        }
        */

        if (!colliders.Contains(other) && other.gameObject.tag == "Enemy")
        {
            colliders.Add(other);
        }
    }

    public List<Collider> GetColliders()
    {
        return colliders;
    }

    public void RefreshList()
    {
        colliders.Clear();
    }

    void CalculateNumOfEnemiesInCollider()
    {
        GetColliders();
    }
}
/*
 * debug / old things
 *             if (Physics.Raycast(transform.position - transform.forward * -1, transform.TransformDirection(-Vector3.forward), out hit, 2))
            {
                Debug.DrawRay(transform.position - transform.forward * -1, transform.TransformDirection(-Vector3.forward) * hit.distance, Color.yellow);
                Debug.Log("Did Hit");
            }
            else
            {
                Debug.DrawRay(transform.position - transform.forward * -1, transform.TransformDirection(-Vector3.forward) * 100, Color.white);
                Debug.Log("Did not Hit");
            }

    void ShotgunCone()
    {
        if (currentClip <= 0)
        {
            ReloadMethod();
        }
        else
        {
            coneCol.enabled = true;

            currentClip--;

            coneCol.enabled = false;
        }
    }
            /*
        if (Physics.Raycast(transform.position - transform.forward * -1.5f, transform.TransformDirection(-Vector3.forward), out hit, 3.5f))
        {
            Debug.DrawRay(transform.position - transform.forward * -1.5f, transform.TransformDirection(-Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("Did Hit");
        }
        else
        {
            Debug.DrawRay(transform.position - transform.forward * -1.5f, transform.TransformDirection(-Vector3.forward) * 3.5f, Color.white);
            Debug.Log("Did not Hit");
        }
        */

