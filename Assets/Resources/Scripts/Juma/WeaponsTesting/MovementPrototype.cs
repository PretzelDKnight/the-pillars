﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPrototype : MonoBehaviour
{
    [SerializeField] bool playerWorldSpaceMovement = true;
    int playerSpeed = 5;
    int maxvelo = 10;
    Rigidbody pRB;

    Camera mainCam;

    // Start is called before the first frame update
    void Start()
    {
        pRB = GetComponent<Rigidbody>();
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerWorldSpaceMovement == true)
        {
            WorldSpaceMOVEMENT();
        }
        else
        {
            LocalSpaceMOVEMENT();
        }
        RayCastObjToMouse();
    }

    void RayCastObjToMouse()
    {
        Ray camRay = mainCam.ScreenPointToRay(Input.mousePosition);

        Plane floor = new Plane(Vector3.up, Vector3.zero);
        float rayLength; 
        if(floor.Raycast(camRay, out rayLength))
        {
            Vector3 pointToAim = camRay.GetPoint(rayLength);
            Debug.DrawLine(camRay.origin, pointToAim, Color.white);
            transform.LookAt(new Vector3(pointToAim.x, transform.position.y, pointToAim.z));
        }
    }    

    void LocalSpaceMOVEMENT()
    {
        if (Input.GetKey(KeyCode.W))
            pRB.velocity += transform.forward * playerSpeed;
        if (Input.GetKey(KeyCode.A))
            pRB.velocity += -transform.right * playerSpeed;
        if (Input.GetKey(KeyCode.S))
            pRB.velocity += -transform.forward * playerSpeed;
        if (Input.GetKey(KeyCode.D))
            pRB.velocity += transform.right * playerSpeed;
        if (pRB.velocity.magnitude >= maxvelo)
            pRB.velocity = pRB.velocity.normalized * maxvelo;
        Debug.DrawRay(transform.position, pRB.velocity, Color.blue);
    }

    void WorldSpaceMOVEMENT()
    {
        if (Input.GetKey(KeyCode.W))
            pRB.velocity += Vector3.forward * playerSpeed;
        if (Input.GetKey(KeyCode.A))
            pRB.velocity += -Vector3.right * playerSpeed;
        if (Input.GetKey(KeyCode.S))
            pRB.velocity += -Vector3.forward * playerSpeed;
        if (Input.GetKey(KeyCode.D))
            pRB.velocity += Vector3.right * playerSpeed;
        if (pRB.velocity.magnitude >= maxvelo)
            pRB.velocity = pRB.velocity.normalized * maxvelo;
        Debug.DrawRay(transform.position, pRB.velocity, Color.blue);
    }
}
