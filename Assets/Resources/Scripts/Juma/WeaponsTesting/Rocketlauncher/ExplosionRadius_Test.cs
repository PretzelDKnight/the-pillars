﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionRadius_Test : MonoBehaviour
{
    int rocketDamage;

    // Start is called before the first frame update
    void Start()
    {
        rocketDamage = 10;
        StartCoroutine(DestroyOnNextFrame());
    }

    void OnTriggerEnter(Collider other)
    {
        print("explosion happened");
        if (other.gameObject.tag == "Enemy")
        { 
            print("Enemy is in damage radius & should take dmg");
            Vector3 dir = other.transform.position - transform.position;
            other.GetComponent<Rigidbody>().AddForce((dir.normalized * 15) + (transform.up * 10), ForceMode.Impulse);
            other.GetComponent<HealthTester>().RocketDMG(rocketDamage);
        }
        // IF PLAYER IS IN RANGE OF EXPLOSION PLAYER IS ALSO KNOCKED BACK
        if (other.gameObject.tag == "Player")
        {
            print("Player is in damage radius & should be knocked back");
            Vector3 dir = other.transform.position - transform.position;
            other.GetComponent<Rigidbody>().AddForce((dir.normalized * 15) + (transform.up * 10), ForceMode.Impulse);
        }
    }

    IEnumerator DestroyOnNextFrame()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        Destroy(this.gameObject);
    }
}

