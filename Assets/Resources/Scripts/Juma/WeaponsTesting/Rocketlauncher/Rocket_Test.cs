﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Rocket_Test : MonoBehaviour
{
    [SerializeField] ExplosionRadius_Test explosionRadius;
    [SerializeField] GameObject explosionParticles;

    RocketLauncher launcher;

    [NonSerialized] public float speed;
    [NonSerialized] public float damage;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
        Destroy(this.gameObject, 5);
    }

    void OnCollisionEnter(Collision collision)
    {
            //^^^ check for all collisions that we need ^^^
            Vector3 collisionPos = collision.GetContact(0).point;
            Instantiate(explosionRadius, collisionPos, Quaternion.identity);
            Instantiate(explosionParticles, collisionPos, Quaternion.identity);
            Destroy(gameObject);
    }
}
