﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RL_Test : MonoBehaviour
{
    [SerializeField] GameObject rocket;
    [SerializeField] ParticleSystem muzzleExplosion;
    public float rocketSpeed;
    public int maxClipSize = 1;
    public float rocketLauncherReloadTime;
    int currentClip;
    bool isReloading;
    bool isFiring;
    // Start is called before the first frame update
    void Start()
    {
        rocketSpeed = 7f;
        muzzleExplosion.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            FireToMouse();
            if (isFiring == false && isReloading == false)
            {
                FireRocket();
                StartCoroutine(RocketLauncherParticleTest());
            }
        }
        if (currentClip == 0)
        {
            ReloadMethod();
        }
    }

    void FireRocket()
    {
        isFiring = true;
        currentClip--;
        Instantiate(rocket, transform.position, transform.rotation);
        isFiring = false;
    }

    IEnumerator RocketLauncherParticleTest()
    {
        muzzleExplosion.Play();
        yield return new WaitForSeconds(0.1f);
        muzzleExplosion.Stop();

    }

    private void FireToMouse()
    {
        RaycastHit hit;
        Ray rayMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(rayMouse.origin, rayMouse.direction, out hit, Mathf.Infinity))
        {
            transform.LookAt(hit.point);
        }
    }


    IEnumerator ReloadGunCor()
    {
        isReloading = true;
        yield return new WaitForSeconds(rocketLauncherReloadTime);
        currentClip = maxClipSize;
        isReloading = false;
    }

    void ReloadMethod()
    {
        if (isReloading == false)
        {
            StartCoroutine(ReloadGunCor());
        }
    }

}
