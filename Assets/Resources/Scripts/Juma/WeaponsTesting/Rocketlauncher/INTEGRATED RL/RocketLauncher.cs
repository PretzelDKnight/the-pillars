﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : Weapon
{
    public float rocketSpeed = 17f;
    public float force;

    public int maxClipSize = 1;
    int currentClip;
    bool isReloading;
    bool isFiring;

    // Start is called before the first frame update
    void Start()
    {
        currentClip = maxClipSize;
        isFiring = false;
        isReloading = false;
        ResetValues();
    }

    // Update is called once per frame
    public override void WeaponUpdate()
    {
        if (currentClip == 0)
        {
            ReloadMethod();
        }
    }

    public override void Fire(Vector3 target)
    {
        if (isFiring == false && isReloading == false)
        {
            StartCoroutine(FireRocket());
        }
    }

    public override string ReturnType()
    {
        return "RocketLauncher";
    }

    IEnumerator FireRocket()
    {
        isFiring = true;
        currentClip--;
        Instantiate(bullet, transform.position, transform.rotation);
        yield return new WaitForSeconds(fireRate);
        isFiring = false;
    }

    IEnumerator ReloadGunCor()
    {
        isReloading = true;
        yield return new WaitForSeconds(rechargeTime);
        currentClip = maxClipSize;
        isReloading = false;
    }

    void ReloadMethod()
    {
        if (isReloading == false)
        {
            StartCoroutine(ReloadGunCor());
        }
    }

    public int Damage()
    {
        return damage;
    }
}
