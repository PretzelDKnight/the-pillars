﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    [SerializeField] GameObject explosionRadius;
    [SerializeField] GameObject explosionParticles;
    RocketLauncher rocketSpeedAccess;
    float speed;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rocketSpeedAccess = FindObjectOfType<RocketLauncher>();
        speed = rocketSpeedAccess.rocketSpeed;
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
        Destroy(gameObject, 5);
    }

    void OnCollisionEnter(Collision collision)
    {
        Vector3 collisionPos = collision.GetContact(0).point;
        Instantiate(explosionRadius, collisionPos, Quaternion.identity);
        Instantiate(explosionParticles, collisionPos, Quaternion.identity);
        Destroy(gameObject);
    }
}
