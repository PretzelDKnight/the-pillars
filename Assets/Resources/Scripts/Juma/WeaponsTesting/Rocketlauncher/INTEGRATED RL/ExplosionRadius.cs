﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionRadius : MonoBehaviour
{
    int rocketDamage;
    float force;

    RocketLauncher rocketDamageAccess;
    // Start is called before the first frame update
    void Start()
    {
        rocketDamageAccess = FindObjectOfType<RocketLauncher>();
        rocketDamage = rocketDamageAccess.Damage();
        force = rocketDamageAccess.force;

        StartCoroutine(DestroyOnNextFrame());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        { 
            other.GetComponent<AdvEnemyAIBase>().TakeDamage(rocketDamage, (other.transform.position - transform.position).normalized * force );
        }
        if (other.gameObject.tag == "Segment")
        {
            other.gameObject.GetComponent<PillarBlock>().Damage(rocketDamage);
        }
        // IF PLAYER IS IN RANGE OF EXPLOSION PLAYER IS ALSO KNOCKED BACK
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<Player>().RecieveDamage((other.transform.position - transform.position).normalized * force);
        }
    }

    IEnumerator DestroyOnNextFrame()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        Destroy(this.gameObject);
    }
}

