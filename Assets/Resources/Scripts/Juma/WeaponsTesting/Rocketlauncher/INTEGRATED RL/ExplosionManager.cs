﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionManager : MonoBehaviour
{
    ParticleSystem explosionParticle;
    // Start is called before the first frame update
    void Start()
    {
        explosionParticle = GetComponent<ParticleSystem>();
        explosionParticle.Stop();
        StartCoroutine(PlayExplosionEffect());
    }

    IEnumerator PlayExplosionEffect()
    {
        explosionParticle.Play();
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }
}