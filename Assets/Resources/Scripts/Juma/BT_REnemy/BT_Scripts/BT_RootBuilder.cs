﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_BB : MonoBehaviour
{
    public Enemy enemy;
    public BTBaseNode root;

    public BT_BB(BTBaseNode node, Enemy _enemy)
    {
        enemy = _enemy;
        root = node;
    }

    void Start()
    {
        BTBaseNode nodeUntillFail = new Untill_Fail();
        BTBaseNode nodeSelector = new Selector();
        BTBaseNode nodeSequence = new Sequence();

        root = new Selector();
        root.childNodes.Add(nodeSequence);
        nodeSequence.childNodes.Add(new LN_RE_Movement());
        nodeSequence.childNodes.Add(new LN_RE_Attack());
    }

    public void Executed()
    {
        root.UpdateBehavior(enemy);
    }
}
