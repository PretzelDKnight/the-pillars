﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LN_RE_Movement : BTBaseNode
{
    public override RESULTS UpdateBehavior(Enemy enemy)
    {
        //For the movement the current should be running untill he reaches the target
        //the current should switch to success : current = RESULTS.SUCCEED;
        current = RESULTS.RUNNING;

        //EXAMPLE
        //  if (distance between enemy & target is < 3)
        //  {
        //      current = RESULTS.SUCCEED;      
        //  }
        // this should hopefully bring the enemy to the next state then start attacking

        current = RESULTS.SUCCEED;
        return current;
    }
}
