﻿Shader "Unlit/ViewShiftTEst"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DistortionMap ("Distortion Map", 2D) = "black" {}
        _DistortionFactor ("Distortion Factor", Range(0, 1)) = 0.5
        _EffectTexture ("Effect Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 tanSpaceVDir : TEXCOORD1;
         //       float3 tanSpaceVDir : 
            };

            sampler2D _MainTex, _EffectTexture, _DistortionMap;
            float4 _MainTex_ST;
            float _DistortionFactor;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                fixed3 worldViewDir = normalize(WorldSpaceViewDir(v.vertex));
                
                fixed3 viewDir = UnityWorldToObjectDir(-worldViewDir);

                float3 bitangent = cross( v.normal, v.tangent.xyz ) * v.tangent.w;

                float3x3 tanSpaceMatrx = float3x3(v.tangent.xyz, bitangent, v.normal.xyz);

                o.tanSpaceVDir = mul(tanSpaceMatrx, viewDir);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed2 viewDirShift = fixed2(i.tanSpaceVDir.x, i.tanSpaceVDir.y) * _DistortionFactor;
                float3 distortTexture =  tex2D(_DistortionMap, i.uv+ viewDirShift) * tex2D(_DistortionMap, i.uv + viewDirShift);
                fixed4 color = tex2D(_EffectTexture, i.uv + viewDirShift * (1 - distortTexture.x));
                fixed4 col = tex2D(_MainTex, i.uv) + color;
            //    col.xyz = i.viewDir;
                return col;
            }
            ENDCG
        }
    }
}
